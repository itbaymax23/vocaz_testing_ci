<?php
	define('DB_HOST', 'localhost');
	define('DB_USERNAME', 'nextsent_vocabzy');
	define('DB_DATABASE', 'nextsent_vocabzy');
	define('DB_PASSWORD', '123website123');
	$db = mysqli_connect(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_DATABASE);

	$sql = "SELECT * FROM plans";
	$result = mysqli_query( $db, $sql );

	$index = 0;
	while($row = mysqli_fetch_array($result)) {             
	    $rows[$index]['id'] = $row['id'];
	    $rows[$index]['plan_name'] = $row['plan_name'];
	    $rows[$index]['during_days'] = $row['during_days'];
	    $rows[$index]['plan_price'] = $row['plan_price'];
	    $index++;
	}
?>
<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="SITE KEYWORDS HERE" />
		<meta name="description" content="">
		<meta name='copyright' content=''>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Title -->
		<title>Pricing - Education</title>
		<!-- Favicon -->
		
		<?php include('links.php') ?>

    </head>
    <body>
		<!-- Book Preloader -->
        <div class="book_preload">
            <div class="book">
                <div class="book__page"></div>
                <!--<div class="book__page"></div>-->
                <!--<div class="book__page"></div>-->
            </div>
        </div>
		<!--/ End Book Preloader -->

		<?php include('header.php'); ?>
		<!-- Start Breadcrumbs -->
		<section class="breadcrumbs overlay">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2>Pricing</h2>
						<ul class="bread-list">
							<li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
							<li class="active"><a href="about.html">pricing</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Breadcrumbs -->
		
		<!-- About US -->
		<section class="about-us section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					<!--<h1 class="text-center">More Options. No Obligations.</h1>-->
					<!--<div class="dropdown text-center">-->
     <!--                   <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" style="margin-top: 20px;">-->
     <!--                     Select plans-->
     <!--                   </button>-->
     <!--                   <div class="dropdown-menu">-->
     <!--                     <a class="dropdown-item" id="clk1">1 Year</a>-->
     <!--                     <a class="dropdown-item" id="clk2">2 Year</a>-->
     <!--                     <a class="dropdown-item" id="clk3">3 Year</a>-->
     <!--                   </div>-->
                        
                        
                        
     <!--                 </div>-->
						<div class="price-table-wrapper">
							<div class="pricing-table" id="sho1">
								<h2 class="pricing-table__header">- <?php echo $rows[0]['plan_name']?> -</h2>
								<h3 class="pricing-table__price">FREE TRIAL</h3>
								<a  class="pricing-table__button free_trial" href="javascript:;">
								Join Now!
								</a>
								<ul class="pricing-table__list">
								<li>Free Trial <?php echo $rows[0]['during_days']?> days</li>
								<li>Over 40+ Words</li>
								<li>Automatic Grading and Report</li>
								<li>24 hour tech support</li>
								</ul>
							</div>
							<div class="pricing-table featured-table" id="sho2">
								<h2 class="pricing-table__header">- <?php echo $rows[1]['plan_name']?> -</h2>
								<h3 class="pricing-table__price">$<?php echo $rows[1]['plan_price']?></h3>
								<a  class="pricing-table__button basic" href="javascript:;">
								Join Now!
								</a>
								<ul class="pricing-table__list">
								<li>1 Year Subscription</li>
								<li>Over 40+ Words</li>
								<li>Automatic Grading and Report</li>
								<li>24 hour tech support</li>
								</ul>
							</div>
							<div class="pricing-table" id="sho3">
								<h2 class="pricing-table__header">- <?php echo $rows[2]['plan_name']?> -</h2>
								<h3 class="pricing-table__price">$<?php echo $rows[2]['plan_price']?></h3>
								<a  class="pricing-table__button upgrade" href="javascript:;">
								Join Now!
								</a>
								<ul class="pricing-table__list">
								<li>2 Years Subscription</li>
								<li>Over 40+ Words</li>
								<li>5% discount</li>
								<li>24 hour tech support</li>
								</ul>
							</div>
							<div class="pricing-table" id="sho4">
								<h2 class="pricing-table__header">- <?php echo $rows[3]['plan_name']?> -</h2>
								<h3 class="pricing-table__price">$<?php echo $rows[3]['plan_price']?></h3>
								<a  class="pricing-table__button premium" href="javascript:;">
								Join Now!
								</a>
								<ul class="pricing-table__list">
								<li>3 Years Subscription</li>
								<li>Over 40+ Words</li>
								<li>10% discount</li>
								<li>24 hour tech support</li>
								</ul>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</section>
		<!--/ End About US -->
		
		<?php include('footer.php'); ?> 
   
//   <script>
// $(document).ready(function(){
//     $("#sho1").hide();
//     $("#sho2").hide();
//     $("#sho3").hide();
    
//     $("#clk1").click(function(){
//     $("#sho1").show('slow');
//     $("#sho2").hide();
//     $("#sho3").hide();
//     });
    
//     $("#clk2").click(function(){
//     $("#sho1").hide();
//     $("#sho2").show('slow');
//     $("#sho3").hide();
//     });
    
//     $("#clk3").click(function(){
//     $("#sho1").hide();
//     $("#sho2").hide();
//     $("#sho3").show('slow');
//     });
// });
// </script>
	<script type="text/javascript">
		var session = window.sessionStorage.getItem('session');
		// console.log(session);
		$('.free_trial').click(function(){
			if(session) {
				if(session == 'admin_dashboard') {
					window.location.href = '/portal/' + session + '/suscription_plan';
				} else {
					window.location.href = '/portal/' + session;
				}
			} else {
				window.location.href = '/portal';
			}
		})
		$('.basic').click(function(){
			if(session) {
            	if(session == 'admin_dashboard') {
					window.location.href = '/portal/' + session + '/suscription_plan/2';
				} else {
					window.location.href = '/portal/' + session;
				}
			} else {
				window.location.href = '/portal';
			}
		})
		$('.upgrade').click(function(){
			if(session) {
            	if(session == 'admin_dashboard') {
					window.location.href = '/portal/' + session + '/suscription_plan/3';
				} else {
					window.location.href = '/portal/' + session;
				}
			} else {
				window.location.href = '/portal';
			}
		})
		$('.premium').click(function(){
			if(session) {
            	if(session == 'admin_dashboard') {
					window.location.href = '/portal/' + session + '/suscription_plan/4';
				} else {
					window.location.href = '/portal/' + session;
				}
			} else {
				window.location.href = '/portal';
			}
		})
	</script>
	</body>
</html>