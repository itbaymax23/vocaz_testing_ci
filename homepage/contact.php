<?php
    session_start();
    
?>

<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="SITE KEYWORDS HERE" />
		<meta name="description" content="">
		<meta name='copyright' content=''>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Title -->
		<title>Contact Us - Education</title>
		<!-- Favicon -->
		
		<?php include('links.php') ?>

    </head>
    <body>
		<!-- Book Preloader -->
        <div class="book_preload">
            <div class="book">
                <div class="book__page"></div>
                <!--<div class="book__page"></div>-->
                <!--<div class="book__page"></div>-->
            </div>
        </div>
		<!--/ End Book Preloader -->

		<?php include('header.php'); ?>
		<!-- Start Breadcrumbs -->
		<section class="breadcrumbs overlay">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2>Contact Us</h2>
						<ul class="bread-list">
							<li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
							<li class="active"><a href="about.html">contact</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Breadcrumbs -->
		
		<!-- Contact Us -->
		<section id="contact" class="contact section">
			<div class="container">
				<!--<div class="row">-->
				<!--	<div class="col-md-12">-->
				<!--		<div class="section-title">-->
				<!--			<h2><span>Contact</span> Information</h2>-->
				<!--			<p>Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendrerit, sollicitudin nec ante.  </p>-->
				<!--		</div>-->
				<!--	</div>-->
				<!--</div>-->
				<div class="contact-head">
					<div class="row">
						<!--<div class="col-lg-6 col-md-6 col-12">-->
						<!--	<div class="contact-map">-->
								<!-- Map -->
						<!--		<div id="map1">-->
						<!--			<img src="images/map.jpg" class="img-responsive"/>-->
						<!--		</div>-->
								<!--/ End Map -->
						<!--	</div>-->
						<!--</div>-->
						<div class="col-lg-6 col-md-6 col-6">
						    <h2 style="color:green">
						        <?php
						            if(isset($_SESSION['msg'])){
                                        if($_SESSION['msg']){
                                            echo "mail sent";
                                            unset($_SESSION['msg']);
                                        }else{
                                            echo "mail problem";
                                            unset($_SESSION['msg']);
                                        }
                                        
                                    }
						        ?>
						        
						    </h2>
							<div class="form-head">
								<!-- Form -->
								<form class="form" method="POST" action="mail.php">
									<div class="form-group">
										<input name="name" type="text" placeholder="Enter Name">
									</div>
									<div class="form-group">
										<input name="email" type="email" placeholder="Email Address">
									</div>
									<div class="form-group">
										<input name="subject" type="text" placeholder="School Name">
									</div>
									<div class="form-group">
										<textarea name="message" placeholder="Comment"></textarea>
									</div>
									<div class="form-group">
										<div class="button">
											<button type="submit" name="sub" class="btn primary">Post Comment</button>
										</div>
									</div>
								</form>
								<!--/ End Form -->
							</div>
						</div>
					</div>
				</div>
				<div class="contact-bottom">
					<div class="row">
						<!--<div class="col-lg-4 col-md-4 col-12">-->
							<!-- Contact-Info -->
						<!--	<div class="contact-info">-->
						<!--		<div class="icon"><i class="fa fa-map"></i></div>-->
						<!--		<h3>Location</h3>-->
						<!--		<p>60 Grant Ave. Central New Road 0708, UK 9766 Tanner</p>-->
						<!--	</div>-->
						<!--</div>-->
						<div class="col-lg-6 col-md-6 col-12">
							<!-- Contact-Info -->
							<div class="contact-info">
								<div class="icon"><i class="fa fa-envelope"></i></div>
								<h3>Email Address</h3>
								<a href="mailto:sunbox10@yahoo.com">sunbox10@yahoo.com</a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-12">
							<!-- Contact-Info -->
							<div class="contact-info">
								<div class="icon"><i class="fa fa-phone"></i></div>
								<h3>Get in Touch</h3>
								<p>(855) 731-7525</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Contact Us -->
		<?php include('footer.php'); ?> 
    </body>
</html>