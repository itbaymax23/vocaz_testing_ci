<?php
	$nm= explode(".",basename($_SERVER['PHP_SELF']))[0];
?>
		<!-- Header -->
		<header class="header">
			<!-- Topbar -->
			<div class="topbar" style="padding: 5px 0;">
				<div class="container">
					<div class="row">
						<!-- <div class="col-lg-8 col-12"> -->
							<!-- Contact -->
							<!-- <ul class="content"> -->
								<!-- <li><i class="fa fa-phone"></i>123-456-789</li> -->
								<!-- <li><a href="mailto:info@yourdomain.com"><i class="fa fa-envelope-o"></i>contact@yourdomain.com</a></li> -->
								<!-- <li><i class="fa fa-clock-o"></i>Opening: 10:00am - 5:00pm</li> -->
							<!-- </ul> -->
							<!-- End Contact -->
						<!-- </div> -->
						<div class="col-lg-12 col-md-12 col-12 col-md-offset-3 col-lg-offset-3" style="float: right;">
							<!-- Social -->
							<ul class="social">
								<li ><a id="dashboard" href="/portal" hidden>Dashboard</a></li>
								<li ><a id="session" href="javascript:;"></a></li>
								<li><a id="registration" href="/portal/admin/register">Sign Up</a></li>
							</ul>
							<!-- End Social -->
						</div>
					</div>
				</div>
			</div>
			<!-- End Topbar -->
			<!-- Header Inner -->
			<div class="header-inner">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-12">
							<div class="logo">
								<a href="index.php"><img src="images/logo.png" alt="#"></a>
							</div>
							<div class="mobile-menu"></div>
						</div>
						<div class="col-lg-9 col-md-9 col-12">
							<!-- Header Widget -->
							<div class="header-widget">
								<div class="single-widget">
									<i class="fa fa-phone"></i>
									<h4>Call Now<span>(855) 731-7525</span></h4>
								</div>
								<div class="single-widget">
									<i class="fa fa-envelope-o"></i>
									<h4>Send Message<a href="mailto:sunbox10@yahoo.com"><span>support@education.com</span></a></h4>
								</div>
								<div class="single-widget">
								    <a href="#">
									<i class="fa fa-money"></i>
									<h4>Donate <span>To Scholarship Program</span></h4>
									</a>
								</div>
							</div>
							<!--/ End Header Widget -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Header Inner -->
			<!-- Header Menu -->
			<div class="header-menu">
				<div class="container">
					<div class="row">
						<div class="col-6">
							<nav class="navbar navbar-default">
								<div class="navbar-collapse">
									<!-- Main Menu -->
									<ul id="nav" class="nav menu navbar-nav">
										<li <?=('index'==$nm)?'class="active"':'' ?>><a href="index.php">Home</a></li>
										<li <?=('about'==$nm)?'class="active"':'' ?>><a href="about.php">About Us</a></li>
										<li <?=('pricing'==$nm)?'class="active"':'' ?>><a href="pricing.php">Software</a></li>
										<li <?=('contact'==$nm)?'class="active"':'' ?>><a href="contact.php">Contact</a></li>
									</ul>
									<!-- End Main Menu -->
								</div> 
							</nav>
						</div>
						<div class="col-6 header-new">
							<nav class="navbar navbar-default">
								<div class="header-widget1">
								<div class="single-widget1">
									<i class="fa fa-phone"></i>
									<h4>Call Now<span>(855) 731-7525</span></h4>
								</div>
								<div class="single-widget1">
									<i class="fa fa-envelope-o"></i>
									<h4>Send Message<a href="mailto:mailus@mail.com"><span>support@education.com</span></a></h4>
								</div>
								<div class="single-widget1">
								    <a href="#">
									<i class="fa fa-money"></i>
									<h4>Donate <span>To Scholarship Program</span></h4>
									</a>
								</div>
							</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Header Menu -->
		</header>
		<!-- End Header -->
