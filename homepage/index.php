<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="SITE KEYWORDS HERE" />
		<meta name="description" content="">
		<meta name='copyright' content=''>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Title -->
		<title>Vocabzy</title>
		<!-- Favicon -->
		
		<?php include('links.php') ?>

    </head>
    <body>
		<!-- Book Preloader -->
        <div class="book_preload">
            <div class="book">
                <div class="book__page"></div>
               <!--<div class="book__page"></div>-->
               <!-- <div class="book__page"></div>-->
            </div>
        </div>
		<!--/ End Book Preloader -->

		<?php include('header.php'); ?>

		<!-- Slider Area -->
		<section class="home-slider">
			<div class="slider-active">
				<!-- Single Slider -->
				<div class="single-slider overlay" style="background-image:url('images/slider/slider7b.jpg')">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-12">
								<div class="slider-text">
									<h1></h1>
									<p></p>
									<!-- <div class="button">
										<a href="courses.html" class="btn primary">Our Courses</a>
										<a href="about.html" class="btn">About Learnedu</a>
									</div> -->
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/ End Single Slider -->
				<!-- Single Slider -->
				<div class="single-slider overlay" style="background-image:url('images/slider/Preschool-Slider-2_0.jpg')">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2 col-12">
								<div class="slider-text text-center">
									<!-- <h1>Creative Template for <span>Education</span> & Courses Website</h1>
									<p>Our mission is to empower clients, colleagues, and communities to achieve aspirations while building lasting, caring relationships.</p>
									<div class="button">
										<a href="courses.html" class="btn primary">Our Courses</a>
										<a href="about.html" class="btn">About Learnedu</a>
									</div> -->
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/ End Single Slider -->
				<!-- Single Slider -->
				<div class="single-slider overlay" style="background-image:url('images/slider/slider_4.jpg')">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 offset-lg-4 col-md-8 offset-md-4 col-12">
								<div class="slider-text text-right">
									<!-- <h1>Responsive Template for <span>Education</span> & Courses Website</h1>
									<p>Our mission is to empower clients, colleagues, and communities to achieve aspirations while building lasting, caring relationships.</p>
									<div class="button">
										<a href="courses.html" class="btn primary">Our Courses</a>
										<a href="about.html" class="btn">About Learnedu</a>
									</div> -->
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/ End Single Slider -->
			</div>
		</section>
		<!--/ End Slider Area -->
		
		<!-- Features -->
		<section class="our-features section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section-title">
							<h2>We Provide <span>Teachers with tools</span> To Test for Words</h2>
							<p>Education should be fun and simple. Vocabzy is dedicated to provide teachers with the tools needed to test students and help spell the basic words. The prime goal for vocabzy is to foster learning at a young age and continue into high school and beyond. Give us a try.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-4 col-12">
					
					
						<!-- Single Feature -->
						<div class="single-feature">
							<div class="feature-head contai">
								<img src="images/12087_giao_vien_1.jpg" alt="#" class="imge">
								<div class="middl">
                                    <div class="tet">Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis.</div>
                                  </div>
							</div>
							<h2 class="text-center">Primary</h2>
<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim</p>	-->
						</div>
						<!--/ End Single Feature -->
					</div>
					<div class="col-lg-4 col-md-4 col-12">
						<!-- Single Feature -->
						<div class="single-feature">
							<div class="feature-head contai">
								<img src="images/kindergartners.jpg" alt="#" class="imge">
								<div class="middl">
                                    <div class="tet">Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis.</div>
                                  </div>
							</div>
                            <h2 class="text-center">1<sup>st</sup> to 3<sup>rd</sup> Grade</h2>
<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim</p>	-->
						</div>
						<!--/ End Single Feature -->
					</div>
					<div class="col-lg-4 col-md-4 col-12">
						<!-- Single Feature -->
						<div class="single-feature">
							<div class="feature-head contai">
								<img src="images/image_2019_0.png" alt="#" class="imge">
								<div class="middl">
                                    <div class="tet">Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis.</div>
                                  </div>
							</div>
                            <h2 class="text-center">4<sup>th</sup> to 8<sup>th</sup> Grade</h2>
<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim</p>	-->
						</div>
						<!--/ End Single Feature -->
					</div>
				</div>
			</div>
		</section>
		<!-- End Features -->

		
		
		<!-- Enroll -->
		<section class="enroll overlay section" data-stellar-background-ratio="0.5">
			<div class="container">
				<div class="row">
					<div class="col-lg-10 offset-lg-1">
						<div class="row">
							<div class="col-lg-12 col-12">
								<!-- Single Enroll -->
								<div class="enroll-form">
									<div class="form-title">
										<h2>FEATURES</h2>
<!--										<p>Before you miss the chance to get your seat!</p>-->
									</div>
									<!-- Form -->
								    <div class="col-md-12 pdng">
								        <div class="col-md-12 ft">
                                            <h2>TechnoKids and 21st Century Learning</h2>	            
				                        </div>
								        <div class="col-md-12 ft">
                                            <p>Computer lessons have students apply knowledge to analyze information, collaborate, solve problems, and make decisions. Discover the benefits to using TechnoKids technology projects:</p>	            
				                        </div>
				                        <div class="row">
				                        <div class="col-md-6">
				                            <ul class="fa-ul">
  <li><span class="fa-li" ><i class="fa fa-check-square"></i></span>Everyone at your school or site can use the projects</li>
  <li><span class="fa-li"><i class="fa fa-check-square"></i></span>Pay one price - number of devices or users does not affect cost</li>
  <li><span class="fa-li" ><i class="fa fa-check-square"></i></span>No renewal fee</li>
  <li><span class="fa-li"><i class="fa fa-check-square"></i></span>Includes teacher guide, workbook and resources</li>
  <li><span class="fa-li" ><i class="fa fa-check-square"></i></span>Activities are project based</li>
  <li><span class="fa-li"><i class="fa fa-check-square"></i></span>Assignments include step-by-step instructions</li>
  
</ul>
				                        </div>
				                        <div class="col-md-6">
				                        <ul class="fa-ul">
    <li><span class="fa-li" ><i class="fa fa-check-square"></i></span>Free curriculum support</li>
  <li><span class="fa-li"><i class="fa fa-check-square"></i></span>Resource files are customizable</li>
  <li><span class="fa-li" ><i class="fa fa-check-square"></i></span>Themes integrate into curriculum units</li>
  <li><span class="fa-li"><i class="fa fa-check-square"></i></span>Teach technology skills in a meaningful way</li>
  <li><span class="fa-li" ><i class="fa fa-check-square"></i></span>Download a project or receive a set on a USB drive</li>
  <li><span class="fa-li"><i class="fa fa-check-square"></i></span>Multi-site licence discounts available</li>
                                            </ul>
				                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 ft text-center">
                                        <h3>Engage Students with Fun Computer Lesson Plans</h3><br>
                                    </div>
                                    <div class="col-md-12 ft text-center">
                                        <p>Interdisciplinary activities target learning objectives from multiple subject areas such as language arts, mathematics, social studies, science, geography, history, visual arts, or drama.</p>
                                    </div>
                                    
                                    
									</div>
									<!--/ End Form -->
								</div>
								<!-- Single Enroll -->
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Enroll -->

		<!-- Courses -->
		<!--<section class="courses section-bg section">-->
		<!--	<div class="container">-->
		<!--		<div class="row">-->
		<!--			<div class="col-12">-->
		<!--				<div class="section-title">-->
		<!--					<h2>Popular <span>Courses</span> We Offer</h2>-->
		<!--					<p>Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri </p>-->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--		<div class="row">-->
		<!--			<div class="col-12">-->
		<!--				<div class="course-slider">-->
							<!-- Single Course -->
		<!--					<div class="single-course">-->
		<!--						<div class="course-head overlay">-->
		<!--							<img src="images/course/course1.jpg" alt="#">-->
		<!--							<a href="course-single.html" class="btn"><i class="fa fa-link"></i></a>-->
		<!--						</div>-->
		<!--						<div class="single-content">-->
		<!--							<h4><a href="course-single.html"><span>Commerce</span>Business Management</a></h4>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit aenean </p>-->
		<!--						</div>-->
		<!--						<div class="course-meta">-->
		<!--							<div class="meta-left">-->
		<!--								<span><i class="fa fa-users"></i>36 Seat</span>-->
		<!--								<span><i class="fa fa-clock-o"></i>2 Years</span>-->
		<!--							</div>-->
		<!--							<span class="price">$350</span>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Course -->
							<!-- Single Course -->
		<!--					<div class="single-course">-->
		<!--						<div class="course-head overlay">-->
		<!--							<img src="images/course/course2.jpg" alt="#">-->
		<!--							<a href="course-single.html" class="btn"><i class="fa fa-link"></i></a>-->
		<!--						</div>-->
		<!--						<div class="single-content">-->
		<!--							<h4><a href="course-single.html"><span>Science</span>Software Engineer</a></h4>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit aenean </p>-->
		<!--						</div>-->
		<!--						<div class="course-meta">-->
		<!--							<div class="meta-left">-->
		<!--								<span><i class="fa fa-users"></i>20 Seat</span>-->
		<!--								<span><i class="fa fa-clock-o"></i>1 Years</span>-->
		<!--							</div>-->
		<!--							<span class="price">$590</span>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Course -->
							<!-- Single Course -->
		<!--					<div class="single-course">-->
		<!--						<div class="course-head overlay">-->
		<!--							<img src="images/course/course3.jpg" alt="#">-->
		<!--							<a href="course-single.html" class="btn"><i class="fa fa-link"></i></a>-->
		<!--						</div>-->
		<!--						<div class="single-content">-->
		<!--							<h4><a href="course-single.html"><span>Science</span>Electrical Management</a></h4>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit aenean </p>-->
		<!--						</div>-->
		<!--						<div class="course-meta">-->
		<!--							<div class="meta-left">-->
		<!--								<span><i class="fa fa-users"></i>49 Seat</span>-->
		<!--								<span><i class="fa fa-clock-o"></i>2 Years</span>-->
		<!--							</div>-->
		<!--							<span class="price">$140</span>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Course -->
							<!-- Single Course -->
		<!--					<div class="single-course">-->
		<!--						<div class="course-head overlay">-->
		<!--							<img src="images/course/course1.jpg" alt="#">-->
		<!--							<a href="course-single.html" class="btn"><i class="fa fa-link"></i></a>-->
		<!--						</div>-->
		<!--						<div class="single-content">-->
		<!--							<h4><a href="course-single.html"><span>Commerce</span>Business Management</a></h4>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit aenean </p>-->
		<!--						</div>-->
		<!--						<div class="course-meta">-->
		<!--							<div class="meta-left">-->
		<!--								<span><i class="fa fa-users"></i>36 Seat</span>-->
		<!--								<span><i class="fa fa-clock-o"></i>2 Years</span>-->
		<!--							</div>-->
		<!--							<span class="price">$350</span>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Course -->
							<!-- Single Course -->
		<!--					<div class="single-course">-->
		<!--						<div class="course-head overlay">-->
		<!--							<img src="images/course/course2.jpg" alt="#">-->
		<!--							<a href="course-single.html" class="btn"><i class="fa fa-link"></i></a>-->
		<!--						</div>-->
		<!--						<div class="single-content">-->
		<!--							<h4><a href="course-single.html"><span>Science</span>Software Engineer</a></h4>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit aenean </p>-->
		<!--						</div>-->
		<!--						<div class="course-meta">-->
		<!--							<div class="meta-left">-->
		<!--								<span><i class="fa fa-users"></i>20 Seat</span>-->
		<!--								<span><i class="fa fa-clock-o"></i>1 Years</span>-->
		<!--							</div>-->
		<!--							<span class="price">$590</span>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Course -->
							<!-- Single Course -->
		<!--					<div class="single-course">-->
		<!--						<div class="course-head overlay">-->
		<!--							<img src="images/course/course3.jpg" alt="#">-->
		<!--							<a href="course-single.html" class="btn"><i class="fa fa-link"></i></a>-->
		<!--						</div>-->
		<!--						<div class="single-content">-->
		<!--							<h4><a href="course-single.html"><span>Science</span>Electrical Management</a></h4>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit aenean </p>-->
		<!--						</div>-->
		<!--						<div class="course-meta">-->
		<!--							<div class="meta-left">-->
		<!--								<span><i class="fa fa-users"></i>49 Seat</span>-->
		<!--								<span><i class="fa fa-clock-o"></i>2 Years</span>-->
		<!--							</div>-->
		<!--							<span class="price">$140</span>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Course -->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--	</div>-->
		<!--</section>-->
		<!--/ End Courses -->	
		
		<!-- Call To Action -->
		<!--<section class="cta" data-stellar-background-ratio="0.5">-->
		<!--	<div class="container">-->
		<!--		<div class="row">-->
		<!--			<div class="col-lg-5 offset-lg-6 col-12">-->
		<!--				<div class="cta-inner overlay">-->
		<!--					<div class="text-content">-->
		<!--						<h2>We Focus On Brands, Products & Campaigns</h2>-->
		<!--						<p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore cum. Coluta nobis est eligendi optio cumque nihil impedit quo minusd quod maxime placeat facere possimus, omnis voluptas assumenda est. Our friendly support team is standing by to help you every step of the way.</p>-->
		<!--						<div class="button">-->
		<!--							<a class="btn primary" href="contact.html" >Register Now</a>-->
		<!--						</div>-->
		<!--					</div>-->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--	</div>-->
		<!--</section>-->
		<!--/ End Call To Action -->
		
		<!-- Team -->
		<!--<section class="team section">-->
		<!--	<div class="container">-->
		<!--		<div class="row">-->
		<!--			<div class="col-12">-->
		<!--				<div class="section-title">-->
		<!--					<h2>Our Awesome <span>Teachers</span></h2>-->
		<!--					<p>Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri </p>-->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--		<div class="row">-->
		<!--			<div class="col-lg-3 col-md-6 col-12">-->
						<!-- Single Team -->
		<!--				<div class="single-team">-->
		<!--					<img src="images/team/team1.jpg" alt="#">-->
		<!--					<div class="team-hover">-->
		<!--						<h4>Rohan Jonson<span>Associate Professor</span></h4>-->
		<!--						<p>cumque nihil impedit quo minusid quod maxime placeat facere possimus</p>-->
		<!--						<ul class="social">-->
		<!--							<li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-youtube"></i></a></li>-->
		<!--						</ul>-->
		<!--					</div>-->
		<!--				</div>-->
						<!--/ End Single Team -->
		<!--			</div>-->
		<!--			<div class="col-lg-3 col-md-6 col-12">-->
						<!-- Single Team -->
		<!--				<div class="single-team">-->
		<!--					<img src="images/team/team2.jpg" alt="#">-->
		<!--					<div class="team-hover">-->
		<!--						<h4 class="name">Ian Harvey<span class="work">Web Programmer</span></h4>-->
		<!--						<p>cumque nihil impedit quo minusid quod maxime placeat facere possimus</p>-->
		<!--						<ul class="social">-->
		<!--							<li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-youtube"></i></a></li>-->
		<!--						</ul>-->
		<!--					</div>-->
		<!--				</div>-->
						<!--/ End Single Team -->
		<!--			</div>-->
		<!--			<div class="col-lg-3 col-md-6 col-12">-->
						<!-- Single Team -->
		<!--				<div class="single-team">-->
		<!--					<img src="images/team/team3.jpg" alt="#">-->
		<!--					<div class="team-hover">-->
		<!--						<h4 class="name">Lusfat Roman<span class="work">Software Engineer</span></h4>-->
		<!--						<p>cumque nihil impedit quo minusid quod maxime placeat facere possimus</p>-->
		<!--						<ul class="social">-->
		<!--							<li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-youtube"></i></a></li>-->
		<!--						</ul>-->
		<!--					</div>-->
		<!--				</div>-->
						<!--/ End Single Team -->
		<!--			</div>-->
		<!--			<div class="col-lg-3 col-md-6 col-12">-->
						<!-- Single Team -->
		<!--				<div class="single-team">-->
		<!--					<img src="images/team/team4.jpg" alt="#">-->
		<!--					<div class="team-hover">-->
		<!--						<h4 class="name">Nalpamb Bold<span class="work">JS Developer</span></h4>-->
		<!--						<p>cumque nihil impedit quo minusid quod maxime placeat facere possimus</p>-->
		<!--						<ul class="social">-->
		<!--							<li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>-->
		<!--							<li><a href="#"><i class="fa fa-youtube"></i></a></li>-->
		<!--						</ul>-->
		<!--					</div>-->
		<!--				</div>-->
						<!--/ End Single Team -->
		<!--			</div>-->
		<!--		</div>-->
		<!--	</div>-->
		<!--</section>-->
		<!--/ End Team -->
		
		<!-- Testimonials -->
		<!--<section class="testimonials overlay section" data-stellar-background-ratio="0.5">-->
		<!--	<div class="container">-->
		<!--		<div class="row">-->
		<!--			<div class="col-12">-->
		<!--				<div class="testimonial-slider">-->
							<!-- Single Testimonial -->
		<!--					<div class="single-testimonial">-->
		<!--						<img src="images/testimonial1.jpg" alt="#">-->
		<!--						<div class="main-content">-->
		<!--							<h4 class="name">Sanavce Faglane</h4>-->
		<!--							<p>Nulla cursus a metus vel placerat. Fusce malesuada volutpat pretium. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus velit libero, viverra quis euismod vel pellentesque at tortor. Donec</p>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Testimonial -->
							<!-- Single Testimonial -->
		<!--					<div class="single-testimonial">-->
		<!--						<img src="images/testimonial2.jpg" alt="#">-->
		<!--						<div class="main-content">-->
		<!--							<h4 class="name">Jansan Kate</h4>-->
		<!--							<p>Nulla cursus a metus vel placerat. Fusce malesuada volutpat pretium. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus velit libero, viverra quis euismod vel pellentesque at tortor. Donec</p>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Testimonial -->
							<!-- Single Testimonial -->
		<!--					<div class="single-testimonial">-->
		<!--						<img src="images/testimonial3.jpg" alt="#">-->
		<!--						<div class="main-content">-->
		<!--							<h4 class="name">Sanavce Faglane</h4>-->
		<!--							<p>Nulla cursus a metus vel placerat. Fusce malesuada volutpat pretium. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus velit libero, viverra quis euismod vel pellentesque at tortor. Donec</p>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Testimonial -->
							<!-- Single Testimonial -->
		<!--					<div class="single-testimonial">-->
		<!--						<img src="images/testimonial4.jpg" alt="#">-->
		<!--						<div class="main-content">-->
		<!--							<h4 class="name">Jansan Kate</h4>-->
		<!--							<p>Nulla cursus a metus vel placerat. Fusce malesuada volutpat pretium. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus velit libero, viverra quis euismod vel pellentesque at tortor. Donec</p>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Testimonial -->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--	</div>-->
		<!--</section>-->
		<!--/ End Testimonials -->
		
		<!-- Events -->
		<!--<section class="events section">-->
		<!--	<div class="container">-->
		<!--		<div class="row">-->
		<!--			<div class="col-12">-->
		<!--				<div class="section-title">-->
		<!--					<h2>Upcoming <span>Events</span></h2>-->
		<!--					<p>Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri </p>-->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--		<div class="row">-->
		<!--			<div class="col-12">-->
		<!--				<div class="event-slider">-->
							<!-- Single Event -->
		<!--					<div class="single-event">-->
		<!--						<div class="head overlay">-->
		<!--							<img src="images/events/event1.jpg" alt="#">-->
		<!--							<a href="images/events/event1.jpg" data-fancybox="photo" class="btn"><i class="fa fa-search"></i></a>-->
		<!--						</div>-->
		<!--						<div class="event-content">-->
		<!--							<div class="meta"> -->
		<!--								<span><i class="fa fa-calendar"></i>05 June 2018</span>-->
		<!--								<span><i class="fa fa-clock-o"></i>12.00-5.00PM</span>-->
		<!--							</div>-->
		<!--							<h4><a href="event-single.html">Freshers Day Reception 2018</a></h4>-->
		<!--							<p>Lorem ipsum dolor sit amet, consectetur adipi sicing elit, sed do eiusmod tempor incididunt</p>-->
		<!--							<div class="button">-->
		<!--								<a href="event-single.html" class="btn">Join Event</a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Event -->
							<!-- Single Event -->
		<!--					<div class="single-event">-->
		<!--						<div class="head overlay">-->
		<!--							<img src="images/events/event2.jpg" alt="#">-->
		<!--							<a href="images/events/event2.jpg" data-fancybox="photo" class="btn"><i class="fa fa-search"></i></a>-->
		<!--						</div>-->
		<!--						<div class="event-content">-->
		<!--							<div class="meta">-->
		<!--								<span><i class="fa fa-calendar"></i>03 July 2018</span>-->
		<!--								<span><i class="fa fa-clock-o"></i>03.20-5.20PM</span>-->
		<!--							</div>-->
		<!--							<h4><a href="event-single.html">Best Student Award 2018</a></h4>-->
		<!--							<p>Lorem ipsum dolor sit amet, consectetur adipi sicing elit, sed do eiusmod tempor incididunt</p>-->
		<!--							<div class="button">-->
		<!--								<a href="event-single.html" class="btn">Join Event</a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Event -->
							<!-- Single Event -->
		<!--					<div class="single-event">-->
		<!--						<div class="head overlay">-->
		<!--							<img src="images/events/event3.jpg" alt="#">-->
		<!--							<a href="images/events/event3.jpg" data-fancybox="photo" class="btn"><i class="fa fa-search"></i></a>-->
		<!--						</div>-->
		<!--						<div class="event-content">-->
		<!--							<div class="meta">-->
		<!--								<span><i class="fa fa-calendar"></i>15 Dec 2018</span>-->
		<!--								<span><i class="fa fa-clock-o"></i>12.30-5.30PM</span>-->
		<!--							</div>-->
		<!--							<div class="title">-->
		<!--								<h4><a href="event-single.html">Student Workshop</a></h4>-->
		<!--								<p>Lorem ipsum dolor sit amet, consectetur adipi sicing elit, sed do eiusmod tempor incididunt</p>-->
		<!--							</div>-->
		<!--							<div class="button">-->
		<!--								<a href="event-single.html" class="btn">Join Event</a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!--/ End Single Event -->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--	</div>-->
		<!--</section>-->
		<!--/ End Events -->
		
		<!-- Fun Facts -->
		<div class="fun-facts overlay" data-stellar-background-ratio="0.5">
			<div class="container">
				<div class="row">
					<!--<div class="col-lg-3 col-md-6 col-6">-->
						<!-- Single Fact -->
					<!--	<div class="single-fact">-->
					<!--		<i class="fa fa-institution"></i>-->
					<!--		<div class="number"><span class="counter">80</span>k+</div>-->
					<!--		<p>Active Cources</p>-->
					<!--	</div>-->
						<!--/ End Single Fact -->
					<!--</div>-->
					<div class="col-lg-6 col-md-6 col-6">
						<!-- Single Fact -->
						<div class="single-fact">
							<i class="fa fa-graduation-cap"></i>
							<div class="number"><span class="counter">33</span>k+</div>
							<p>Active Students</p>
						</div>
						<!--/ End Single Fact -->
					</div>
					<!--<div class="col-lg-3 col-md-6 col-6">-->
						<!-- Single Fact -->
					<!--	<div class="single-fact">-->
					<!--		<i class="fa fa-video-camera"></i>-->
					<!--		<div class="number"><span class="counter">278</span>+</div>-->
					<!--		<p>Video Cources</p>-->
					<!--	</div>-->
						<!--/ End Single Fact -->
					<!--</div>-->
					<div class="col-lg-6 col-md-6 col-6">
						<!-- Single Fact -->
						<div class="single-fact">
							<i class="fa fa-trophy"></i>
							<div class="number"><span class="counter">308</span>+</div>
							<p>Awards Won</p>
						</div>
						<!--/ End Single Fact -->
					</div>
				</div>
			</div>
		</div>
		<!--/ End Fun Facts -->
		
		<!-- Blogs -->
		<!--<section class="blog section">-->
		<!--	<div class="container">-->
		<!--		<div class="row">-->
		<!--			<div class="col-12">-->
		<!--				<div class="section-title">-->
		<!--					<h2>Latest <span>News</span></h2>-->
		<!--					<p>Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri </p>-->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--		<div class="row">-->
		<!--			<div class="col-12">-->
		<!--				<div class="blog-slider">-->
							<!-- Single Blog -->
		<!--					<div class="single-blog">-->
		<!--						<div class="blog-head overlay">-->
		<!--							<div class="date">-->
		<!--								<h4>10<span>May</span></h4>-->
		<!--							</div>-->
		<!--							<img src="images/blog/blog1.jpg" alt="#">-->
		<!--						</div>-->
		<!--						<div class="blog-content">-->
		<!--							<h4 class="blog-title"><a href="blog-single.html">Our Student Have sit amet egestas</a></h4>-->
		<!--							<div class="blog-info">-->
		<!--								<a href="#"><i class="fa fa-user"></i>By: Admin</a>-->
		<!--								<a href="#"><i class="fa fa-list"></i>Learning</a>-->
		<!--								<a href="#"><i class="fa fa-heart-o"></i>53K</a>-->
		<!--							</div>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Et harum quidem rerum facilis est et expedita distinctio</p>-->
		<!--							<div class="button">-->
		<!--								<a href="blog-single.html" class="btn">Read More<i class="fa fa-angle-double-right"></i></a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!-- End Single Blog -->
							<!-- Single Blog -->
		<!--					<div class="single-blog">-->
		<!--						<div class="blog-head overlay">-->
		<!--							<div class="date">-->
		<!--								<h4>05<span>May</span></h4>-->
		<!--							</div>-->
		<!--							<img src="images/blog/blog2.jpg" alt="#">-->
		<!--						</div>-->
		<!--						<div class="blog-content">-->
		<!--							<h4 class="blog-title"><a href="blog-single.html">Our teachers egestas erat dignissim</a></h4>-->
		<!--							<div class="blog-info">-->
		<!--								<a href="#"><i class="fa fa-user"></i>By: Admin</a>-->
		<!--								<a href="#"><i class="fa fa-list"></i>Academic</a>-->
		<!--								<a href="#"><i class="fa fa-heart-o"></i>33K</a>-->
		<!--							</div>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Et harum quidem rerum facilis est et expedita distinctio</p>-->
		<!--							<div class="button">-->
		<!--								<a href="blog-single.html" class="btn">Read More<i class="fa fa-angle-double-right"></i></a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!-- End Single Blog -->
							<!-- Single Blog -->
		<!--					<div class="single-blog">-->
		<!--						<div class="blog-head overlay">-->
		<!--							<div class="date">-->
		<!--								<h4>15<span>Mar</span></h4>-->
		<!--							</div>-->
		<!--							<img src="images/blog/blog3.jpg" alt="#">-->
		<!--						</div>-->
		<!--						<div class="blog-content">-->
		<!--							<h4 class="blog-title"><a href="blog-single.html">We are Proffesional Have velit Landon</a></h4>-->
		<!--							<div class="blog-info">-->
		<!--								<a href="#"><i class="fa fa-user"></i>By: Admin</a>-->
		<!--								<a href="#"><i class="fa fa-list"></i>Knowledge</a>-->
		<!--								<a href="#"><i class="fa fa-heart-o"></i>11K</a>-->
		<!--							</div>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Et harum quidem rerum facilis est et expedita distinctio</p>-->
		<!--							<div class="button">-->
		<!--								<a href="blog-single.html" class="btn">Read More<i class="fa fa-angle-double-right"></i></a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!-- End Single Blog -->
							<!-- Single Blog -->
		<!--					<div class="single-blog">-->
		<!--						<div class="blog-head overlay">-->
		<!--							<div class="date">-->
		<!--								<h4>10<span>Mar</span></h4>-->
		<!--							</div>-->
		<!--							<img src="images/blog/blog4.jpg" alt="#">-->
		<!--						</div>-->
		<!--						<div class="blog-content">-->
		<!--							<h4 class="blog-title"><a href="blog-single.html">Our Student Have sit amet egestas</a></h4>-->
		<!--							<div class="blog-info">-->
		<!--								<a href="#"><i class="fa fa-user"></i>By: Admin</a>-->
		<!--								<a href="#"><i class="fa fa-list"></i>Learning</a>-->
		<!--								<a href="#"><i class="fa fa-heart-o"></i>53K</a>-->
		<!--							</div>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Et harum quidem rerum facilis est et expedita distinctio</p>-->
		<!--							<div class="button">-->
		<!--								<a href="blog-single.html" class="btn">Read More<i class="fa fa-angle-double-right"></i></a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!-- End Single Blog -->
							<!-- Single Blog -->
		<!--					<div class="single-blog">-->
		<!--						<div class="blog-head overlay">-->
		<!--							<div class="date">-->
		<!--								<h4>25<span>Feb</span></h4>-->
		<!--							</div>-->
		<!--							<img src="images/blog/blog2.jpg" alt="#">-->
		<!--						</div>-->
		<!--						<div class="blog-content">-->
		<!--							<h4 class="blog-title"><a href="blog-single.html">Our teachers egestas erat dignissim</a></h4>-->
		<!--							<div class="blog-info">-->
		<!--								<a href="#"><i class="fa fa-user"></i>By: Admin</a>-->
		<!--								<a href="#"><i class="fa fa-list"></i>Academic</a>-->
		<!--								<a href="#"><i class="fa fa-heart-o"></i>33K</a>-->
		<!--							</div>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Et harum quidem rerum facilis est et expedita distinctio</p>-->
		<!--							<div class="button">-->
		<!--								<a href="blog-single.html" class="btn">Read More<i class="fa fa-angle-double-right"></i></a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!-- End Single Blog -->
							<!-- Single Blog -->
		<!--					<div class="single-blog">-->
		<!--						<div class="blog-head overlay">-->
		<!--							<div class="date">-->
		<!--								<h4>28<span>Feb</span></h4>-->
		<!--							</div>-->
		<!--							<img src="images/blog/blog3.jpg" alt="#">-->
		<!--						</div>-->
		<!--						<div class="blog-content">-->
		<!--							<h4 class="blog-title"><a href="blog-single.html">We are Proffesional Have velit Landon</a></h4>-->
		<!--							<div class="blog-info">-->
		<!--								<a href="#"><i class="fa fa-user"></i>By: Admin</a>-->
		<!--								<a href="#"><i class="fa fa-list"></i>Knowledge</a>-->
		<!--								<a href="#"><i class="fa fa-heart-o"></i>11K</a>-->
		<!--							</div>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Et harum quidem rerum facilis est et expedita distinctio</p>-->
		<!--							<div class="button">-->
		<!--								<a href="blog-single.html" class="btn">Read More<i class="fa fa-angle-double-right"></i></a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!-- End Single Blog -->
							<!-- Single Blog -->
		<!--					<div class="single-blog">-->
		<!--						<div class="blog-head overlay">-->
		<!--							<div class="date">-->
		<!--								<h4>03<span>Jan</span></h4>-->
		<!--							</div>-->
		<!--							<img src="images/blog/blog4.jpg" alt="#">-->
		<!--						</div>-->
		<!--						<div class="blog-content">-->
		<!--							<h4 class="blog-title"><a href="blog-single.html">Our Student Have sit amet egestas</a></h4>-->
		<!--							<div class="blog-info">-->
		<!--								<a href="#"><i class="fa fa-user"></i>By: Admin</a>-->
		<!--								<a href="#"><i class="fa fa-list"></i>Learning</a>-->
		<!--								<a href="#"><i class="fa fa-heart-o"></i>53K</a>-->
		<!--							</div>-->
		<!--							<p>Vivamus volutpat eros pulvinar velit laoreet, sit amet egestas erat dignissim. Et harum quidem rerum facilis est et expedita distinctio</p>-->
		<!--							<div class="button">-->
		<!--								<a href="blog-single.html" class="btn">Read More<i class="fa fa-angle-double-right"></i></a>-->
		<!--							</div>-->
		<!--						</div>-->
		<!--					</div>-->
							<!-- End Single Blog -->
		<!--				</div>-->
		<!--			</div>-->
		<!--		</div>-->
		<!--	</div>-->
		<!--</section>-->
		<!--/ End Blogs -->
		
		<?php include('footer.php'); ?> 
   
   
    </body>
</html>