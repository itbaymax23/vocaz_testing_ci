<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';
require 'config.php';

function send_mail($subj=null,$body=null,$bcc=null,$email=null)
{
    // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);
    
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = SMTP_Host;  // Specify main and backup SMTP servers
        $mail->SMTPAuth   = SMTPAuth;                                   // Enable SMTP authentication
        $mail->Username   = Username;                     // SMTP username
        $mail->Password   = Password;                               // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = Port;                                    // TCP port to connect to
    
        //Recipients
        $mail->setFrom(Username, setFrom_title);
        
        if($email===null){
    		$mail->addAddress($bcc[0]);
    	} else {
    		$mail->addAddress($email);
    	}
    	
    	if($bcc!==null){
    		for($i=0;$i<count($bcc);$i++){
    			$mail->addBCC($bcc[$i]);	
    		}
    	}
        
        // $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
        // $mail->addAddress('ankitchaudhary.969@gmail.com');               // Name is optional
        // $mail->addReplyTo(Username, 'Information');
        // $mail->addCC('sunbox10@yahoo.com');
        // $mail->addBCC('ankitchaudhary.969@gmail.com');
    
        // Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    
        // Content
        $mail->isHTML(isHTML);                                  // Set email format to HTML
        $mail->Subject = $subj;
        $mail->Body    = $body;
    
        if(!$mail->send()) {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
		    return false;
        }else{
            echo 'Message has been sent';
		    return true;
        }
            
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

}