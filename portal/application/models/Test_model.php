<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Test_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_sum_students_word_ok($test_id) {
        $query = $this->db->query("SELECT SUM(sta.word_ok) as cantidad FROM students_tests st INNER JOIN 
        students_tests_answers sta ON sta.students_tests_id = st.id WHERE test_id = {$test_id} AND status = 2");
        return $query->row()->cantidad;
    }
    
    public function get_count_students_tests($test_id) {
        $query = $this->db->query("SELECT COUNT(1) as cantidad FROM students_tests WHERE test_id = {$test_id} AND status = 2");
    }
    
    public function get_percent_test($test_id, $max) {
        $query = $this->db->query("SELECT SUM(sta.word_ok) as cantidad FROM students_tests st INNER JOIN 
        students_tests_answers sta ON sta.students_tests_id = st.id WHERE st.test_id = '{$test_id}' AND st.status = 2 
        GROUP BY st.student_id");
        $result = $query->result();
        $count_ok = $count_no = 0;
        foreach($result as $key => $item) {
            if($item->cantidad == $max) {
                $count_ok++;
            } else {
                $count_no++;
            }
        }

        $total = count($result);
        
        return array('ok' => ceil($count_ok * 100 / $total), 'no' => ceil($count_no * 100 / $total));
    }
    
    public function get_count_reports($teacher_id) {
        $query = $this->db->query("SELECT COUNT(st.id) as cantidad FROM students_tests st 
        INNER JOIN tests t ON st.test_id = t.id WHERE t.teacher_id = {$teacher_id}");
        return $query->result();
    }
    
    public function get_stats_by_field($key, $teacher_id, $value = 1, $word_id) {
        $query = $this->db->query("SELECT COUNT(sta.id) as valor FROM students_tests_answers sta LEFT JOIN students_tests st ON 
        sta.students_tests_id = st.id LEFT JOIN tests t on st.test_id = t.id WHERE 
        teacher_id = {$teacher_id} AND {$key} = {$value} AND sta.word_id = {$word_id}");
        return $query->result();
    }
    
    public function get_stats_by_field_no_value($key, $teacher_id, $word_id) {
        $query = $this->db->query("SELECT COUNT(sta.id) as valor FROM students_tests_answers sta LEFT JOIN students_tests st ON 
        sta.students_tests_id = st.id LEFT JOIN tests t on st.test_id = t.id WHERE 
        teacher_id = {$teacher_id} AND sta.word_id = {$word_id}");
        return $query->result();
    }
    
    public function get_count_reports_all($workspace_id) {
        $query = $this->db->query("SELECT COUNT(id) as cantidad FROM students_tests WHERE workspace_id = {$workspace_id}");
        return $query->result();        
    }
    
    public function get_sum_correctas($student_test_id) {
        $query = $this->db->query("SELECT SUM(word_ok) as cantidad FROM students_tests_answers WHERE students_tests_id = '{$student_test_id}'");
        $row = $query->row();
        return ($row ? $row->cantidad : 0);        
    }
    
    public function get_promedios_by_teacher($teacher_id, $dir = 'ASC') {
        $query = $this->db->query("SELECT w.word, word_id, SUM(word_ok)/COUNT(sta.id) as promedio, COUNT(sta.id) as count FROM 
        students_tests_answers sta INNER JOIN words w ON sta.word_id = w.id WHERE students_tests_id IN (SELECT st.id 
        FROM students_tests st INNER JOIN tests t ON st.test_id = t.id WHERE t.teacher_id = {$teacher_id}) GROUP BY word_id 
        ORDER BY promedio {$dir}");
        
        return $query->result();
    }
    
    public function get_promedios_all($dir = 'ASC', $workspace_id) {
        $query = $this->db->query("SELECT w.word, word_id, SUM(word_ok)/COUNT(sta.id) as promedio, COUNT(sta.id) as count FROM 
        students_tests_answers sta INNER JOIN words w ON sta.word_id = w.id WHERE sta.workspace_id = '{$workspace_id}' GROUP BY word_id 
        ORDER BY promedio {$dir}");
        
        return $query->result();        
    }
    
    public function update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('tests', $data);        
    }
    
    public function get_tests_by_year($teacher_id, $year) {
        $this->db->where('date_format(created, "%Y") = ', $year);
        $this->db->where('teacher_id', $teacher_id);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('tests')->result();
    }
    
    public function get_tests_by_year_and_roster($teacher_id, $year, $roster_id) {
        $this->db->where('date_format(created, "%Y") = ', $year);
        $this->db->where('roster_id', $roster_id); 
        $this->db->where('teacher_id', $teacher_id);
        return $this->db->get('tests')->result();
    }
    
    public function get_archives($teacher_id) {
        $this->db->select("DISTINCT(date_format(created, '%Y')), date_format(created, '%Y') as anio");
        $this->db->order_by('anio', 'DESC');
        $this->db->where('teacher_id', $teacher_id);
        return $this->db->get('tests')->result();
    }
    
    public function get_archives_by_roster($teacher_id, $roster_id) {
        $this->db->select("DISTINCT(date_format(created, '%Y')), date_format(created, '%Y') as anio");
        $this->db->order_by('anio', 'DESC');
        $this->db->where('teacher_id', $teacher_id);
        $this->db->where('roster_id', $roster_id);        
        return $this->db->get('tests')->result();
    }
    
    public function bann_student($test_id, $student_id) {
        $data = array('banned' => 1);
        $this->db->where('student_id', $student_id);
        $this->db->where('test_id', $test_id);
        $this->db->update('students_tests', $data);          
    }
    
    public function unbann_student($test_id, $student_id) {
        $data = array('banned' => 0);
        $this->db->where('student_id', $student_id);
        $this->db->where('test_id', $test_id);
        $this->db->update('students_tests', $data);          
    }
    
    public function fullscreen_off($student_id, $test_id) {
        $data = array('not_fullscreen' => 1);
        $this->db->where('student_id', $student_id);
        $this->db->where('test_id', $test_id);
        $this->db->update('students_tests', $data);        
    }
    
    public function end_test($student_id, $test_id) {
        $data = array('status' => 2);
        $this->db->where('student_id', $student_id);
        $this->db->where('test_id', $test_id);
        $this->db->update('students_tests', $data);        
    }
    
    public function start_test($roster_id, $teacher_id, $grade, $uid, $practice) {
        $this->db->insert('tests', 
            array(
                'roster_id' => $roster_id,
                'teacher_id' => $teacher_id,
                'status' => 1,
                'grade' => $grade,
                'workspace_id' => $uid,
                'practice' => $practice
            )
        );        
    }
    
    public function get_students_by_test($test_id) {
        $this->db->where('test_id', $test_id);     
        $this->db->order_by('id', 'ASC');   
        return $this->db->get('students_tests')->result();
    }
    
    public function get_last_test_by_grade($grade, $uid) {
        $this->db->order_by('created', 'DESC');
        $this->db->where('workspace_id', $uid);
        $this->db->where('grade', $grade);
        $this->db->where('status', 2);
        return $this->db->get('tests')->row();
    }
    
    public function get_student_test($test_id, $student_id) {
        $this->db->from('students_tests');
        $this->db->where('test_id', $test_id);        
        $this->db->where('student_id', $student_id);
        
        $row = $this->db->get()->row();
        return $row;
    }
    
    public function get_student_test_by_id($id) {
        $this->db->from('students_tests');
        $this->db->where('id', $id);
        $row = $this->db->get()->row();
        return $row;
    }

    public function get_all_student_test_by_year($year, $student_id) {
        $this->db->where('date_format(created, "%Y") = ', $year);
        $this->db->where('student_id', $student_id);
        $this->db->limit(4);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('students_tests')->result();
    }
    
    public function get_student_test_id($test_id, $student_id) {
        $this->db->from('students_tests');
        $this->db->where('test_id', $test_id);        
        $this->db->where('student_id', $student_id);
        
        $row = $this->db->get()->row();
        return (!$row ? null : $row->id);
    }
    
    public function is_test_finished($test_id, $student_id) {
        $this->db->from('students_tests');
        $this->db->where('test_id', $test_id);        
        $this->db->where('student_id', $student_id);
        
        $row = $this->db->get()->row();
        
        return ($row->status == 2);        
    }
    
    public function get_student_test_answer($student_test_id, $word_id) {
        $this->db->where('students_tests_id', $student_test_id);
        $this->db->where('word_id', $word_id);        
        return $this->db->get('students_tests_answers')->row_array();        
    }
    
    public function get_current_word_test($test_id, $student_id) {
        $student_test_id = $this->get_student_test_id($test_id, $student_id);
        if(!$student_test_id) return array('sti' => '', 'count' => 0);

        $this->db->from('students_tests_answers');
        $this->db->where('students_tests_id', $student_test_id);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return array('sti' => $student_test_id, 'count' => ($rowcount > 0 ? $rowcount: 0));
    }
    
    public function remove_current_answer($student_test_id, $word_id) {
        $this->db->where('students_tests_id', $student_test_id);
        $this->db->where('word_id', $word_id);
        $this->db->delete('students_tests_answers');         
    }
    
    public function remove_test($id) {
        $this->db->where('id', $id);
        $this->db->delete('tests');
        $this->db->where('test_id', $id);
        $this->db->delete('students_tests');
    }
    
    public function student_insert_answer($data) {
        $this->remove_current_answer($data['students_tests_id'], $data['word_id']);
        $this->db->insert('students_tests_answers', $data);
    }
    
    public function student_start_test($test_id, $student_id, $uid) {
        $this->db->insert('students_tests', 
            array(
                'test_id' => $test_id,
                'student_id' => $student_id,
                'status' => 1,
                'workspace_id' => $uid
            )
        );
        
        return $this->db->insert_id();
    }
    
    public function get_by_year($year, $teache_id) {
        $this->db->where('teacher_id', $teache_id);
        $this->db->where('date_format(created, "%Y") = ', $year);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('tests')->result();
    }
    
    public function get_by_roster_last($roster_id, $status = 0) {
        $this->db->where('roster_id', $roster_id);
        $this->db->where('status', $status);
        $this->db->limit(1);
        $this->db->order_by('created', 'DESC');
        return $this->db->get('tests')->row();         
    }
    
    public function get_by_roster($roster_id, $status = 0) {
        $this->db->where('roster_id', $roster_id);
        $this->db->where('status', $status);
        return $this->db->get('tests')->row();         
    }
    
    public function get($test_id) {
        $this->db->where('id', $test_id);
        return $this->db->get('tests')->row();         
    }
    
    public function get_active_test($teacher_id) {
        $this->db->where('teacher_id', $teacher_id);
        $this->db->where('status', 1);
        return $this->db->get('tests')->row();         
    }
    
}
