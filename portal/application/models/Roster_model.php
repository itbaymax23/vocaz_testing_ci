<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH.'third_party/PHPExcel/Classes/PHPExcel.php';

class Roster_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_next_roster($roster_id, $workspace_id) {
        
        $roster = $this->get($roster_id);
        if($roster->grade == 10) return -1;
        
        $next_grade = $roster->grade + 1;
        $newRoster = $this->get_by_grade_and_workspace($next_grade, $workspace_id);
        
        return ($newRoster ? $newRoster->id : 0);
    }
    
    public function get_by_grade_and_workspace($grade, $workspace_id) {

        $this->db->where('grade', $grade);
        $this->db->where('workspace_id', $workspace_id);
        return $this->db->get('rosters')->row();        
    }
    
    public function get_students_by_roster_id($roster_id) {
        $this->db->where('roster_id', $roster_id);
        $this->db->order_by('name', 'ASC');
        return $this->db->get('users')->result();         
    }
    
    public function get_rosters_by_teacher($teacher_id) {
        $this->db->where('teacher_id', $teacher_id);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('rosters')->result();         
    }
    
    public function get_rosters($workspace_id) {
        $this->db->where('workspace_id', $workspace_id);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('rosters')->result();        
    }
    
    public function get($id) {
        $this->db->where('id', $id);
        return $this->db->get('rosters')->row();        
    }
    
    public function get_student_by_username($username) {
        $this->db->where('username', $username);
        $user = $this->db->get('users')->row();
        
        return $user;        
    }
    
    public function get_student_by_email($email) {
        $this->db->where('email', $email);
        $user = $this->db->get('users')->row();
        
        return $user;        
    }
    
    public function reset_roster($id) {
        $this->db->where('roster_id', $id);
        $this->db->update('users', array('roster_id' => 0));
    }
    
    public function add_student_roster($data, $id, $grade) {
        
        $roster = $this->get($id);
        $student = $this->get_student_by_email($data['email']);
        $student2 = $this->get_student_by_username($data['username']);

        $ids = array();
        
        if(empty($data['username'])) {
            $data['username'] = 'user' . rand(100000, 999999);
        }
        
        if($student) {
            $ids[] = $student->id;
            
            if($student && $student2 && $student->id != $student2->id) {
                $uname = $data['username'] . rand(100000, 999999);
                while($this->get_student_by_username($uname)) {
                    $uname = $data['username'] . rand(100000, 999999);
                }
                 $data['username'] = $uname;
            }
            
            $this->db->where('id', (!empty($student) ? $student->id : $student2->id));
            $this->db->update('users', 
                array(
                    'name' => $data['fname'],
                    'lname' => $data['lname'],
                    'username' => $data['username'],
                    'email' => $data['email'],
                    'roster_id' => $id,
                    'grade' => $grade
                )
            );            
        } else {

            if($student && $student2 && $student->id != $student2->id) {
                $uname = $data['username'] . rand(100000, 999999);
                while($this->get_student_by_username($uname)) {
                    $uname = $data['username'] . rand(100000, 999999);
                }
                 $data['username'] = $uname;
            }
            
            $data['password'] = sha1(base_url() . $data['password']);
            
            $this->db->insert('users', 
                array(
                    'name' => $data['fname'],
                    'lname' => $data['lname'],
                    'roster_id' => $id,
                    'password' => sha1(base_url() . rand(10000, 99999)),
                    'email' => $data['email'],
                    'grade' => $grade,
                    'username' => $data['username'],
                    'type' => 'student',
                    'hash' => sha1(base_url() . $data['email']),
                    'workspace_id' => $roster->workspace_id
                )
            ); 
            
            $ids[] = $this->db->insert_id();            
        }
    }
    
    public function read_excel_roaster($filename) {
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $highestRow = $objWorksheet->getHighestRow(); 
        $highestColumn = $objWorksheet->getHighestColumn(); 
        
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        
        $rows = array();
        for ($row = 1; $row <= $highestRow; ++$row) {
            $rows[] = array(
                'fname' => $objWorksheet->getCellByColumnAndRow(0, $row)->getValue(),
                'lname' => $objWorksheet->getCellByColumnAndRow(1, $row)->getValue(),
                'email' => $objWorksheet->getCellByColumnAndRow(2, $row)->getValue(),
                'username' => $objWorksheet->getCellByColumnAndRow(3, $row)->getValue(),
            );
        }
        return $rows;        
    }
    
    public function update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('rosters', $data);        
    }
    
    public function get_by_teacher_id($teacher_id) {
        $this->db->where('teacher_id', $teacher_id);
        $user = $this->db->get('rosters')->row();
        
        return $user;        
    }
    
    public function get_teacher_by_id($id)
    {
        $this->db->where('id', $id);
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function get_teacher_by_email($email)
    {
        $this->db->where('email', $email);
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('rosters');
    }
    
    public function add($name, $teacher_id, $grade = 0, $uid)
    {
        $roster_used = $this->get_by_teacher_id($teacher_id);
        if($roster_used) {
            $this->update(array('teacher_id' => 0), $roster_used->id);
        }
        $this->db->insert('rosters', array('name' => $name, 'teacher_id' => $teacher_id, 'grade' => $grade, 'workspace_id' => $uid));
        return $this->db->insert_id();
    }
}
