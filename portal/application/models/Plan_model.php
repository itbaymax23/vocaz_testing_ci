<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'third_party/paypal/vendor/autoload.php';

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\Payee;

class Plan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        
        $this->client_id = 'AVdFN-_wStnXMtXG6wLvtxDLdWGVdqRHuvXQS9npNneqT4fR2pT5-kIeGMYXN4SOcZRGn-hZntN4qn0q';
        $this->client_secret = 'EOH9oUmdNGHLh7AMIV6d59jChr-bkWM2znLWoKhYg-r7hZ5uVQMoo0x9qSsESDCj45MgByxtx0jLbtrI';
        $this->currency = 'USD';
        $this->paypal_store_name = 'jgalarza';

        //set API context
        $this->apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential($this->client_id, $this->client_secret)
        );

        $this->apiContext->setConfig(
            array(
                'mode' => 'sandbox',
            )
        );
    }
    
    public function execute_item_payment() {
        $response = $this->input->get('success', true);
        $product_id = $this->input->get('product_id', true);
        $price = $this->input->get('price', true);
        $description = $this->input->get('description', true);  
        
        if ($response == false) {
            $this->session->set_flashdata('error', "Error in the payment");
            return false;
        }

        $paymentId = $this->input->get('paymentId', true);
        $payerId = $this->input->get('PayerID', true);

        $payment = Payment::get($paymentId, $this->apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        $transaction = new Transaction();
        $amount = new Amount();

        $amount->setCurrency($this->currency);
        $amount->setTotal($price);
        $transaction->setAmount($amount);
        $execution->addTransaction($transaction);

        try {
            $result = $payment->execute($execution, $this->apiContext);
            return $result;

        } catch (Exception $ex) {
            $this->session->set_flashdata('error', "Error in the payment");
            return false;
        }      
    }
    
    public function create_payment_item($product_id, $price, $description)
    {
        // $price = $price / 100;
        $this->apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential($this->client_id, $this->client_secret)
        );

        $this->apiContext->setConfig(
            array(
                'mode' => 'sandbox',
            )
        );
        
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");        
        

        // Set redirect URLs
        $redirectUrls = new RedirectUrls();
        $baseUrl = base_url();
        $redirectUrls->setReturnUrl($baseUrl . "admin_dashboard/suscription_plan?success=true&product_id=" . $product_id . "&price=" . $price . "&description=" . urlencode($description))
          ->setCancelUrl($baseUrl . "admin_dashboard/suscription_plan?success=false&product_id=" . $product_id . "&price=" . $price . "&description=" . urlencode($description));
        
        // Set payment amount
        $amount = new Amount();
        $amount->setCurrency("USD")
          ->setTotal($price);
          
        $transaction = new Transaction();
        $transaction->setAmount($amount)
          ->setDescription($description)
          ->setInvoiceNumber(uniqid());
        
        // Create the full payment object
        $payment = new Payment();
        $payment->setIntent('sale')
          ->setPayer($payer)
          ->setRedirectUrls($redirectUrls)
          ->setTransactions(array($transaction));
  
        try {
          $payment->create($this->apiContext);
        
          // Get PayPal redirect URL and redirect the customer
          $approvalUrl = $payment->getApprovalLink();
        
          // Redirect the customer to $approvalUrl
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
          echo $ex->getCode();
          echo $ex->getData();
          die($ex);
        } catch (Exception $ex) {
          die($ex);
        } 
        
        return $approvalUrl; 
    }
    
    public function add($data) {
        $this->db->insert('subscriptions', $data);
        return $this->db->insert_id();
    }
    
    public function get_current_suscription($workspace_id) {
        $this->db->where('workspace_id', $workspace_id);
        return $this->db->get('subscriptions')->row();
    }
    
    public function get_current_trials() {
        return $this->db->get('trial')->result();
    }
    
    public function get_trial($workspace_id) {
        $this->db->where('workspace_id', $workspace_id);
        return $this->db->get('trial')->row();
    }
    
    public function delete_trials() {
        $this->db->empty_table('trial'); 
    }
    
    public function get_option($key) {
        $this->db->where('option', $key);
        return $this->db->get('options')->row();
    }
    
    public function delete_option($key) {
        $this->db->where('option', $key);
        $this->db->delete('options');
    }
    
    public function set_option($key, $value) {
        $this->delete_option($key);
        $this->db->insert('options', array('option' => $key, 'value' => $value));
    }
    
    public function add_trial($trial_days, $workspace_id) {
        //$this->delete_trials();
        $this->db->insert('trial', 
        array(
            'start_date' => date("Y-m-d H:i:s"), 
            'end_date' => date("Y-m-d H:i:s", strtotime("+{$trial_days} days")),
            'workspace_id' => $workspace_id
            )
        );
    }

    public function get_suscription($id) {
        $this->db->where('owner_id', $id);
        return $this->db->get('subscriptions')->row();
    }

    public function get_all_plans() {
        return $this->db->get('plans')->result();
    }

    public function get_plan_by_id($id) {
        $this->db->where('id', $id);
        return $this->db->get('plans')->row();
    }

    public function get_plan_by_name($key) {
        $this->db->where('plan_name', $key);
        return $this->db->get('plans')->row();
    }

    public function update_plan($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('plans', $data);        
    }
}
