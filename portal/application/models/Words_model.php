<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Words_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('words', $data);        
    }
    
    public function get_all_by_level($level) {
        $this->db->where('level', $level);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('words')->result();         
    }
    
    public function get_workspace_audio($workspace_id, $word_id) {
        $this->db->where('teacher_id', $workspace_id);
        $this->db->where('word_id', $word_id);
        $row = $this->db->get('words_audios')->row();
        return $row;        
    }
    
    public function get_audio_teacher($teacher_id, $word_id, $workspace_id) {
        $this->db->where('teacher_id', $teacher_id);
        $this->db->where('word_id', $word_id);
        $row = $this->db->get('words_audios')->row();
        if(!$row) {
            $row = $this->get_workspace_audio($workspace_id, $word_id);
        }
        
        return $row;
    }
    
    public function get_all_superadmin($teacher_id = null, $workspace_id = null) {
        $level1 = $this->get_all_by_level(1);  
        $level2 = $this->get_all_by_level(2);
        $level3 = $this->get_all_by_level(3);
        
        return array('level1' => $level1, 'level2' => $level2, 'level3' => $level3);    
    }
    
    public function get_all($teacher_id = null, $workspace_id = null) {
        $level1 = $this->get_all_by_level(1);  
        $level2 = $this->get_all_by_level(2);
        $level3 = $this->get_all_by_level(3);
        
        if($teacher_id != null) {
            foreach($level1 as $key => $item) {
                //$level1[$key]->audio = "";
                $audio = $this->get_audio_teacher($teacher_id, $item->id, $workspace_id);
                if($audio) {
                    $level1[$key]->audio = $audio->audio;
                    $level1[$key]->custom = true;
                    $level1[$key]->audio_id = $audio->id;
                } else {
                    $level1[$key]->custom = false;
                }
            }
            foreach($level2 as $key => $item) {
                //$level2[$key]->audio = "";
                $audio = $this->get_audio_teacher($teacher_id, $item->id, $workspace_id);
                if($audio) {
                    $level2[$key]->audio = $audio->audio;
                    $level2[$key]->custom = true;
                    $level2[$key]->audio_id = $audio->id;
                } else {
                    $level2[$key]->custom = false;
                }
            }
            foreach($level3 as $key => $item) {
                //$level3[$key]->audio = "";
                $audio = $this->get_audio_teacher($teacher_id, $item->id, $workspace_id);
                if($audio) {
                    $level3[$key]->audio = $audio->audio;
                    $level3[$key]->audio_id = $audio->id;
                    $level3[$key]->custom = true;
                } else {
                    $level3[$key]->custom = false;
                }
            }            
        }
        
        return array('level1' => $level1, 'level2' => $level2, 'level3' => $level3);    
    }
    
    public function get_words_by_level_array($level) {
        $this->db->where('level', $level);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('words')->result_array();        
    }
    
    public function get_word_array($id) {
        $this->db->where('id', $id);
        return $this->db->get('words')->row_array();        
    }
    
    public function remove_audio($id) {
        $this->db->where('id', $id);
        $this->db->delete('words_audios');        
    }
    
    public function remove_audio_teacher($word_id, $teacher_id) {
        $this->db->where('teacher_id', $teacher_id);
        $this->db->where('word_id', $word_id);
        $this->db->delete('words_audios');        
    }
    
    public function add_audio_teacher($audio, $teacher_id, $word_id) {
        $this->remove_audio_teacher($teacher_id, $word_id);
        $this->db->insert('words_audios', array('audio' => $audio, 'teacher_id' => $teacher_id, 'word_id' => $word_id));
    }   
}
