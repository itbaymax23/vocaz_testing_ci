<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Authentication_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('users', $data);        
    }
    
    public function get_school($school_id) {
        $this->db->where('school_id', $school_id);
        return $this->db->get('users')->row();
    }
    
    public function login_school($email, $password, $school_id) {
        $hashpass = sha1(base_url() . $password);
        
        $school_id = intval($school_id);
        $school_id = str_pad($school_id, 4, '0', STR_PAD_LEFT);
        
        $school = $this->get_school($school_id);
        
        $this->db->where('email', $email)->where("(school_id = '{$school_id}' OR workspace_id = '{$school->id}')");
        $user = $this->db->get('users')->row();
        
        if(!$user) {
            $this->db->where('username', $email)->where("(school_id = '{$school_id}' OR workspace_id = '{$school->id}')");
            $user = $this->db->get('users')->row();            
        }
        
        if(!$user) return -1;
        
        if($user->password != $hashpass) {
            return -2;
        }
        
        return $user;        
    }

    public function login($email, $password)
    {
        $hashpass = sha1(base_url() . $password);
        
        $this->db->where('email', $email);
        $user = $this->db->get('users')->row();
        
        if(!$user) {
            $this->db->where('username', $email);
            $user = $this->db->get('users')->row();            
        }
        
        if(!$user) return -1;
        
        if($user->password != $hashpass) {
            return -2;
        }
        
        return $user;
    }
    
    public function get_schools_by_term($term) {
        $this->db->like('name', $term);
        return $this->db->get('users')->result();
    }
    
    public function reset_password($user_id, $newpassword) {
        
        $hashpass = sha1(base_url() . $newpassword);
        
        $data = array('password' => $hashpass);
        
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);        
    }
    
    public function set_school_id($school_id, $user_id) {
        $this->db->where('id', $user_id);
        $this->db->update('users', array('school_id' => $school_id));        
    }
    
    public function get_user_by_username($username)
    {
        $this->db->where('username', $username);
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function get_user_by_username_and_wid($username, $workspace_id)
    {
    
        $school_id = intval($workspace_id);
        $school_id = str_pad($school_id, 4, '0', STR_PAD_LEFT);
        
        $school = $this->get_school($school_id);
        
        $this->db->where('username', $username)->where("(school_id = '{$school_id}' OR workspace_id = '{$school->id}')");

        $user = $this->db->get('users')->row();
        
        return $user;
    }

    public function get_user_by_email($email)
    {
        $this->db->where('email', $email);
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function get_user_by_email_and_wid($email, $workspace_id)
    {
        $school_id = intval($workspace_id);
        $school_id = str_pad($school_id, 4, '0', STR_PAD_LEFT);
        
        $school = $this->get_school($school_id);
        
        $this->db->where('email', $email)->where("(school_id = '{$school_id}' OR workspace_id = '{$school->id}')");
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function get_user_by_id($id)
    {
        $this->db->where('id', $id);
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function get_users_by_type($type) {
        $this->db->where('type', $type);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('users')->result();       
    }
    
    public function get_count_by_type($type, $workspace_id) {
        $this->db->from('users');
        $this->db->where('workspace_id', $workspace_id);
        $this->db->where('type', $type);           
        return $this->db->count_all_results();;        
    }
    
    public function get_user_by_hash_and_password($hash, $password)
    {
        $this->db->where('hash', $hash);
        $this->db->where('password', $password);
        $user = $this->db->get('users')->row();
        
        return $user;
    }

    /**
     * @param  boolean If Client or Staff
     * @return none
     */
    public function logout($staff = true)
    {
        $this->delete_autologin($staff);
        if (is_client_logged_in()) {
            do_action('before_client_logout', get_client_user_id());
            $this->session->unset_userdata('client_user_id');
            $this->session->unset_userdata('client_logged_in');
        } else {
            do_action('before_staff_logout', get_client_user_id());
            $this->session->unset_userdata('staff_user_id');
            $this->session->unset_userdata('staff_logged_in');
        }
        $this->session->sess_destroy();
    }
}
