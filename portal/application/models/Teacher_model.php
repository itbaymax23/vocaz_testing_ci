<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Teacher_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function update_teacher($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('users', $data);        
    }
    
    public function get_all_teachers($uid) {
        $this->db->where('type', 'teacher');
        $this->db->where('workspace_id', $uid);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('users')->result();
    }
    
    
    public function get_teacher_by_id($id)
    {
        $this->db->where('id', $id);
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function get_teacher_by_email($email)
    {
        $this->db->where('email', $email);
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function get_teacher_by_email_and_ws($email, $workspace_id)
    {
        $this->db->where('email', $email);
        $this->db->where('workspace_id', $workspace_id);
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function delete_teacher($id) {
        $this->db->where('id', $id);
        $this->db->delete('users');
    }
    
    public function add_teacher($data)
    {
        //$existe = $this->get_teacher_by_email_and_ws($data['email'], $workspace_id);
        //if($existe) return -1;
        
        $data['password'] = sha1(base_url() . $data['password']);
        
        $this->db->insert('users', $data);

        return $this->db->insert_id();
    }
}
