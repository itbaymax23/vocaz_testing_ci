<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Student_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function update_student($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }
    
    public function get_students_by_roster($roster_id) {
        $this->db->where('type', 'student');
        $this->db->where('roster_id', $roster_id);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('users')->result();
    }
    
    public function get_all_archived_students($workspace_id) {
        $this->db->where('type', 'student');
        $this->db->where('archived', 1);
        $this->db->where('workspace_id', $workspace_id);
        return $this->db->get('users')->result();
    }
    
    public function get_all_students($workspace_id) {
        $this->db->where('type', 'student');
        $this->db->where('archived', 0);
        $this->db->where('workspace_id', $workspace_id);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('users')->result();
    }
    
    public function get_all_students_by_teacher($roster_id) {
        $this->db->where('type', 'student');
        $this->db->where('roster_id', $roster_id);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('users')->result();
    }
    
    public function get_student_by_id($id) {
        $this->db->where('id', $id);
        $user = $this->db->get('users')->row();
        
        return $user;        
    }
    
    public function get_student_by_email($email)
    {
        $this->db->where('email', $email);
        $user = $this->db->get('users')->row();
        
        return $user;
    }
    
    public function delete_student($id) {
        $this->db->where('id', $id);
        $this->db->delete('users');
    }
    
    public function add_student($data, $is_admin = false)
    {
        if(!$is_admin) {
            // $existe = $this->get_student_by_email($data['email']);
            // if($existe) return -1;            
        }
        
        $data['password'] = sha1(base_url() . $data['password']);
        
        $this->db->insert('users', $data);

        return $this->db->insert_id();
    }
}
