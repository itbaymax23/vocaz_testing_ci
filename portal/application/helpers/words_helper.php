<?php

defined('BASEPATH') or exit('No direct script access allowed');

function get_consonants($word) {
    return preg_replace('#[aeiou\s]+#i', '', $word);
}

function letters_separated($letters, $word) {
    if(strpos($letters, '-') !== FALSE) {
        preg_match('/'.str_replace('-', '(.*?)', $letters).'/', $word, $res);
        if(!$res) return false;
        return (strlen($res[1]) > 0 ? true : false);        
    } else {
        return (strpos($word, $letters) !== FALSE ? 1 : 0);
    }
}

function sortByOrder($a, $b) {
    return $b['value'] - $a['value'];
}

function sortByOrderLabel($a, $b) {
    return $b['label'] - $a['label'];
}

function get_name_field($key) {
    switch($key) {
        case 'wo1_beginning': return 'Beggining Consonant'; break;
        case 'wo1_final': return 'Final Consonant'; break;
        case 'wo1_short': return 'Short Vowels'; break;
        case 'wo1_consonant': return 'Consonant Digraphs'; break;
        case 'wo1_conblends': return 'Consonant Blends'; break;
        case 'wo1_longvow': return 'Long Vowel Patterns'; break;
        case 'wo1_othervow': return 'Other Vowel Patterns'; break;
        case 'wo1_inflected': return 'Inflected Endings'; break;
        
        case 'wo2_beginning': return 'Beggining Consonant'; break;
        case 'wo2_final': return 'Final Consonant'; break;
        case 'wo2_short': return 'Short Vowels'; break;
        case 'wo2_consonant': return 'Consonant Digraphs'; break;
        case 'wo2_conblends': return 'Consonant Blends'; break;
        case 'wo2_longvow': return 'Long Vowel Patterns'; break;
        case 'wo2_othervow': return 'Other Vowel Patterns'; break;
        case 'wo2_inflected': return 'Inflected Endings'; break;
        case 'wo2_syllable': return 'Syllable Junctures'; break;
        case 'wo2_unacented': return 'Unaccented Syllables'; break;
        case 'wo2_harder': return 'Harder Suffixes'; break;
        case 'wo2_base': return 'Base - Root - Derivative'; break;
        
        case 'wo3_blends': return 'Blends and Digraphs'; break;
        case 'wo3_vowels': return 'Vowels'; break;
        case 'wo3_complex': return 'Complex Consonants'; break;
        case 'wo3_inflected': return 'Inflected Endings - Syllable Juncture'; break;
        case 'wo3_unnacented': return 'Unaccented Final Syllables Affixes'; break;
        case 'wo3_affixes': return 'Affixes'; break;
        case 'wo3_reduced': return 'Reduced Vowels in Unaccented Syllables'; break;
        case 'wo3_greek': return 'Greek &amp; Latin Elements'; break;
        case 'wo3_assimilated': return 'Assimilated Prefixes'; break;             
        
    }
}

function get_puntaje_word($input, $level, $word) {
    $puntaje = array();
    $input = strtolower($input);
    
    $level = intval($level);

    $puntaje['word_ok'] = ($input == $word['word'] ? 1 : 0);
    if($level == 1) {
        if(!empty($word['wo1_beginning'])) {
            $puntaje['wo1_beginning'] = (substr($input, 0, 1) == $word['wo1_beginning'] ? 1 : 0);    
        }
        if(!empty($word['wo1_final'])) {
            $input_aux = get_consonants($input);
            $puntaje['wo1_final'] = (substr($input_aux, -1) == $word['wo1_final'] ? 1 : 0);
        }
        if(!empty($word['wo1_short'])) {
            $puntaje['wo1_short'] = (strpos($input, $word['wo1_short']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo1_consonant'])) {
            $puntaje['wo1_consonant'] = (strpos($input, $word['wo1_consonant']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo1_conblends'])) {
            $puntaje['wo1_conblends'] = (strpos($input, $word['wo1_conblends']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo1_longvow'])) {
            $puntaje['wo1_longvow'] = (letters_separated($word['wo1_longvow'], $input) ? 1 : 0);
        }
        if(!empty($word['wo1_othervow'])) {
            $puntaje['wo1_othervow'] = (strpos($input, $word['wo1_othervow']) !== FALSE ? 1 : 0);
        }       
        if(!empty($word['wo1_inflected'])) {
            $puntaje['wo1_inflected'] = (substr($input, -strlen($word['wo1_inflected'])) == $word['wo1_inflected'] ? 1 : 0);
        }
    }
    
    if($level == 2) {
        if(!empty($word['wo2_beginning'])) {
            $puntaje['wo2_beginning'] = (substr($input, 0, 1) == $word['wo2_beginning'] ? 1 : 0);
        }
        if(!empty($word['wo2_final'])) {
            $input_aux = get_consonants($input);
            $puntaje['wo2_final'] = (substr($input_aux, -1) == $word['wo2_final'] ? 1 : 0);
        }
        if(!empty($word['wo2_short'])) {
            $puntaje['wo2_short'] = (strpos($input, $word['wo2_short']) !== FALSE ? 1 : 0);
        } 
        if(!empty($word['wo2_consonant'])) {
            $puntaje['wo2_consonant'] = (strpos($input, $word['wo2_consonant']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo2_othervow'])) {
            $puntaje['wo2_othervow'] = (strpos($input, $word['wo2_othervow']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo2_conblends'])) {
            $puntaje['wo2_conblends'] = (strpos($input, $word['wo2_conblends']) !== FALSE ? 1 : 0);
        }  
        if(!empty($word['wo2_longvow'])) {
            $puntaje['wo2_longvow'] = (letters_separated($word['wo2_longvow'], $input) ? 1 : 0);
        } 
        if(!empty($word['wo2_inflected'])) {
            $puntaje['wo2_inflected'] = (substr($input, -strlen($word['wo2_inflected'])) == $word['wo2_inflected'] ? 1 : 0);
        }
        if(!empty($word['wo2_syllable'])) {
            $puntaje['wo2_syllable'] = (strpos($input, $word['wo2_syllable']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo2_unacented'])) {
            $puntaje['wo2_unacented'] = (substr($input, -strlen($word['wo2_unacented'])) == $word['wo2_unacented'] ? 1 : 0);
        }
        if(!empty($word['wo2_harder'])) {
            $puntaje['wo2_harder'] = (substr($input, -strlen($word['wo2_harder'])) == $word['wo2_harder'] ? 1 : 0);
        }
        if(!empty($word['wo2_base'])) {
            $puntaje['wo2_base'] = (strpos($input, $word['wo2_base']) !== FALSE ? 1 : 0);
        }
    }
    
    if($level == 3) {
        if(!empty($word['wo3_blends'])) {
            $puntaje['wo3_blends'] = (strpos($input, $word['wo3_blends']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo3_vowels'])) {
            $puntaje['wo3_vowels'] = (letters_separated($word['wo3_vowels'], $input) ? 1 : 0);
        }
        if(!empty($word['wo3_complex'])) {
            $puntaje['wo3_complex'] = (strpos($input, $word['wo3_complex']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo3_inflected'])) {
            $puntaje['wo3_inflected'] = (strpos($input, $word['wo3_inflected']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo3_unnacented'])) {
            $puntaje['wo3_unnacented'] = (substr($input, -strlen($word['wo3_unnacented'])) == $word['wo3_unnacented'] ? 1 : 0);
        }
        if(!empty($word['wo3_affixes'])) {
            $puntaje['wo3_affixes'] = (strpos($input, $word['wo3_affixes']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo3_reduced'])) {
            $puntaje['wo3_reduced'] = (strpos($input, $word['wo3_reduced']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo3_greek'])) {
            $puntaje['wo3_greek'] = (strpos($input, $word['wo3_greek']) !== FALSE ? 1 : 0);
        }
        if(!empty($word['wo3_assimilated'])) {
            $puntaje['wo3_assimilated'] = (strpos($input, $word['wo3_assimilated']) !== FALSE ? 1 : 0);
        }
    }
    return $puntaje;
}

//$res = get_puntaje_word($input, $level, $item);//result_array()
