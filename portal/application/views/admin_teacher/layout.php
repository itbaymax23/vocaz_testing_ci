<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>assets/gentelella-master/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>assets/gentelella-master/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url();?>assets/gentelella-master/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>assets/gentelella-master/build/css/custom.min.css" rel="stylesheet">
  </head>
  
  <?php echo $content;?>
  
  <style>
    #subscription-status {
        float: left;
        font-size: 20px;
        position: absolute;
        top: 16px;
    }
  </style>
  <script>

    <?php if($subscription && strtotime($subscription->end_date) > time()):?>
        var text = "Subscription active until: <?php echo date("F d, Y", strtotime($subscription->end_date));?>";
    <?php else:?>
        <?php if($trial && strtotime($trial->end_date) > time()):?>
            var text = "Your have a trial until: <?php echo date("F d, Y", strtotime($trial->end_date));?>"
        <?php else:?>
            var text = "You are not subscribed";
        <?php endif;?>
    <?php endif;?>
    $(function() {
        $('<ul class="nav navbar-nav navbar-left"><li id="subscription-status">'+text+'</li></ul>').insertBefore('.top_nav .navbar-right'); 
    });
  </script>  
  </html>