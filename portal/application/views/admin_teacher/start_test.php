  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url();?>teacher_dashboard" class="site_title"><i class="fa fa-envelope"></i> <span style="font-size: 18px;">Vocab Management</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $user->name;?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php echo $sidebar;?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a href="<?php echo base_url();?>teacher_settings" data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>admin/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt=""><?php echo $user->name;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="<?php echo base_url();?>teacher_settings">
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <style>
            .top_titles {
                text-align: center;
            }
            .tile-stats {
                text-align: center;
                padding: 15px;
            }
            .tile-stats .count {
                font-size: 24px;
            }
        </style>
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
              <h3 class="text-center">Select Vocab Test</h3>  
            </div>
            <p>&nbsp;</p>
            <div class="row top_tiles" style="text-align: center;">
              <?php if($teacher->level == 1):?>  
                  <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12" style="float: inherit;display: inline-block;">
                    <div class="tile-stats title-stats-1">
                      <div class="count">TK - Kinder</div>
                      <p>&nbsp;</p>
                      <?php if($test && $test->status < 2):?>
                        <a class="btn btn-danger" href="javascript:;" onclick="StopTest(<?php echo $tid;?>)">Stop Test</a>
                        <p>&nbsp;</p>
                        <a class="btn btn-info" href="<?php echo base_url();?>teacher_students_loggedin"><i class="fa fa-user"></i> View Students Logged In</a>
                      <?php else:?>
                        <a class="btn btn-success" href="javascript:;" onclick="StartTest(<?php echo $roster->id;?>)">Start Test</a>
                        <p>&nbsp;</p>
                        <a class="btn btn-primary" href="javascript:;" onclick="StartPracticeTest(<?php echo $roster->id;?>)">Start Practice Test</a>
                        <div class="checkbox" style="display: none;">
                        <label>
                          <input type="checkbox" id="practice"> Practice Test
                        </label>
                        </div>
                      <?php endif;?>
                    </div>
                  </div>
              <?php endif;?>
              
              <?php if($teacher->level == 2):?>  
                  <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12" style="float: inherit;display: inline-block;">
                    <div class="tile-stats title-stats-1">
                      <div class="count">1st - 3th Grade</div>
                      <p>&nbsp;</p>
                      <?php if($test && $test->status < 2):?>
                        <a class="btn btn-danger" href="javascript:;" onclick="StopTest(<?php echo $tid;?>)">Stop Test</a>
                        <p>&nbsp;</p>
                        <a class="btn btn-info" href="<?php echo base_url();?>teacher_students_loggedin"><i class="fa fa-user"></i> View Students Logged In</a>
                      <?php else:?>
                        <a class="btn btn-success" href="javascript:;" onclick="StartTest(<?php echo $roster->id;?>)">Start Test</a>
                        <p>&nbsp;</p>
                        <a class="btn btn-primary" href="javascript:;" onclick="StartPracticeTest(<?php echo $roster->id;?>)">Start Practice Test</a>
                        <div class="checkbox" style="display: none;">
                        <label>
                          <input type="checkbox" id="practice"> Practice Test
                        </label>
                        </div>
                      <?php endif;?>
                    </div>
                  </div>
              <?php endif;?>
              
              <?php if($teacher->level == 3):?>  
                  <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12" style="float: inherit;display: inline-block;">
                    <div class="tile-stats title-stats-1">
                      <div class="count">4th - 8th Grade</div>
                      <p>&nbsp;</p>
                      <?php if($test && $test->status < 2):?>
                        <a class="btn btn-danger" href="javascript:;" onclick="StopTest(<?php echo $tid;?>)">Stop Test</a>
                        <p>&nbsp;</p>
                        <a class="btn btn-info" href="<?php echo base_url();?>teacher_students_loggedin"><i class="fa fa-user"></i> View Students Logged In</a>
                      <?php else:?>
                        <a class="btn btn-success" href="javascript:;" onclick="StartTest(<?php echo $roster->id;?>)">Start Test</a>
                        <p>&nbsp;</p>
                        <a class="btn btn-primary" href="javascript:;" onclick="StartPracticeTest(<?php echo $roster->id;?>)">Start Practice Test</a>
                        <div class="checkbox" style="display: none;">
                        <label>
                          <input type="checkbox" id="practice"> Practice Test
                        </label>
                        </div>
                      <?php endif;?>
                    </div>
                  </div>
              <?php endif;?>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved - &COPY; <?php echo date("Y");?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/gentelella-master/build/js/custom.min.js"></script>
    
    <script>
        function StartPracticeTest(roster_id) {
            $('#practice').prop('checked', true);
            StartTest(roster_id);
        }
    
        function StartTest(roster_id) {
            if(!confirm('Sure you want to Start a Test now?')) return;
             practice = ($('#practice').prop('checked') == true ? 1 : 0);
             $.post( "<?php echo base_url();?>teacher_dashboard/ajax_start_test", {roster_id: roster_id, practice: practice}, function( data ) {
    
             }, "json").done(function( data ) {
                location.reload();
             }); 
        }
        
        function StopTest(test_id) {
            if(!confirm('Sure you want to Stop the Test now?')) return;
             $.post( "<?php echo base_url();?>teacher_dashboard/ajax_stop_test", {test_id: test_id}, function( data ) {
    
             }, "json").done(function( data ) {
                location.reload();
             }); 
        }
    </script>
	
  </body>