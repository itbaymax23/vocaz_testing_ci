<table class="table-100" style="max-width: 1200px; margin: auto;">
    <tr>
        <td>
            <table class="table-100">
                <tr style="padding-bottom: 10px;">
                    <td colspan="11">
                        <h3>Name: <?php echo $student->name;?> <?php echo $student->lname;?></h3>
                    </td>
                </tr>
                <tr class="tr-rotates">
                    <td style="width: 150px;text-align: center;" class="borderer bg-grey pad-value">STAGES</td>
                    <td class="borderer bg-grey pad-value" style="text-align: center;" colspan="3">Letter Name<br />Alphabetic</td>
                    <td class="borderer bg-grey pad-value" style="text-align: center;" colspan="3">Within Word<br />Pattern</td>
                    <td class="borderer bg-grey pad-value" style="text-align: center;" colspan="2">Syllables and<br />Affixes</td>
                    <td class="bg-grey borderer pad-value" style="text-align: center;">TOTALS</td>
                </tr>
                <tr style="height: 50px;" class="tr-rotates">
                    <td class="borderer bg-grey"></td>
                    <td class="borderer bg-grey pad-value">late</td>
                    <td class="borderer bg-grey pad-value">early</td>
                    <td class="borderer bg-grey pad-value">mid</td>
                    <td class="borderer bg-grey pad-value">late</td>
                    <td class="borderer bg-grey pad-value">early</td>
                    <td class="borderer bg-grey pad-value">mid</td>
                    <td class="borderer bg-grey pad-value">late</td>
                    <td class="borderer bg-grey pad-value">early</td>
                    <td class="borderer bg-grey pad-value">

                    </td>
                </tr>
                
                <tr style="height: 120px;" class="tr-rotates">
                    <td class="borderer"></td>
                    <td class="borderer pad-value"><span class="n90-grados">Beginning Consonant</span></td>
                    <td class="borderer pad-value"><span class="n90-grados">Final Consonant</span></td>
                    <td class="borderer pad-value"><span class="n90-grados">Short Vowels</span></td>
                    <td class="borderer pad-value"><span class="n90-grados">Consonant Digraphs</span></td>
                    <td class="borderer pad-value"><span class="n90-grados">Consonant Blends</span></td>
                    <td class="borderer pad-value"><span class="n90-grados">Long Vowels<br />Patterns</span></td>
                    <td class="borderer pad-value"><span class="n90-grados">Other Vowels<br />Patterns</span></td>
                    <td class="borderer pad-value"><span class="n90-grados">Inflected Endings</span></td>
                    <td class="borderer">
                        <table class="table-100">
                            <tr>
                                <td class="borderer pad-value">Featured<br />Point</td>
                                <td class="borderer pad-value">Correct<br />Spelling</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php foreach($words as $key => $item):?>
                <tr style="height: 30px;" class="<?php echo ($key % 2 ? 'bg-grey' : '');?>">
                    <td class="borderer pad-value" style="width: 150px;"><?php echo ($key + 1);?> - <?php echo $item['word'];?></td>
                    <td class="borderer pad-value"><?php echo $item['wo1_beginning'];?></td>
                    <td class="borderer pad-value"><?php echo $item['wo1_final'];?></td>
                    <td class="borderer pad-value"><?php echo $item['wo1_short'];?></td>
                    <td class="borderer pad-value"><?php echo $item['wo1_consonant'];?></td>
                    <td class="borderer pad-value"><?php echo $item['wo1_conblends'];?></td>
                    <td class="borderer pad-value"><?php echo $item['wo1_longvow'];?></td>
                    <td class="borderer pad-value"><?php echo $item['wo1_othervow'];?></td>
                    <td class="borderer pad-value"><?php echo $item['wo1_inflected'];?></td>
                    <td class="borderer">
                        <table class="table-100">
                            <tr>
                                <td>
                                    <table class="table-100">
                                        <tr>
                                            <td style="height: 30px;" class="borderer"><?php echo $results['featured_point0'][$key];?></td>
                                            <td style="height: 30px;" class="borderer"><?php echo $results['featured_point1'][$key];?></td>
                                            <td style="height: 30px;" class="borderer"><?php echo $results['featured_point2'][$key];?></td>
                                            <td style="height: 30px;" class="borderer"><?php echo $results['featured_point3'][$key];?></td>
                                        </tr>
                                    </table>                                    
                                </td>
                                <td>
                                    <table class="table-100">
                                        <tr>
                                            <td style="height: 30px;" class="borderer"><?php echo $results['correct_spelling0'][$key];?></td>
                                            <td style="height: 30px;" class="borderer"><?php echo $results['correct_spelling1'][$key];?></td>
                                            <td style="height: 30px;" class="borderer"><?php echo $results['correct_spelling2'][$key];?></td>
                                            <td style="height: 30px;" class="borderer"><?php echo $results['correct_spelling3'][$key];?></td>
                                        </tr>
                                    </table>                                     
                                </td>
                            </tr>
                        </table>                        
                    </td>
                </tr>                    
                <?php endforeach;?>                                

                <tr style="height: 30px;">
                    <td class="borderer pad-value">TOTALS</td>
                    <td class="borderer pad-value"><?php echo $results['wo1_beginning_total'];?></td>
                    <td class="borderer pad-value"><?php echo $results['wo1_final_total'];?></td>
                    <td class="borderer pad-value"><?php echo $results['wo1_short_total'];?></td>
                    <td class="borderer pad-value"><?php echo $results['wo1_consonant_total'];?></td>
                    <td class="borderer pad-value"><?php echo $results['wo1_conblends_total'];?></td>
                    <td class="borderer pad-value"><?php echo $results['wo1_longvow_total'];?></td>
                    <td class="borderer pad-value"><?php echo $results['wo1_othervow_total'];?></td>
                    <td class="borderer pad-value"><?php echo $results['wo1_inflected_total'];?></td>
                    <td class="borderer">
                        <table class="table-100">
                            <tr>
                                <td>
                                    <table class="table-100">
                                        <tr>
                                            <td class="borderer-right pad-value"><?php echo $results['featured_point0_total'];?></td>
                                            <td class="borderer-right pad-value"><?php echo $results['featured_point1_total'];?></td>
                                            <td class="borderer-right pad-value"><?php echo $results['featured_point2_total'];?></td>
                                            <td class="borderer-right pad-value"><?php echo $results['featured_point3_total'];?></td>
                                        </tr>
                                    </table>                                    
                                </td>
                                <td>
                                    <table class="table-100">
                                        <tr>
                                            <td class="borderer-right pad-value"><?php echo $results['correct_spelling0_total'];?></td>
                                            <td class="borderer-right pad-value"><?php echo $results['correct_spelling1_total'];?></td>
                                            <td class="borderer-right pad-value"><?php echo $results['correct_spelling2_total'];?></td>
                                            <td class="borderer-right pad-value"><?php echo $results['correct_spelling3_total'];?></td>
                                        </tr>
                                    </table>                                     
                                </td>
                            </tr>
                        </table>                          
                    </td>
                </tr>
                                                                            
            </table>
        </td>
    </tr>
</table>