  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url();?>teacher_dashboard" class="site_title"><i class="fa fa-envelope"></i> <span style="font-size: 18px;">Vocab Management</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $user->name;?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php echo $sidebar;?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a href="<?php echo base_url();?>teacher_settings" data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>admin/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt=""><?php echo $user->name;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="<?php echo base_url();?>teacher_settings">
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <style>
            .title-stats-1 {
                background-color: #6dc463;
                color: #fff;
            }
            .title-stats-1 .count {
                margin: 44px 0 45px 10px;
            }
            .title-stats-2 {
                background-color: #f1c40f;
                color: #fff;
            }
            .title-stats-3 {
                background-color: #3598db;
                color: #fff;
            }
            
            .tile-stats h3 {
                color: #fff;
            }
            
            .title-stats-1, .title-stats-2, .title-stats-3 {
                text-align: center;
            }
            .title-stats-1 .icon, .title-stats-2 .icon, .title-stats-3 .icon {
                width: auto;
                height: 28px;
                color: #BAB8B8;
                position: relative;
                right: inherit;
                top: inherit;
                z-index: 1;
                margin: 18px auto;
                color: #fff;
            }
        </style>
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-2">
                    
                </div>
                <div class="col-md-8 text-center">
                    <div class="page-title">
                      <h1 class="text-center">Archive</h1>  
                      <br />
                      <form action="" method="post" name="frmYear">
                        <select class="form-control" name="year" onchange="document.frmYear.submit()">
                            <?php foreach($archives as $key => $item):?>
                                <option <?php if(!empty($year) && $year == $item->anio):?>selected="selected"<?php endif;?> value="<?php echo $item->anio;?>"><?php echo $item->anio;?></option>
                            <?php endforeach;?>
                        </select>
                      </form>
                      <?php if(!empty($year)):?>
                        <p>&nbsp;</p>
                        <a class="btn btn-primary" href="<?php echo base_url();?>teacher_view_all_answers/<?php echo $year;?>" target="_blank">View all answers</a>
                        <a class="btn btn-info" href="<?php echo base_url();?>teacher_print_all_answers/<?php echo $year;?>" target="_blank">Print all answers</a>
                        <a class="btn btn-primary" href="<?php echo base_url();?>teacher_view_all_reports/<?php echo $year;?>" target="_blank">View all reports</a>
                        <a class="btn btn-info" href="<?php echo base_url();?>teacher_print_all_reports/<?php echo $year;?>" target="_blank">Print all reports</a> 
                      <?php endif;?>
                    </div>
                </div>
                <div class="col-md-2">

                </div>
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div style="max-width: 800px;margin: auto;">
            
              <!-- Nav tabs -->
              <p>&nbsp;</p>
              
              <ul class="nav nav-tabs" role="tablist">
                <?php foreach($tests as $key => $item):?>
                <li role="presentation" <?php if($key == 0):?>class="active"<?php endif;?>><a href="#test-<?php echo $item->id;?>" aria-controls="home" role="tab" data-toggle="tab">
                    <?php if($key == 0):?>
                    1st Test
                    <?php endif;?>
                    <?php if($key == 1):?>
                    2nd Test
                    <?php endif;?>
                    <?php if($key == 2):?>
                    3rd Test
                    <?php endif;?> 
                    <?php if($key > 2):?>
                    <?php echo ($key + 1);?>th Test
                    <?php endif;?>   
                    <?php if($item->practice == 1):?>
                        <span class="label label-primary">Practice</span>
                    <?php endif;?>                                                       
                </a></li>
                <?php endforeach;?>
              </ul>
            
              <!-- Tab panes -->
              <div class="tab-content">
                <?php foreach($tests as $key => $item):?>
                <div role="tabpanel" class="tab-pane <?php if($key == 0):?>active<?php endif;?>" id="test-<?php echo $item->id;?>">
                    <div class="row">
                        <div class="col-md-5">
                            <p>&nbsp;</p>
                            <h2>Month, Date and Year of the Report</h2>
                            <p>&nbsp;</p>
                            <h1><?php echo date("F d, Y", strtotime($item->created));?></h1>
                            <?php if($item->practice == 1):?>
                                <div class="alert alert-info"><strong>PRACTICE TEST</strong></div>
                            <?php endif;?>
                            <a href="javascript:;" onclick="RemoveTest(<?php echo $item->id;?>)" class="btn btn-danger">Delete Test</a>
                        </div>
                        <div class="col-md-7">
                            <p>&nbsp;</p>
                            <table class="table table-bordered">
                              <tbody>
                                <?php foreach($item->students as $kst => $stud):?>
                                    <tr>
                                        <td><strong><?php echo $stud->student->name;?> <?php echo $stud->student->lname;?></strong></td>
                                        <td><a target="_blank" href="<?php echo base_url();?>teacher_view_answer/<?php echo $stud->id;?>/<?php echo $item->id;?>">View Answer</a></td>
                                        <td><a target="_blank" href="<?php echo base_url();?>teacher_view_report/<?php echo $stud->id;?>/<?php echo $item->id;?>">View Report</a></td>
                                        <td><a target="_blank" href="<?php echo base_url();?>teacher_print_answer/<?php echo $stud->id;?>/<?php echo $item->id;?>">Print Answer</a></td>
                                        <td><a target="_blank" href="<?php echo base_url();?>teacher_print_report/<?php echo $stud->id;?>/<?php echo $item->id;?>">Print Report</a></td>
                                    </tr>
                                <?php endforeach;?>
                              </tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
              </div>
            
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved - &COPY; <?php echo date("Y");?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/gentelella-master/build/js/custom.min.js"></script>
	<script>
        function RemoveTest(id) {
            if(!confirm('Sure you want to remove this test?')) return;
            $.post( "<?php echo base_url();?>teacher_dashboard/ajax_remove_test", {id: id}, function( data ) {
    
            }, "json").done(function( data ) {
                location.reload();
            });
        }
    </script>
  </body>