  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url();?>teacher_dashboard" class="site_title"><i class="fa fa-envelope"></i> <span style="font-size: 18px;">Vocab Management</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $user->name;?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php echo $sidebar;?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a href="<?php echo base_url();?>teacher_settings" data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>admin/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt=""><?php echo $user->name;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="<?php echo base_url();?>teacher_settings">
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <style>
            #graph_bar_3 svg, #graph_bar_4 svg {
                height: 500px;
            }
        </style>
        <div class="right_col" role="main">

            <h1>Analysis</h1>
            
            <div class="row">
                <div class="col-md-12">
                    <h2>The most incorrect words of students</h2>
                    <div id="graph_bar_2"></div>
                </div>
                <div class="col-md-12">
                    <h2>The most correct words of students</h2>
                    <div id="graph_bar_1"></div>
                </div>
                <div class="col-md-12">
                    <h2>The most incorrect column</h2>
                    <div id="graph_bar_3"></div>
                </div>                
                <div class="col-md-12">
                    <h2>The most correct column</h2>
                    <div id="graph_bar_4"></div>
                </div> 
                <div class="col-md-12">
                    <h2>The percentage of one test to the other</h2>
                    <div id="graph_bar_5"></div>
                </div>
                <div class="col-md-12">
                    <h2>The correct percentage of the class.</h2>
                    <div id="graph_bar_6"></div>
                </div>
                <div class="col-md-12">
                    <h2>The incorrect percentage of the class.</h2>
                    <div id="graph_bar_7"></div>
                </div>
                <div class="col-md-12">
                    <h2>The average of points that the class received.</h2>
                    <div id="graph_bar_8"></div>
                </div>
                <div class="col-md-12">
                    <h2>Percent of corrects answers in the same grade.</h2>
                    <div id="graph_bar_9"></div>
                </div>
                <div class="col-md-12">
                    <h2>Correct percent with other grades in the same test.</h2>
                    <div id="graph_bar_10"></div>
                </div>                
                <div class="col-md-12">
                    <h2>Incorrect percent with other grades in the same test.</h2>
                    <div id="graph_bar_11"></div>
                </div>                
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved - &COPY; <?php echo date("Y");?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/morris.js/morris.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/build/js/custom.min.js"></script>
	<script>
			$(function() {
                Morris.Bar({
                  element: 'graph_bar_1',
                  data: [
                    <?php foreach($correctas as $key => $item):?>
                	{word: '<?php echo $item->word;?>', score: <?php echo doubleval($item->promedio) * 100;?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'word',
                  ykeys: ['score'],
                  labels: ['Words'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 90,
                  hideHover: 'auto',
                  resize: true
                });

                Morris.Bar({
                  element: 'graph_bar_2',
                  data: [
                    <?php foreach($incorrectas as $key => $item):?>
                	{word: '<?php echo $item->word;?>', score: <?php echo 100 - (doubleval($item->promedio) * 100);?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'word',
                  ykeys: ['score'],
                  labels: ['Words'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 90,
                  hideHover: 'auto',
                  resize: true
                });

                Morris.Bar({
                  element: 'graph_bar_3',
                  data: [
                    <?php foreach($cols_no as $key => $item):?>
                	{column: '<?php echo $item['label'];?>', score: <?php echo $item['value'];?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'column',
                  ykeys: ['score'],
                  labels: ['Words'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 45,
                  hideHover: 'auto',
                  resize: true
                });
                
                Morris.Bar({
                  element: 'graph_bar_4',
                  data: [
                    <?php foreach($cols_ok as $key => $item):?>
                	{column: '<?php echo $item['label'];?>', score: <?php echo $item['value'];?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'column',
                  ykeys: ['score'],
                  labels: ['Words'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 45,
                  hideHover: 'auto',
                  resize: true
                });                

                Morris.Bar({
                  element: 'graph_bar_5',
                  data: [
                    <?php foreach($tests_comp as $key => $item):?>
                	{column: '<?php echo $item['label'];?>', score: <?php echo $item['value'];?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'column',
                  ykeys: ['score'],
                  labels: ['Words'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 45,
                  hideHover: 'auto',
                  resize: true
                });

                Morris.Bar({
                  element: 'graph_bar_6',
                  data: [
                    <?php foreach($average_students_corr as $key => $item):?>
                	{column: '<?php echo $item['label'];?>', score: <?php echo $item['value'];?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'column',
                  ykeys: ['score'],
                  labels: ['Correct %'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 45,
                  hideHover: 'auto',
                  resize: true
                });

                Morris.Bar({
                  element: 'graph_bar_7',
                  data: [
                    <?php foreach($average_students_incorr as $key => $item):?>
                	{column: '<?php echo $item['label'];?>', score: <?php echo $item['value'];?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'column',
                  ykeys: ['score'],
                  labels: ['Incorrect %'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 45,
                  hideHover: 'auto',
                  resize: true
                });

                Morris.Bar({
                  element: 'graph_bar_8',
                  data: [
                    <?php foreach($average_students as $key => $item):?>
                	{column: '<?php echo $item['label'];?>', score: <?php echo $item['value'];?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'column',
                  ykeys: ['score'],
                  labels: ['Average'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 45,
                  hideHover: 'auto',
                  resize: true
                });

                Morris.Bar({
                  element: 'graph_bar_9',
                  data: [
                    <?php foreach($correctas_malas_clase as $key => $item):?>
                	{column: '<?php echo $item['label'];?>', score: <?php echo $item['value'];?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'column',
                  ykeys: ['score'],
                  labels: ['Percent %'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 45,
                  hideHover: 'auto',
                  resize: true
                });
                
                Morris.Bar({
                  element: 'graph_bar_10',
                  data: [
                    <?php foreach($results_by_grade_ok as $key => $item):?>
                	{column: '<?php echo $item['label'];?>', score: <?php echo $item['value'];?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'column',
                  ykeys: ['score'],
                  labels: ['Percent %'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 45,
                  hideHover: 'auto',
                  resize: true
                });                

                Morris.Bar({
                  element: 'graph_bar_11',
                  data: [
                    <?php foreach($results_by_grade_no as $key => $item):?>
                	{column: '<?php echo $item['label'];?>', score: <?php echo $item['value'];?>},
                    <?php endforeach;?>
                  ],
                  xkey: 'column',
                  ykeys: ['score'],
                  labels: ['Percent %'],
                  barRatio: 0.2,
                  barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                  xLabelAngle: 45,
                  hideHover: 'auto',
                  resize: true
                }); 
			})
    </script>
  </body>