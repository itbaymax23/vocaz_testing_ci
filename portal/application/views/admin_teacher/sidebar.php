<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url();?>teacher_dashboard"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo base_url();?>teacher_start_test"><i class="fa fa-paper-plane"></i> Start Test</a></li>
      <li><a href="<?php echo base_url();?>teacher_students"><i class="fa fa-user"></i> Students</a></li>
      <li><a href="<?php echo base_url();?>teacher_reports"><i class="fa fa-clone"></i> Reports</a></li>
      <li><a href="<?php echo base_url();?>teacher_analysis"><i class="fa fa-paper-plane"></i> Analysis</a></li>
      <li><a href="<?php echo base_url();?>teacher_archive"><i class="fa fa-archive"></i> Archive</a></li>
    </ul>
  </div>
  <div class="menu_section">
    <h3>Live On</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url();?>teacher_recording"><i class="fa fa-user"></i> Recording</a></li>
      <li><a href="<?php echo base_url();?>teacher_settings"><i class="fa fa-cog"></i> Settings</a></li>  
    </ul>
  </div>

</div>