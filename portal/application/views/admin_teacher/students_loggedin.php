  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url();?>teacher_dashboard" class="site_title"><i class="fa fa-envelope"></i> <span style="font-size: 18px;">Vocab Management</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $user->name;?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php echo $sidebar;?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a href="<?php echo base_url();?>teacher_settings" data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>admin/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt=""><?php echo $user->name;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="<?php echo base_url();?>teacher_settings">
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <!-- page content -->
        <style>
            .student-online {
                height: 40px;
                width: 40px;
                background-color: rgba(38,185,154,.88);
                border-radius: 50%;
                margin: auto;
            }
            .student-offline {
                height: 40px;
                width: 40px;
                background-color: rgba(231,76,60,.88);
                border-radius: 50%;
                margin: auto;
            }
        </style>
        <div class="right_col" role="main">
            <div class="page-title">
              <h1 class="text-center">View logged in students</h1>  
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="row">
                <?php foreach($students as $key => $item):?>
                    <div class="col-md-6 col-xs-12">
                        <div class="panel panel-default" style="height: 115px;">
                          <div class="panel-body">
                                <div class="row student-row" id="student_<?php echo $item->id;?>" data-id="<?php echo $item->id;?>">
                                    <div class="col-xs-4 text-center">
                                        <i class="fa fa-user" style="font-size: 60px;"></i>
                                        <br />
                                        <div class="student-banned"></div>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <h2><?php echo $item->name;?> <?php echo $item->lname;?></h2>
                                        <div class="student-alert"></div>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <div class="student-status"></div>
                                    </div>
                                </div>
                          </div>
                        </div>                       
                    </div>
                <?php endforeach;?>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved - &COPY; <?php echo date("Y");?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/gentelella-master/build/js/custom.min.js"></script>
	<script>
    
        function BannUser(id) {
            $.post( "<?php echo base_url();?>teacher_dashboard/ajax_bann_student", {student_id: id}, function( data ) {
    
            }, "json").done(function( data ) {
                GetStatusStudent(id);
            });             
        }
        
        function UnbannUser(id) {
            $.post( "<?php echo base_url();?>teacher_dashboard/ajax_unbann_student", {student_id: id}, function( data ) {
    
            }, "json").done(function( data ) {
                GetStatusStudent(id);
            });            
        }
    
        function GetStatusStudent(id) {
            $.post( "<?php echo base_url();?>teacher_dashboard/ajax_get_student_status", {student_id: id}, function( data ) {
    
            }, "json").done(function( data ) {
                if(data.absent == 1) {
                    $('#student_' + id).find('.student-status').html('<div class="student-offline"></div>');
                    $('#student_' + id).find('.student-banned').html('');
                } else {
                    if(parseInt(data.result.status) == 1) {
                        $('#student_' + id).find('.student-status').html('<div class="student-online"></div>');
                    }
                    if(parseInt(data.result.status) == 2) {
                        $('#student_' + id).find('.student-status').html('<div class="alert alert-info text-center">COMPLETE</div>');
                    }
                    
                    if(parseInt(data.result.fulloff) == 1) {
                        $('#student_' + id).find('.student-alert').html('<span class="label label-danger">Student not in fullscreen.</span>');
                    }
                    
                    if(parseInt(data.result.status) != 2) {
                        if(data.result.banned == 0) {
                            $('#student_' + id).find('.student-banned').html('<button class="alert alert-danger" style="font-size: 12px;padding: 5px;" type="button" onclick="BannUser('+id+')">FREEZE ACCOUNT</div>');
                        } else {
                            $('#student_' + id).find('.student-banned').html('<button class="alert alert-success" style="font-size: 12px;padding: 5px;" type="button" onclick="UnbannUser('+id+')">ACTIVE ACCOUNT</div>');
                        }                        
                    } else {
                        $('#student_' + id).find('.student-banned').html('');
                    }          
                }

            });            
        }
    
        function CheckStudents() {
            $('.student-row').each(function() {
                student_id = $(this).attr('data-id');
                GetStatusStudent(student_id);
            });
        }
        
        $(function() {
            CheckStudents();
            setInterval(function() {
                CheckStudents();
            }, 15000);
        })
    </script>
  </body>