<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Test <?php echo date("F d, Y", strtotime($test->created));?></title>
  </head>
  <body>
    <style>
        h1 {
            text-align: center;
        }
        .table-100 {
            width: 100%;
            height: 100%;
        }
        .n90-grados {
            /*transform: rotateZ(-90deg);
            float: left;
            width: 32px;*/
        }
        .tr-rotates td {
   
        }
        .borderer {
            border: 1px solid #000;
        }
        
        .borderer-right {
            border: 1px solid #000;
        }
        
        tr {
            border-collapse: collapse;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
        td {
            /*padding: 2px;*/
            padding: 0;
            margin: 0
        }
        .pad-value {
            padding: 2px;
        }
        .bg-grey {
            background-color: #f1f1f1;
        }
    </style>
    
    <table class="table-100" style="max-width: 1200px; margin: auto;">
        <tr>
            <td>
                <table class="table-100" style="">
                    <?php if($return != 1):?>
                    <tr>
                        <td colspan="11">
                            <h2>Feature Guide for Upper Level Spelling Inventory</h2>
                        </td>
                    </tr>
                    <?php endif;?>
                    <tr style="padding-bottom: 10px;">
                        <td colspan="11">
                            <h3>Name: <?php echo $student->name;?> <?php echo $student->lname;?></h3>
                        </td>
                    </tr>
                    <tr style="padding-bottom: 10px;">
                        <td colspan="11">
                            <p>&nbsp;</p>
                        </td>
                    </tr>
                    <tr class="tr-rotates">
                        <td class="borderer bg-grey pad-value" style="width: 120px;text-align: center;">STAGES</td>
                        <td class="borderer bg-grey pad-value" style="text-align: center;" colspan="3">Within Word<br />Pattern</td>
                        <td class="borderer bg-grey pad-value" style="text-align: center;" colspan="3">Syllables and<br />Affixes</td>
                        <td class="borderer bg-grey pad-value" style="text-align: center;" colspan="3">Derivation<br />Relations</td>
                        <td class="borderer bg-grey pad-value" style="text-align: center;">TOTALS</td>
                    </tr>
                    <tr style="height: 50px;" class="tr-rotates">
                        <td class="borderer bg-grey"></td>
                        <td class="borderer bg-grey pad-value">late</td>
                        <td class="borderer bg-grey pad-value">early</td>
                        <td class="borderer bg-grey pad-value">mid</td>
                        
                        <td class="borderer bg-grey pad-value">late</td>
                        <td class="borderer bg-grey pad-value">early</td>
                        <td class="borderer bg-grey pad-value">mid</td>
                        
                        <td class="borderer bg-grey pad-value">late</td>
                        <td class="borderer bg-grey pad-value">early</td>
                        <td class="borderer bg-grey pad-value">mid</td>                  
                        <td class="borderer bg-grey">

                        </td>
                    </tr>
                    
                    <tr style="height: 120px;" class="tr-rotates">
                        <td class="borderer"></td>
                        <td class="borderer pad-value"><span class="n90-grados">Blends and<br /> Digraphs</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Vowels</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Complex<br /> Consonants</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Inflected Ending +<br /> Syllable Juncture</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Unaccented Final<br /> Syllables Affixes</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Affixes</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Reduced Vowels in<br /> Unaccented Syllables</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Greek &amp;<br /> Latin Elements</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Assimilates<br /> Prefixes</span></td>
                        <td class="borderer">
                            <table class="table-100" style="height: 120px;">
                                <tr>
                                    <td class="borderer pad-value">Featured<br />Point</td>
                                    <td class="borderer pad-value">Correct<br />Spelling</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php foreach($words as $key => $item):?>
                    <tr style="height: 30px;" class="<?php echo ($key % 2 ? 'bg-grey' : '');?>">
                        <td class="borderer pad-value"><?php echo ($key + 1);?> - <?php echo $item['word'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo3_blends'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo3_vowels'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo3_complex'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo3_inflected'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo3_unnacented'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo3_affixes'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo3_reduced'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo3_greek'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo3_assimilated'];?></td>
                        <td class="borderer">
                            <table class="table-100">
                                <tr>
                                    <td>
                                        <table class="table-100">
                                            <tr>
                                                <td class="borderer" style="height: 30px;"></td>
                                                <td class="borderer" style="height: 30px;"></td>
                                                <td class="borderer" style="height: 30px;"></td>
                                                <td class="borderer" style="height: 30px;"></td>
                                            </tr>
                                        </table>                                    
                                    </td>
                                    <td>
                                        <table class="table-100">
                                            <tr>
                                                <td class="borderer" style="height: 30px;"></td>
                                                <td class="borderer" style="height: 30px;"></td>
                                                <td class="borderer" style="height: 30px;"></td>
                                                <td class="borderer" style="height: 30px;"></td>
                                            </tr>
                                        </table>                                    
                                    </td>
                                </tr>
                            </table>                        
                        </td>
                    </tr>                    
                    <?php endforeach;?>                                

                    <tr style="height: 30px;">
                        <td class="borderer pad-value">TOTALS</td>
                        <td class="borderer pad-value"><?php echo $results['wo3_blends_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo3_vowels_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo3_complex_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo3_inflected_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo3_unnacented_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo3_affixes_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo3_reduced_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo3_greek_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo3_assimilated_total'];?></td>
                        <td class="borderer">
                            <table class="table-100">
                                <tr>
                                    <td>
                                        <table class="table-100">
                                            <tr>
                                                <td class="borderer-right pad-value"><?php echo $total;?></td>
                                                <td class="borderer-right pad-value"><?php echo $total;?></td>
                                                <td class="borderer-right pad-value"><?php echo $total;?></td>
                                                <td class="borderer-right pad-value"><?php echo $total;?></td>
                                            </tr>
                                        </table>                                    
                                    </td>
                                    <td>
                                        <table class="table-100">
                                            <tr>
                                                <td class="borderer-right pad-value"><?php echo $results['word_ok'];?></td>
                                                <td class="borderer-right pad-value"><?php echo $results['word_ok'];?></td>
                                                <td class="borderer-right pad-value"><?php echo $results['word_ok'];?></td>
                                                <td class="borderer-right pad-value"><?php echo $results['word_ok'];?></td>
                                            </tr>
                                        </table>                                    
                                    </td>
                                </tr>
                            </table>                          
                        </td>
                    </tr>
                                                                                
                </table>
            </td>
        </tr>
    </table>
  </body>
</html>