<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Test <?php echo date("F d, Y", strtotime($test->created));?></title>
  </head>
  <body>
    <style>
        h1 {
            text-align: center;
        }
        .table-100 {
            width: 100%;
            height: 100%;
        }
        .n90-grados {
            /*transform: rotateZ(-90deg);
            float: left;
            width: 32px;*/
        }
        .tr-rotates td {
   
        }
        .borderer {
            border: 1px solid #000;
        }
        
        .borderer-right {
            border: 1px solid #000;
        }
        
        tr {
            border-collapse: collapse;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
        td {
            /*padding: 2px;*/
            padding: 0;
            margin: 0
        }
        .pad-value {
            padding: 2px;
        }
        .bg-grey {
            background-color: #f1f1f1;
        }
    </style>
    <?php if($return != 1):?>
    <h1>Feature Guide for Primary Spelling Inventory</h1>
    <?php endif;?>
    <table class="table-100" style="max-width: 1200px; margin: auto;">
        <tr>
            <td>
                <table class="table-100">
                    <tr style="padding-bottom: 10px;">
                        <td colspan="11">
                            <h3>Name: <?php echo $student->name;?> <?php echo $student->lname;?></h3>
                        </td>
                    </tr>
                    <tr class="tr-rotates">
                        <td style="width: 150px;text-align: center;" class="bg-grey borderer pad-value">STAGES</td>
                        <td class="borderer bg-grey pad-value" style="text-align: center;">emege</td>
                        <td class="borderer bg-grey pad-value" style="text-align: center;" colspan="3">Letter Name<br />Alphabetic</td>
                        <td class="borderer bg-grey pad-value" style="text-align: center;" colspan="3">Within Word<br />Pattern</td>
                        <td class="borderer bg-grey pad-value" style="text-align: center;" colspan="3">Syllables and<br />Affixes</td>
                    </tr>
                    <tr style="height: 50px;" class="tr-rotates">
                        <td class="bg-grey borderer"></td>
                        <td class="borderer bg-grey pad-value">late</td>
                        <td class="borderer bg-grey pad-value">early</td>
                        <td class="borderer bg-grey pad-value">mid</td>
                        <td class="borderer bg-grey pad-value">late</td>
                        <td class="borderer bg-grey pad-value">early</td>
                        <td class="borderer bg-grey pad-value">mid</td>
                        <td class="borderer bg-grey pad-value">late</td>
                        <td class="borderer bg-grey pad-value">early</td>
                        <td class="borderer bg-grey">

                        </td>
                    </tr>
                    
                    <tr style="height: 120px;" class="tr-rotates">
                        <td class="borderer"></td>
                        <td class="borderer pad-value"><span class="n90-grados">Beginning Consonant</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Final Consonan</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Short Vowels</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Consonant Digraphs</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Consonant Blends</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Long Vowels<br />Patterns</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Other Vowels<br />Patterns</span></td>
                        <td class="borderer pad-value"><span class="n90-grados">Inflected Endings</span></td>
                        <td class="borderer pad-value">
                            <table class="table-100">
                                <tr>
                                    <td class="borderer pad-value">Featured<br />Point</td>
                                    <td class="borderer pad-value">Correct<br />Spelling</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php foreach($words as $key => $item):?>
                    <tr style="height: 30px;" class="<?php echo ($key % 2 ? 'bg-grey' : '');?>">
                        <td class="borderer pad-value" style="width: 150px;"><?php echo ($key + 1);?> - <?php echo $item['word'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo1_beginning'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo1_final'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo1_short'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo1_consonant'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo1_conblends'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo1_longvow'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo1_othervow'];?></td>
                        <td class="borderer pad-value"><?php echo $item['wo1_inflected'];?></td>
                        <td class="borderer">
                            <table class="table-100">
                                <tr>
                                    <td>
                                        <table class="table-100">
                                            <tr>
                                                <td style="height: 30px;" class="borderer"></td>
                                                <td style="height: 30px;" class="borderer"></td>
                                                <td style="height: 30px;" class="borderer"></td>
                                                <td style="height: 30px;" class="borderer"></td>
                                            </tr>
                                        </table>                                    
                                    </td>
                                    <td>
                                        <table class="table-100">
                                            <tr>
                                                <td style="height: 30px;" class="borderer"></td>
                                                <td style="height: 30px;" class="borderer"></td>
                                                <td style="height: 30px;" class="borderer"></td>
                                                <td style="height: 30px;" class="borderer"></td>
                                            </tr>
                                        </table>                                    
                                    </td>
                                </tr>
                            </table>                        
                        </td>
                    </tr>                    
                    <?php endforeach;?>                                

                    <tr style="height: 30px;">
                        <td class="borderer pad-value">TOTALS</td>
                        <td class="borderer pad-value"><?php echo $results['wo1_beginning_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo1_final_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo1_short_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo1_consonant_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo1_conblends_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo1_longvow_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo1_othervow_total'];?></td>
                        <td class="borderer pad-value"><?php echo $results['wo1_inflected_total'];?></td>
                        <td class="borderer">
                            <table class="table-100">
                                <tr>
                                    <td>
                                        <table class="table-100">
                                            <tr>
                                                <td class="borderer-right pad-value"><?php echo $total;?></td>
                                                <td class="borderer-right pad-value"><?php echo $total;?></td>
                                                <td class="borderer-right pad-value"><?php echo $total;?></td>
                                                <td class="borderer-right pad-value"><?php echo $total;?></td>
                                            </tr>
                                        </table>                                    
                                    </td>
                                    <td>
                                        <table class="table-100">
                                            <tr>
                                                <td class="borderer-right pad-value"><?php echo $results['word_ok'];?></td>
                                                <td class="borderer-right pad-value"><?php echo $results['word_ok'];?></td>
                                                <td class="borderer-right pad-value"><?php echo $results['word_ok'];?></td>
                                                <td class="borderer-right pad-value"><?php echo $results['word_ok'];?></td>
                                            </tr>
                                        </table>                                    
                                    </td>
                                </tr>
                            </table>                          
                        </td>
                    </tr>
                                                                                
                </table>
            </td>
        </tr>
    </table>
  </body>
</html>