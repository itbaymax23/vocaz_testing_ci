  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url();?>teacher_dashboard" class="site_title"><i class="fa fa-envelope"></i> <span style="font-size: 18px;">Vocab Management</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $user->name;?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php echo $sidebar;?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a href="<?php echo base_url();?>teacher_settings" data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>admin/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt=""><?php echo $user->name;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="<?php echo base_url();?>teacher_settings">
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <style>
            #graph_bar_3 svg, #graph_bar_4 svg {
                height: 500px;
            }
        </style>
        <div class="right_col" role="main">

            <h1 style="text-align: center;">Your Plan</h1>
            <div id="wrapper">
                <div class="content">
                    <?php if($current_plan):?>
                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <h1>Status: Subscribed</h1>
                            <div class="alert alert-warning" style="max-width: 300px; margin: auto;">
                                <?php if(!empty($current_plan)):?>
                                    Finish on <strong><?php echo date("m.d.Y", strtotime($current_plan->end_date));?></strong>
                                <?php endif;?>    
                            </div>
                        </div>
                    </div>
                    <?php else:?>
                    <div class="row">
                        <div class="col-md-12">
                                <div class="panel panel-default" style="max-width: 800px; margin: auto;">
                                  <div class="panel-heading">
                                    <span class="panel-title">Subscribe</strong></span>
                                    <div style="float: right;">
                                      <select class="plan_type">
                                        <option value="2" <?php echo ($plan_id == 2) ? 'selected' : ''?>>Basic Plan </option>
                                        <option value="3" <?php echo ($plan_id == 3) ? 'selected' : ''?>>Upgrade Plan </option>
                                        <option value="4" <?php echo ($plan_id == 4) ? 'selected' : ''?>>Premium Plan </option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6" style="text-align: center;">
                                                <img style="max-width: 250px;" src="<?php echo base_url();?>assets/images/paypal.png?i=1" />
                                                <br />
                                                <a class="btn btn-success" href="<?php echo $link_plan;?>">Pay USD <?php echo $amount;?> with Paypal</a>                                            
                                            </div>
                                            <div class="col-md-6" style="text-align: center;">
                                                <form action="/admin_dashboard/suscription_plan" method="post" id="payment-form">
                                                    <img style="max-height: 70px;" src="<?php echo base_url();?>assets/images/stripe.png" />
                                                    <br />    
                                                    <label for="card-element">
                                                      Credit or debit Card
                                                    </label>
                                                    <div id="card-element">
                                                      <!-- A Stripe Element will be inserted here. -->
                                                    </div>
                                                    
                                                    <!-- Used to display Element errors. -->
                                                    <div id="card-errors" role="alert"></div>
                                                    <br />
                                                    <button class="btn btn-success" id="btnPagar">Pay USD <?php echo $amount;?> with Stripe</button>
                                                </form>                                            
                                            </div>
                                        </div>
                                  </div>
                                </div>
                        </div>
                    </div>
                    <?php endif;?>
                </div>
            </div>

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved - &COPY; <?php echo date("Y");?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/morris.js/morris.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/build/js/custom.min.js"></script>
    <script>    
        var handler = null;
        var stripe = null;
        var elements = null;
        var handler2 = null;
        var stripe2 = null;
        var elements2 = null;
    
        function LoadStripe() {
            if($('#card-element').length > 0) {
                stripe = Stripe('pk_test_tODqh2CUxCFYlEsB9DBIc5gT');
                elements = stripe.elements();
                // Custom styling can be passed to options when creating an Element.
                var style = {
                  base: {
                    // Add your base input styles here. For example:
                    fontSize: '16px',
                    color: "#32325d",
                  }
                };
                
                // Create an instance of the card Element.
                var card = elements.create('card', {style: style});
                
                // Add an instance of the card Element into the `card-element` <div>.
                card.mount('#card-element');
                
                card.addEventListener('change', function(event) {
                  var displayError = document.getElementById('card-errors');
                  if (event.error) {
                    displayError.textContent = event.error.message;
                  } else {
                    displayError.textContent = '';
                  }
                });
        
        
                // Create a token or display an error when the form is submitted.
                var form = document.getElementById('payment-form');
                form.addEventListener('submit', function(event) {
                  event.preventDefault();
                
                  stripe.createToken(card).then(function(result) {
                    if (result.error) {
                      // Inform the customer that there was an error.
                      var errorElement = document.getElementById('card-errors');
                      errorElement.textContent = result.error.message;
                    } else {
                      // Send the token to your server.
                      stripeTokenHandler(result.token);
                    }
                  });
                });            
            }
        }
        
        function stripeTokenHandler(token) {
          // Insert the token ID into the form so it gets submitted to the server
          var form = document.getElementById('payment-form');
          var hiddenInput = document.createElement('input');
          hiddenInput.setAttribute('type', 'hidden');
          hiddenInput.setAttribute('name', 'stripeToken');
          hiddenInput.setAttribute('value', token.id);
          form.appendChild(hiddenInput);
        
          // Submit the form
          form.submit();
        }
    </script>
    
    <script src="https://js.stripe.com/v3/" onload="LoadStripe()"></script>

    <script type="text/javascript">
      $('.plan_type').change(function(){
        var id = $('option:selected', this).val();
        window.location.href = '<?php echo base_url();?>admin_dashboard/suscription_plan/' + id;
    })
    </script>
  </body>