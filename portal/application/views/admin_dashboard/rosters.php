  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url();?>teacher_dashboard" class="site_title"><i class="fa fa-envelope"></i> <span style="font-size: 18px;">Vocab Management</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $user->name;?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php echo $sidebar;?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a href="<?php echo base_url();?>admin_settings" data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>admin/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt=""><?php echo $user->name;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="<?php echo base_url();?>admin_settings">
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <style>
                .x_title h2 {
                    font-size: 16px;
                    font-weight: bold;
                }
            </style>
            <div class="page-title">
              <div class="title_left">
                <a href="javascript:;" onclick="OpenModalImport()" class="btn btn-success"><i class="fa fa-upload"></i> Import</a>
                <a href="<?php echo base_url();?>admin_dashboard/promote_students" class="btn btn-info"><i class="fa fa-upload"></i> Promote students</a>
              </div>
              <h3 class="pull-left">Import Roster</h3>  
              <div class="title_right" style="width: auto; float: right;">
                <a href="<?php echo base_url();?>admin_create_roster" class="btn btn-primary">Create a new roster</a>
              </div>
            </div>
            <p>&nbsp;</p>
            <div class="row" style="clear: both; margin-top: 35px;">
                <?php foreach($rosters as $key => $item):?>
                      <div class="col-md-4 col-xs-12 widget widget_tally_box">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2><?php echo $item->name;?></h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a href="javascript:;" onclick="EditRoster(<?php echo $item->id;?>, '<?php echo $item->name;?>', '<?php echo $item->teacher_id;?>')">Edit</a>
                                  </li>
                                  <li><a href="javascript:;" onclick="DeleteRoster(<?php echo $item->id;?>)">Delete</a>
                                  </li>
                                </ul>
                              </li>
                              <!--li><a class="close-link"><i class="fa fa-close"></i></a-->
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                            <div>
                              <ul class="list-inline widget_tally">
                                <?php foreach($item->students as $kstu => $stud):?>
                                <li>
                                  <p>
                                    <span class="month"><?php echo $stud->name;?> <?php echo $stud->lname;?></span>
                                    <span class="count"><input value="<?php echo $stud->id;?>" type="checkbox" /></span>
                                  </p>
                                </li>
                                <?php endforeach;?>
                              </ul>
                            </div>
                            
                            
                            <a href="javascript:;" onclick="UploadFull(<?php echo $item->id;?>, '<?php echo $item->name;?>')" class="btn btn-primary" style="width: 100%;">Upload New Full Roster</a>
                          </div>
                        </div>
                      </div>
                <?php endforeach;?>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved - &COPY; <?php echo date("Y");?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    
    <?php if(!empty($sel_roster) && count($rows) > 0):?>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalUploadFullList">
      <div class="modal-dialog moal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Upload for <strong><?php echo $sel_roster->name;?></strong></h4>
          </div>
          <div class="modal-body">
            <form id="frmList">
                <input type="hidden" id="ridlist" name="ridlist" value="<?php echo $sel_roster->id;?>" />
                <?php foreach($rows as $key => $item):?>
                <div class="form-group student-item">
                    <div class="col-md-3">
                        <input type="text" class="form-control fname" value="<?php echo $item['fname'];?>" />
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control lname" value="<?php echo $item['lname'];?>" />
                    </div>
                    <div class="col-md-5">
                        <input type="text" class="form-control email" value="<?php echo $item['email'];?>" />
                        <input type="hidden" class="username" value="<?php echo $item['username'];?>" />
                    </div>                    
                    <div class="col-md-1">
                        <span style="cursor: pointer;" onclick="RemoveItemList(this)" class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </div> 
                </div>
                <?php endforeach;?>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" onclick="SaveList()" class="btn btn-primary">Import</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <?php endif;?>

    <div class="modal fade" tabindex="-1" role="dialog" id="modalImport">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Import Roster</h4>
          </div>
          <div class="modal-body">
            <?php if(count($rosters) > 0):?>
            <form action="" method="post" enctype="multipart/form-data" name="frmImport">
                <div class="form-group">
                    <label>Roster</label>
                    <select class="form-control" name="rid">
                        <?php foreach($rosters as $key => $item):?>
                            <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Select a file:</label>
                    <input type="file" name="userfile" class="form-control" />
                </div>
            </form>
            <?php else:?>
                <div class="alert alert-warning">You need to create a new Roaster.</div>
            <?php endif;?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" onclick="document.frmImport.submit()" class="btn btn-primary">Import</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <form style="display: none;" action="" method="post" name="deleteRoster">
        <input type="hidden" name="roster_delete_id" id="roster_delete_id" />
    </form>
    
    <div class="modal fade" tabindex="-1" role="dialog" id="modalEditRoster">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Editer Roster</h4>
          </div>
          <div class="modal-body">
            <form action="" name="frmEdit" method="post">
                <input type="hidden" id="roster_id" name="roster_id" />
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" name="name" class="form-control" id="edit-name" />
                </div>
                <div class="form-group">
                    <label>Teacher</label>
                    <select name="teacher_id" id="teacher_id" class="form-control">
                        <option value="0">None</option>
                        <?php foreach($teachers as $key => $item):?>
                            <option value="<?php echo $item->id;?>"><?php echo $item->name;?> <?php echo $item->lname;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" onclick="document.frmEdit.submit()" class="btn btn-primary">Send</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="modal fade" tabindex="-1" role="dialog" id="modalUploadFull">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Upload New Full Roster for <strong id="roster_name"></strong></h4>
          </div>
          <div class="modal-body">
            <form action="" method="post" enctype="multipart/form-data" name="frmUF">
                <input type="hidden" id="rid" name="rid" />
                <div class="form-group">
                    <label>Select a file:</label>
                    <input type="file" name="userfile" class="form-control" />
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" onclick="document.frmUF.submit()" class="btn btn-primary">Send</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/gentelella-master/build/js/custom.min.js"></script>
	<script>
        function UploadFull(id, name) {
            $('#roster_name').text(name);
            $('#rid').val(id);
            $('#modalUploadFull').modal('show');
        }
        
        function RemoveItemList(obj) {
            if(!confirm('Sure you want to remove this student?')) return;
            $(obj).parent().parent().remove();
        }
        
        function SaveList() {
            $('#modalUploadFullList').find('input').attr('disabled', 'disabled');
            $('#modalUploadFullList').find('button').hide();
            
            ridlist = $('#ridlist').val();
            
            students = new Array();
            
            $('.student-item').each(function() {
                obj = new Object();
                obj.fname = $(this).find('.fname').val();
                obj.lname = $(this).find('.lname').val();
                obj.email = $(this).find('.email').val();
                obj.username = $(this).find('.username').val();
                students.push(obj); 
            });
            
             $.post( "<?php echo base_url();?>admin_dashboard/ajax_save_list", {ridlist: ridlist, students: students}, function( data ) {
    
            }, "json").done(function( data ) {
                if(data.error != '') {
                    $('#error-email').html(data.error).show();
                } else {
                    alert('Roster saved successfully!');
                    location.href = '<?php echo base_url();?>admin_import_roster';
                }
            }); 
        }
        
        function OpenModalImport() {
            $('#modalImport').modal('show');
        }
        
        function EditRoster(id, name, teacher_id) {
            $('#roster_id').val(id);
            $('#edit-name').val(name);
            $('#teacher_id').val(teacher_id);
            $('#modalEditRoster').modal('show');
        }
        
        function DeleteRoster(id) {
            if(!confirm('Sure you want to delete this Roster?')) return;
            $('#roster_delete_id').val(id);
            document.deleteRoster.submit();
        }
        
        $(function() {
            <?php if(!empty($sel_roster) && count($rows) > 0):?>
            $('#modalUploadFullList').modal('show');
            <?php endif;?>
        })
    </script>
  </body>