<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Test <?php echo date("F d, Y", strtotime($test->created));?></title>
  </head>
  <body>
    <style>
        h1 {
            text-align: center;
        }
        .table-100 {
            width: 100%;
            height: 100%;
        }
        .n90-grados {
            /*transform: rotateZ(-90deg);
            float: left;
            width: 32px;*/
        }
        .tr-rotates td {
   
        }
        .borderer {
            border: 1px solid #000;
        }
        
        .borderer-right {
            border: 1px solid #000;
        }
        
        tr {
            border-collapse: collapse;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
        td {
            /*padding: 2px;*/
            padding: 0;
            margin: 0
        }
        .pad-value {
            padding: 2px;
        }
        .bg-grey {
            background-color: #f1f1f1;
        }
    </style>
    <h2 style="text-align: center;">Feature Guide for Upper Level Spelling Inventory</h2>
    <?php echo $content;?>
  </body>
</html>