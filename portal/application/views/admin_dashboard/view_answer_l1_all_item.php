<table class="table-100" style="max-width: 1200px; margin: auto;">
    <tr>
        <td>
            <table class="table-100">
                <tr style="padding-bottom: 10px;">
                    <td colspan="11">
                        <h3>Name: <?php echo $student->name;?> <?php echo $student->lname;?></h3>
                    </td>
                </tr>
                <tr class="tr-rotates">
                    <td style="width: 150px;text-align: center;" class="borderer bg-grey pad-value">STAGES</td>
                    <td class="borderer bg-grey pad-value" style="text-align: center;">Answer 1</td>
                    <td class="borderer bg-grey pad-value" style="text-align: center;">Answer 2</td>
                    <td class="borderer bg-grey pad-value" style="text-align: center;">Answer 3</td>
                    <td class="bg-grey borderer pad-value" style="text-align: center;">Answer 4</td>
                </tr>
                <?php foreach($words as $key => $item):?>
                <tr style="height: 30px;" class="<?php echo ($key % 2 ? 'bg-grey' : '');?>">
                    <td class="borderer pad-value" style="width: 150px;"><?php echo ($key + 1);?> - <?php echo $item['word'];?></td>
                    <td class="borderer pad-value"><?php echo $results['featured_point0'][$key];?></td>
                    <td class="borderer pad-value"><?php echo $results['featured_point1'][$key];?></td>
                    <td class="borderer pad-value"><?php echo $results['featured_point2'][$key];?></td>
                    <td class="borderer pad-value"><?php echo $results['featured_point3'][$key];?></td>
                </tr>                   
                <?php endforeach;?>
                                                                            
            </table>
        </td>
    </tr>
</table>