<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url();?>admin_dashboard"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo base_url();?>admin_teachers"><i class="fa fa-group"></i> Teachers</a></li>
      <li><a href="<?php echo base_url();?>admin_students"><i class="fa fa-user"></i> Students</a></li>
      <li><a href="<?php echo base_url();?>admin_dashboard/alumni"><i class="fa fa-user"></i> Alumni</a></li>
      <li><a href="<?php echo base_url();?>admin_reports"><i class="fa fa-clone"></i> Reports</a></li>
      <!--li><a href="<?php echo base_url();?>admin_analisys"><i class="fa fa-paper-plane"></i> Analisys</a></li-->
      <li><a href="<?php echo base_url();?>admin_archive"><i class="fa fa-archive"></i> Archive</a></li>
      <li><a href="<?php echo base_url();?>admin_dashboard/suscription_plan"><i class="fa fa-money"></i> Subscription</a></li>
    </ul>
  </div>
  <div class="menu_section">
    <h3>Live On</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url();?>admin_recording"><i class="fa fa-user"></i> Recording</a></li>
      <li><a href="<?php echo base_url();?>admin_settings"><i class="fa fa-cog"></i> Settings</a></li>  
    </ul>
  </div>

</div>