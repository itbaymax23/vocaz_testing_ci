<body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="frmRecover">
              <h1>Recover your account</h1>
              <div>
                <input type="text" id="email" class="form-control" placeholder="Email" required="" />
                <div class="alert alert-danger" id="error-email" style="display: none;"></div>
              </div>
              <div>
                <a class="btn btn-default submit" href="javascript:;" onclick="Recover()">Send</a>
                <a class="reset_pass" href="<?php echo base_url();?>/superadmin/login">Sign In</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <!--h1><i class="fa fa-paw"></i> Gentelella Alela!</h1-->
                  <p>©2016 All Rights Reserved. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
</body>
<script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
<script>
    function Recover() {
        $('.alert-danger').hide();
        
        email = $('#email').val();
        
        error = false;
        
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(email.toLowerCase())) {
            error = true;
            $('#error-email').html('The Email entered is not valid.').show();
        }
        if(error) return;
        
        $.post( "<?php echo base_url();?>superadmin/ajax_recover", {email: email}, function( data ) {

        }, "json").done(function( data ) {
            if(data.error != '') {
                $('#error-email').html(data.error).show();
            } else {
                $('#frmRecover').html('<div class="alert alert-success">' + data.msg + '</div>');
                setTimeout(function() {
                    location.href = '<?php echo base_url();?>superadmin/login';
                }, 3000);
            }
        });        
    }
    
    $(function() {
        $('input').keyup(function(e){
            if(e.keyCode == 13)
            {
                Recover();
            }
        });
    });
</script>