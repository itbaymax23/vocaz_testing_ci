<body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="frmRegister">
              <h1>Register a new account</h1>
              <div>
                <input type="text" id="name" class="form-control" placeholder="School Name" required="" />
                <div class="alert alert-danger" id="error-name" style="display: none;"></div>
              </div>              
              <div>
                <input type="text" id="address" class="form-control" placeholder="Address - Ex: 44 E 2nd St, New York, NY 10003, EE. UU" required="" />
                <div class="alert alert-danger" id="error-address" style="display: none;"></div>
              </div>
              <div>
                <input type="text" id="email" class="form-control" placeholder="Email" required="" />
                <div class="alert alert-danger" id="error-email" style="display: none;"></div>
              </div>
              <div>
                <input type="text" id="username" class="form-control" placeholder="Username" required="" />
                <div class="alert alert-danger" id="error-username" style="display: none;"></div>
              </div>           
              <div>
                <input autocomplete="off" type="password" id="password" class="form-control" placeholder="Password" required="" />
                <div class="alert alert-danger" id="error-password" style="display: none;"></div>
              </div>
              <div>
                <input autocomplete="off" type="password" id="password2" class="form-control" placeholder="Repeat the password" required="" />
                <div class="alert alert-danger" id="error-password2" style="display: none;"></div>
              </div>
              <div>
                <a class="btn btn-default submit" href="javascript:;" onclick="Register()">Register</a>
                <a class="reset_pass" href="<?php echo base_url();?>admin/lost_your_password">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <!--h1><i class="fa fa-paw"></i> Gentelella Alela!</h1-->
                  <p>©2016 All Rights Reserved. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
</body>
<script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
<script>
    function Register() {
        $('.alert-danger').hide();
        
        email = $('#email').val();
        password = $('#password').val();
        password2 = $('#password2').val();
        name = $('#name').val();
        username = $('#username').val();
        address = $('#address').val();
        
        error = false;
        
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(email.toLowerCase())) {
            error = true;
            $('#error-email').html('The Email entered is not valid.').show();
        }

        if(email.length < 2) {
            error = true;
            $('#error-email').html('Enter your Email.').show();
        }
                
        if(password.length < 6 || password.length > 20) {
            error = true;
            $('#error-password').html('The password must contain between 6 and 20 characters.').show();
        }
        
        if(password != password2) {
            error = true;
            $('#error-password2').html('Passwords do not match.').show();            
        }
        
        if(name.length < 2) {
            error = true;
            $('#error-name').html('Enter the School name.').show();             
        }
        
        if(username.length < 2) {
            error = true;
            $('#error-username').html('Enter a username.').show();            
        }
        
        
        if(error) return;
        
        $.post( "<?php echo base_url();?>/admin/ajax_register", {email: email, password: password, name: name, username: username, address: address}, function( data ) {

        }, "json").done(function( data ) {
            if(data.error != '') {
                if(data.error == 'uname_used') {
                    $('#error-username').html('The username is already in use.').show();
                } else {
                    $('#error-email').html(data.error).show();    
                }
            } else {
                $('#frmRegister').html('<div class="alert alert-success">' + data.msg + '</div>');
                setTimeout(function() {
                    location.href = '<?php echo base_url();?>admin/login';
                }, 4000);
            }
        });        
    }
    
    $(function() {
        $('input').keyup(function(e){
            if(e.keyCode == 13)
            {
                Register();
            }
        });
    });
</script>