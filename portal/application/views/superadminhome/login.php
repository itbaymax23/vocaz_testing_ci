<body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form>
              <h1>Super Administrator Login</h1>

              <div>
                <input type="text" id="email" class="form-control" placeholder="Email or Username" required="" />
                <div class="alert alert-danger" id="error-email" style="display: none;"></div>
              </div>
              <div>
                <input type="password" id="password" class="form-control" placeholder="Password" required="" />
                <div class="alert alert-danger" id="error-password" style="display: none;"></div>
              </div>
              <div>
                <a class="btn btn-default submit" href="javascript:;" onclick="Login()">Log in</a>
                <a class="reset_pass" href="<?php echo base_url();?>superadmin/lost_your_password">Lost your password?</a>
              </div>
              <p>&nbsp;</p>
              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <!--h1><i class="fa fa-paw"></i> Gentelella Alela!</h1-->
                  <p>©2016 All Rights Reserved. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
</body>
<script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    function Login() {
        $('.alert-danger').hide();
        
        email = $('#email').val();
        password = $('#password').val();
        
        error = false;
        
        /*var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(email.toLowerCase())) {
            error = true;
            $('#error-email').html('The Email entered is not valid.').show();
        }*/

        if(email.length < 2) {
            error = true;
            $('#error-email').html('Enter the Email or Username.').show();
        }
                
        if(password.length < 6 || password.length > 20) {
            error = true;
            $('#error-password').html('The password must contain between 6 and 20 characters.').show();
        }
        
        if(error) return;
        
        $.post( "<?php echo base_url();?>/superadmin/ajax_login", {email: email, password: password}, function( data ) {

        }, "json").done(function( data ) {
            if(data.error != '') {
                $('#error-email').html(data.error).show();
            } else {
                location.href = '<?php echo base_url();?>' + data.redir;
            }
        });        
    }

    $(function() {
        $('input').keyup(function(e){
            if(e.keyCode == 13)
            {
                Login();
            }
        });
    });
    window.localStorage.setItem('session_data', '');
    session_data = window.localStorage.getItem('session_data');
    console.log(session_data);
</script>