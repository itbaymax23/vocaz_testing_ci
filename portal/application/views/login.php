<div class="container">
	<div class="row">
        <div class="col-md-6 offset-md-3">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="card">
              <h5 class="card-header">Login</h5>
              <div class="card-body">
                <form>
                  <div class="form-group">
                    <input style="margin-bottom: 0;" type="number" min="0" max="9999" maxlength="4" id="school" onkeyup="CheckMaxChar(this, 4)" class="form-control" placeholder="School ID" required="" />
                    <div class="alert alert-danger" id="error-school" style="display: none;"></div>
                    <input type="text" class="form-control" id="search-school" style="display: none;" placeholder="Enter the name of the School" />
                    <p class="help-block" style="margin-bottom: 10px;"><a href="javascript:;" onclick="OpenSearchSchool()">Don't remember your School ID? <i class="fa fa-search"></i></a></p>
                  </div>        
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address or Username</label>
                    <input type="text" id="email" class="form-control" placeholder="Email or Username" required="" />
                    <div class="alert alert-danger" id="error-email" style="display: none;"></div>
                  </div>
                  <div class="form-group">
                    <input type="password" id="password" class="form-control" placeholder="Password" required="" />
                    <div class="alert alert-danger" id="error-password" style="display: none;"></div>
                  </div>
                  <a class="btn btn-primary" href="javascript:;" onclick="Login()">Log in</a>
                  <br />
                  <a class="reset_pass" href="<?php echo base_url();?>index.php/admin/lost_your_password">Lost your password?</a>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    function Login() {
        $('.alert-danger').hide();
        
        email = $('#email').val();
        password = $('#password').val();
        school = $('#school').val();
        
        error = false;
        
        /*var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(email.toLowerCase())) {
            error = true;
            $('#error-email').html('The Email entered is not valid.').show();
        }*/

        if(email.length < 2) {
            error = true;
            $('#error-email').html('Enter the Email or Username.').show();
        }
                
        if(password.length < 6 || password.length > 20) {
            error = true;
            $('#error-password').html('The password must contain between 6 and 20 characters.').show();
        }
        
        if(school.length == 0) {
            error = true;
            $('#error-school').html('Enter the School ID.').show();            
        }
        
        if(error) return;
        
        $.post( "<?php echo base_url();?>admin/ajax_login", {email: email, password: password, school: school}, function( data ) {

        }, "json").done(function( data ) {
            if(data.error != '') {
                $('#error-email').html(data.error).show();
            } else {
                window.sessionStorage.setItem('session', data.redir);
                location.href = '<?php echo base_url();?>' + data.redir;
            }
        });        
    }
    
    function OpenSearchSchool() {
        $('#search-school').slideToggle('slow');
    }
    
    function CheckMaxChar(obj, num) {
        if($(obj).val().toString().length > num) {
            $(obj).val($(obj).val().substr(0, num));
        }
    }
    
    $(function() {
        $( "#search-school" ).autocomplete({
          minLength: 3,
          source: "<?php echo base_url();?>admin/ajax_search_school",
          focus: function( event, ui ) {
            $( "#search-school" ).val( ui.item.label );
            return false;
          },
          select: function( event, ui ) {
            $( "#search-school" ).val( ui.item.label );
            $( "#school" ).val( ui.item.value );
            return false;
          }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
          return $( "<li>" )
            .append( "<div><strong>" + item.label + "</strong><br><font size='-1'>" + item.address + "</font></div>" )
            .appendTo( ul );
        };
    });
    
    $(function() {
        $('input').keyup(function(e){
            if(e.keyCode == 13)
            {
                Login();
            }
        });
    });
</script>