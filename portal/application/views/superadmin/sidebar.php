<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url();?>superadmin_dashboard"><i class="fa fa-home"></i> Dashboard</a></li>
      <li><a href="<?php echo base_url();?>superadmin_schools"><i class="fa fa-home"></i> Schools</a></li>
      <!-- <li><a href="<?php echo base_url();?>superadmin_settings"><i class="fa fa-cog"></i> Settings</a></li> -->
    </ul>
  </div>
  
  <div class="menu_section">
    <h3>Live On</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo base_url();?>superadmin_dashboard/recording"><i class="fa fa-user"></i> Recording</a></li>
      <li><a href="<?php echo base_url();?>superadmin_settings"><i class="fa fa-cog"></i> Settings</a></li>  
    </ul>
  </div>
</div>