  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url();?>admin_dashboard" class="site_title"><i class="fa fa-envelope"></i> <span style="font-size: 18px;">Vocab Management</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $user->name;?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <?php echo $sidebar;?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a href="<?php echo base_url();?>admin_settings" data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>admin/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>assets/gentelella-master/production/images/img.jpg" alt=""><?php echo $user->name;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <a href="<?php echo base_url();?>admin_settings">
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="<?php echo base_url();?>admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <style>
                .x_title h2 {
                    font-size: 16px;
                    font-weight: bold;
                }
            </style>
            <div class="page-title">
              <h1 class="text-center">Recordings</h1>
              <h3 class="text-center">List of Words</h3>  
            </div>
            <p>&nbsp;</p>
            <style>
                ul.widget_tally .month {
                    width: 30%;
                    float: left;
                    text-align: left;
                }
                ul.widget_tally .count {
                    width: 69%;
                    float: left;
                    text-align: right;
                }
            </style>
            <div class="row" style="clear: both; margin-top: 60px;">
                <?php foreach($words as $key => $item):?>
                      <div class="col-md-4 col-xs-12 widget widget_tally_box" style="max-width: 100%;">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>
                                <?php if($key == 'level1'):?>
                                TK - Kinder
                                <?php endif;?>
                                <?php if($key == 'level2'):?>
                                1st - 3th Grade
                                <?php endif;?>
                                <?php if($key == 'level3'):?>
                                4th - 8th Grade
                                <?php endif;?>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <!--li><a class="close-link"><i class="fa fa-close"></i></a-->
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                            <div>
                              <ul class="list-inline widget_tally">
                                <?php foreach($item as $kstu => $stud):?>
                                <li>
                                  <p style="font-size: 20px;">
                                    <span class="month"><?php echo $stud->word;?></span>
                                    <span class="count">
                                        <?php if(!empty($stud->audio)):?>
                                            <a href="javascript:;" style="color: #00ff00;" onclick="PlayAudio('<?php echo $stud->audio;?>')"><i class="fa fa-play"></i></a> <a href="javascript:;"onclick="UploadNewAudio(<?php echo $stud->id;?>, 'admin', '<?php echo $stud->word;?>')" style="font-size: 14px; font-weight: bold; color: black">New Recording</a> <a href="javascript:;" onclick="RemoveAudio(<?php echo $stud->id;?>, 'admin')"><i style="color: red;" class="fa fa-trash"></i></a>
                                        <?php else:?>
                                            <a href="javascript:;" onclick="UploadNewAudio(<?php echo $stud->id;?>, 'admin', '<?php echo $stud->word;?>')"><i class="fa fa-cloud-upload"></i></a>
                                        <?php endif;?>
                                    </span>
                                  </p>
                                </li>
                                <?php endforeach;?>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                <?php endforeach;?>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            All rights reserved - &COPY; <?php echo date("Y");?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    
    <div class="modal fade" tabindex="-1" role="dialog" id="modalUploadFile">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Upload MP3 Audio for <span style="color: orange; font-weight: bold; font-size: 18px;" id="text-audio"></span></h4>
          </div>
          <div class="modal-body">
                <form action="<?php echo base_url();?>superadmin_dashboard/ajax_upload_audio" method="post" class="dropzone" id="my-awesome-dropzone">
                    <div class="row"><div class="col-md-12 text-center"><strong>Drop files here or click to upload</strong></div></div>
                </form>
                <input type="hidden" id="word_id_audio" />
                <input type="hidden" id="word_audio_filename" />
                <div id="audio-preview" style="margin-top: 15px;"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary" onclick="SaveFileTeacher()">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="modal fade" tabindex="-1" role="dialog" id="modalAudio">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div id="body-audio"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/nprogress/nprogress.js"></script>
    <!-- Dropzone.js -->
    <script src="<?php echo base_url();?>assets/gentelella-master/vendors/dropzone/dist/min/dropzone.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/gentelella-master/build/js/custom.min.js"></script>
    <script>
        $(function(){
          Dropzone.options.myAwesomeDropzone = {
            maxFilesize: 5,
            addRemoveLinks: true,
            dictResponseError: 'Server not Configured',
            acceptedFiles: ".mp3",
            init:function(){
              var self = this;
              // config
              self.options.addRemoveLinks = true;
              self.options.dictRemoveFile = "Delete";
              //New file added
              self.on("addedfile", function (file) {
                console.log('new file added ', file);
              });
              // Send file starts
              self.on("sending", function (file, xhr, data) {
                data.append("word_id", $('#word_id_audio').val());
                console.log('upload started', file);
                $('.meter').show();
              });
              
              // File upload Progress
              self.on("totaluploadprogress", function (progress) {
                console.log("progress ", progress);
                $('.roller').width(progress + '%');
              });
        
              self.on("queuecomplete", function (progress) {
                $('.meter').delay(999).slideUp(999);
              });
              
              self.on("success", function(file, response) {
                $('#word_audio_filename').val(response);
                
                $('#audio-preview').html('<audio controls><source src="<?php echo base_url();?>uploads/audios/'+response+'" type="audio/mpeg"></audio>');
                
              });
              
              // On removing file
              self.on("removedfile", function (file) {
                console.log(file);
                $('#audio-preview').html('');
              });
            }
          };
        })
        
        function UploadNewAudio(id, type, txt) {
            Dropzone.forElement('#my-awesome-dropzone').removeAllFiles(true);
            $('#audio-preview').html('');
            $('#modalUploadFile').modal('show');
            $('#word_id_audio').val(id);
            $('#text-audio').text(txt);
        }
        
        function SaveFileTeacher() {
            word_id = $('#word_id_audio').val();
            filename = $('#word_audio_filename').val();
            $.post( "<?php echo base_url();?>superadmin_dashboard/ajax_set_audio", {word_id: word_id, filename: filename}, function( data ) {
    
            }, "json").done(function( data ) {
                location.reload();
            });            
        }
        
        function RemoveAudio(id, type) {
            if(!confirm('Sure you want to remove this audio?')) return;
            $.post( "<?php echo base_url();?>superadmin_dashboard/ajax_remove_audio", {id: id}, function( data ) {
    
            }, "json").done(function( data ) {
                location.reload();
            }); 
        }
        
        function PlayAudio(audio) {
            $('#body-audio').html('<audio controls><source src="<?php echo base_url();?>uploads/audios/'+audio+'" type="audio/mpeg"></audio>');
            $('#modalAudio').modal('show');
        }
    </script>
	
  </body>
  
    <style>
        .dropzone,
        .dropzone *,
        .dropzone-previews,
        .dropzone-previews * {
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
        }
        .dropzone {
          position: relative;
          border: 1px solid rgba(0,0,0,0.08);
          background: rgba(0,0,0,0.02);
          padding: 1em;
        }
        .dropzone.dz-clickable {
          cursor: pointer;
        }
        .dropzone.dz-clickable .dz-message,
        .dropzone.dz-clickable .dz-message span {
          cursor: pointer;
        }
        .dropzone.dz-clickable * {
          cursor: default;
        }
        .dropzone .dz-message {
          opacity: 1;
          -ms-filter: none;
          filter: none;
        }
        .dropzone.dz-drag-hover {
          border-color: rgba(0,0,0,0.15);
          background: rgba(0,0,0,0.04);
        }
        .dropzone.dz-started .dz-message {
          display: none;
        }
        .dropzone .dz-preview,
        .dropzone-previews .dz-preview {
          background: rgba(255,255,255,0.8);
          position: relative;
          display: inline-block;
          margin: 17px;
          vertical-align: top;
          border: 1px solid #acacac;
          padding: 6px 6px 6px 6px;
        }
        .dropzone .dz-preview.dz-file-preview [data-dz-thumbnail],
        .dropzone-previews .dz-preview.dz-file-preview [data-dz-thumbnail] {
          display: none;
        }
        .dropzone .dz-preview .dz-details,
        .dropzone-previews .dz-preview .dz-details {
          width: 100px;
          height: 100px;
          position: relative;
          background: #ebebeb;
          padding: 5px;
          margin-bottom: 22px;
        }
        .dropzone .dz-preview .dz-details .dz-filename,
        .dropzone-previews .dz-preview .dz-details .dz-filename {
          overflow: hidden;
          height: 100%;
        }
        .dropzone .dz-preview .dz-details img,
        .dropzone-previews .dz-preview .dz-details img {
          position: absolute;
          top: 0;
          left: 0;
          width: 100px;
          height: 100px;
        }
        .dropzone .dz-preview .dz-details .dz-size,
        .dropzone-previews .dz-preview .dz-details .dz-size {
          position: absolute;
          bottom: -28px;
          left: 3px;
          height: 28px;
          line-height: 28px;
        }
        .dropzone .dz-preview.dz-error .dz-error-mark,
        .dropzone-previews .dz-preview.dz-error .dz-error-mark {
          display: block;
        }
        .dropzone .dz-preview.dz-success .dz-success-mark,
        .dropzone-previews .dz-preview.dz-success .dz-success-mark {
          display: block;
        }
        .dropzone .dz-preview:hover .dz-details img,
        .dropzone-previews .dz-preview:hover .dz-details img {
          display: none;
        }
        .dropzone .dz-preview .dz-success-mark,
        .dropzone-previews .dz-preview .dz-success-mark,
        .dropzone .dz-preview .dz-error-mark,
        .dropzone-previews .dz-preview .dz-error-mark {
          display: none;
          position: absolute;
          width: 40px;
          height: 40px;
          font-size: 30px;
          text-align: center;
          right: -10px;
          top: -10px;
        }
        .dropzone .dz-preview .dz-success-mark,
        .dropzone-previews .dz-preview .dz-success-mark {
          color: #8cc657;
        }
        .dropzone .dz-preview .dz-error-mark,
        .dropzone-previews .dz-preview .dz-error-mark {
          color: #ee162d;
        }
        .dropzone .dz-preview .dz-progress,
        .dropzone-previews .dz-preview .dz-progress {
          position: absolute;
          top: 100px;
          left: 6px;
          right: 6px;
          height: 6px;
          background: #d7d7d7;
          display: none;
        }
        .dropzone .dz-preview .dz-progress .dz-upload,
        .dropzone-previews .dz-preview .dz-progress .dz-upload {
          display: block;
          position: absolute;
          top: 0;
          bottom: 0;
          left: 0;
          width: 0%;
          background-color: #8cc657;
        }
        .dropzone .dz-preview.dz-processing .dz-progress,
        .dropzone-previews .dz-preview.dz-processing .dz-progress {
          display: block;
        }
        .dropzone .dz-preview .dz-error-message,
        .dropzone-previews .dz-preview .dz-error-message {
          display: none;
          position: absolute;
          top: -5px;
          left: -20px;
          background: rgba(245,245,245,0.8);
          padding: 8px 10px;
          color: #800;
          min-width: 140px;
          max-width: 500px;
          z-index: 500;
        }
        .dropzone .dz-preview:hover.dz-error .dz-error-message,
        .dropzone-previews .dz-preview:hover.dz-error .dz-error-message {
          display: block;
        }
        .dropzone {
          border: 1px solid rgba(0,0,0,0.03);
          min-height: 250px;
          -webkit-border-radius: 3px;
          border-radius: 3px;
          background: rgba(0,0,0,0.03);
          padding: 23px;
        }
        .dropzone .dz-default.dz-message {
          opacity: 1;
          -ms-filter: none;
          filter: none;
          -webkit-transition: opacity 0.3s ease-in-out;
          -moz-transition: opacity 0.3s ease-in-out;
          -o-transition: opacity 0.3s ease-in-out;
          -ms-transition: opacity 0.3s ease-in-out;
          transition: opacity 0.3s ease-in-out;
          background-image: url("../images/spritemap.png");
          background-repeat: no-repeat;
          background-position: 0 0;
          position: absolute;
          width: 428px;
          height: 123px;
          margin-left: -214px;
          margin-top: -61.5px;
          top: 50%;
          left: 50%;
        }
        @media all and (-webkit-min-device-pixel-ratio:1.5),(min--moz-device-pixel-ratio:1.5),(-o-min-device-pixel-ratio:1.5/1),(min-device-pixel-ratio:1.5),(min-resolution:138dpi),(min-resolution:1.5dppx) {
          .dropzone .dz-default.dz-message {
            background-image: url("../images/spritemap@2x.png");
            -webkit-background-size: 428px 406px;
            -moz-background-size: 428px 406px;
            background-size: 428px 406px;
          }
        }
        .dropzone .dz-default.dz-message span {
          display: none;
        }
        .dropzone.dz-square .dz-default.dz-message {
          background-position: 0 -123px;
          width: 268px;
          margin-left: -134px;
          height: 174px;
          margin-top: -87px;
        }
        .dropzone.dz-drag-hover .dz-message {
          opacity: 0.15;
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=15)";
          filter: alpha(opacity=15);
        }
        .dropzone.dz-started .dz-message {
          display: block;
          opacity: 0;
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
          filter: alpha(opacity=0);
        }
        .dropzone .dz-preview,
        .dropzone-previews .dz-preview {
          -webkit-box-shadow: 1px 1px 4px rgba(0,0,0,0.16);
          box-shadow: 1px 1px 4px rgba(0,0,0,0.16);
          font-size: 14px;
        }
        .dropzone .dz-preview.dz-image-preview:hover .dz-details img,
        .dropzone-previews .dz-preview.dz-image-preview:hover .dz-details img {
          display: block;
          opacity: 0.1;
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=10)";
          filter: alpha(opacity=10);
        }
        .dropzone .dz-preview.dz-success .dz-success-mark,
        .dropzone-previews .dz-preview.dz-success .dz-success-mark {
          opacity: 1;
          -ms-filter: none;
          filter: none;
        }
        .dropzone .dz-preview.dz-error .dz-error-mark,
        .dropzone-previews .dz-preview.dz-error .dz-error-mark {
          opacity: 1;
          -ms-filter: none;
          filter: none;
        }
        .dropzone .dz-preview.dz-error .dz-progress .dz-upload,
        .dropzone-previews .dz-preview.dz-error .dz-progress .dz-upload {
          background: #ee1e2d;
        }
        .dropzone .dz-preview .dz-error-mark,
        .dropzone-previews .dz-preview .dz-error-mark,
        .dropzone .dz-preview .dz-success-mark,
        .dropzone-previews .dz-preview .dz-success-mark {
          display: block;
          opacity: 0;
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
          filter: alpha(opacity=0);
          -webkit-transition: opacity 0.4s ease-in-out;
          -moz-transition: opacity 0.4s ease-in-out;
          -o-transition: opacity 0.4s ease-in-out;
          -ms-transition: opacity 0.4s ease-in-out;
          transition: opacity 0.4s ease-in-out;
          background-image: url("../images/spritemap.png");
          background-repeat: no-repeat;
        }
        @media all and (-webkit-min-device-pixel-ratio:1.5),(min--moz-device-pixel-ratio:1.5),(-o-min-device-pixel-ratio:1.5/1),(min-device-pixel-ratio:1.5),(min-resolution:138dpi),(min-resolution:1.5dppx) {
          .dropzone .dz-preview .dz-error-mark,
          .dropzone-previews .dz-preview .dz-error-mark,
          .dropzone .dz-preview .dz-success-mark,
          .dropzone-previews .dz-preview .dz-success-mark {
            background-image: url("../images/spritemap@2x.png");
            -webkit-background-size: 428px 406px;
            -moz-background-size: 428px 406px;
            background-size: 428px 406px;
          }
        }
        .dropzone .dz-preview .dz-error-mark span,
        .dropzone-previews .dz-preview .dz-error-mark span,
        .dropzone .dz-preview .dz-success-mark span,
        .dropzone-previews .dz-preview .dz-success-mark span {
          display: none;
        }
        .dropzone .dz-preview .dz-error-mark,
        .dropzone-previews .dz-preview .dz-error-mark {
          background-position: -268px -123px;
        }
        .dropzone .dz-preview .dz-success-mark,
        .dropzone-previews .dz-preview .dz-success-mark {
          background-position: -268px -163px;
        }
        .dropzone .dz-preview .dz-progress .dz-upload,
        .dropzone-previews .dz-preview .dz-progress .dz-upload {
          -webkit-animation: loading 0.4s linear infinite;
          -moz-animation: loading 0.4s linear infinite;
          -o-animation: loading 0.4s linear infinite;
          -ms-animation: loading 0.4s linear infinite;
          animation: loading 0.4s linear infinite;
          -webkit-transition: width 0.3s ease-in-out;
          -moz-transition: width 0.3s ease-in-out;
          -o-transition: width 0.3s ease-in-out;
          -ms-transition: width 0.3s ease-in-out;
          transition: width 0.3s ease-in-out;
          -webkit-border-radius: 2px;
          border-radius: 2px;
          position: absolute;
          top: 0;
          left: 0;
          width: 0%;
          height: 100%;
          background-image: url("../images/spritemap.png");
          background-repeat: repeat-x;
          background-position: 0px -400px;
        }
        @media all and (-webkit-min-device-pixel-ratio:1.5),(min--moz-device-pixel-ratio:1.5),(-o-min-device-pixel-ratio:1.5/1),(min-device-pixel-ratio:1.5),(min-resolution:138dpi),(min-resolution:1.5dppx) {
          .dropzone .dz-preview .dz-progress .dz-upload,
          .dropzone-previews .dz-preview .dz-progress .dz-upload {
            background-image: url("../images/spritemap@2x.png");
            -webkit-background-size: 428px 406px;
            -moz-background-size: 428px 406px;
            background-size: 428px 406px;
          }
        }
        .dropzone .dz-preview.dz-success .dz-progress,
        .dropzone-previews .dz-preview.dz-success .dz-progress {
          display: block;
          opacity: 0;
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
          filter: alpha(opacity=0);
          -webkit-transition: opacity 0.4s ease-in-out;
          -moz-transition: opacity 0.4s ease-in-out;
          -o-transition: opacity 0.4s ease-in-out;
          -ms-transition: opacity 0.4s ease-in-out;
          transition: opacity 0.4s ease-in-out;
        }
        .dropzone .dz-preview .dz-error-message,
        .dropzone-previews .dz-preview .dz-error-message {
          display: block;
          opacity: 0;
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
          filter: alpha(opacity=0);
          -webkit-transition: opacity 0.3s ease-in-out;
          -moz-transition: opacity 0.3s ease-in-out;
          -o-transition: opacity 0.3s ease-in-out;
          -ms-transition: opacity 0.3s ease-in-out;
          transition: opacity 0.3s ease-in-out;
        }
        .dropzone .dz-preview:hover.dz-error .dz-error-message,
        .dropzone-previews .dz-preview:hover.dz-error .dz-error-message {
          opacity: 1;
          -ms-filter: none;
          filter: none;
        }
        .dropzone a.dz-remove,
        .dropzone-previews a.dz-remove {
          background-image: -webkit-linear-gradient(top, #fafafa, #eee);
          background-image: -moz-linear-gradient(top, #fafafa, #eee);
          background-image: -o-linear-gradient(top, #fafafa, #eee);
          background-image: -ms-linear-gradient(top, #fafafa, #eee);
          background-image: linear-gradient(to bottom, #fafafa, #eee);
          -webkit-border-radius: 2px;
          border-radius: 2px;
          border: 1px solid #eee;
          text-decoration: none;
          display: block;
          padding: 4px 5px;
          text-align: center;
          color: #aaa;
          margin-top: 26px;
        }
        .dropzone a.dz-remove:hover,
        .dropzone-previews a.dz-remove:hover {
          color: #666;
        }
        @-moz-keyframes loading {
          from {
            background-position: 0 -400px;
          }
          to {
            background-position: -7px -400px;
          }
        }
        @-webkit-keyframes loading {
          from {
            background-position: 0 -400px;
          }
          to {
            background-position: -7px -400px;
          }
        }
        @-o-keyframes loading {
          from {
            background-position: 0 -400px;
          }
          to {
            background-position: -7px -400px;
          }
        }
        @keyframes loading {
          from {
            background-position: 0 -400px;
          }
          to {
            background-position: -7px -400px;
          }
        }
    </style>