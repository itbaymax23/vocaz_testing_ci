<body class="login">
<?php if(!$user):?>
    <div class="alert alert-warning">Unknow user</div>
<?php else:?>

    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="frmRecover">
              <h1>Reset your password</h1>
              <div>
                <input type="password" id="password" class="form-control" placeholder="New Password" required="" />
                <div class="alert alert-danger" id="error-password" style="display: none;"></div>
              </div>
              <div>
                <input type="password" id="password2" class="form-control" placeholder="Repeat Password" required="" />
                <div class="alert alert-danger" id="error-password2" style="display: none;"></div>
              </div>
              
              <div>
                <a class="btn btn-default submit" href="javascript:;" onclick="ResetPassword()">Reset password</a>
                <a class="reset_pass" href="<?php echo base_url();?>admin/login">Sign In</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <!--h1><i class="fa fa-paw"></i> Gentelella Alela!</h1-->
                  <p>©2016 All Rights Reserved. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>
<script src="<?php echo base_url(); ?>assets/gentelella-master/vendors/jquery/dist/jquery.min.js"></script>
<script>
    function ResetPassword() {
        $('.alert-danger').hide();
        
        password = $('#password').val();
        password2 = $('#password2').val();
        
        error = false;
        
        if(password.length < 6 || password.length > 20) {
            error = true;
            $('#error-password').html('The password must contain between 6 and 20 characters.').show();
        }
        
        if(password != password2) {
            error = true;
            $('#error-password2').html('Password do not match.').show();            
        }
        
        if(error) return;
        
        hash = '<?php echo $user->hash;?>';
        oldps = '<?php echo $user->password;?>';
        
        $.post( "<?php echo base_url();?>admin/ajax_reset_password", {password: password, hash: hash, oldps: oldps}, function( data ) {

        }, "json").done(function( data ) {
            if(data.error != '') {
                $('#error-password').html(data.error).show();
            } else {
                $('#frmRecover').html('<div class="alert alert-success">' + data.msg + '</div>');
                setTimeout(function() {
                    location.href = '<?php echo base_url();?>admin/login';
                }, 4000);
            }
        });        
    }
</script>   
<?php endif;?> 
</body>