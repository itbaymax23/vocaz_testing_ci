<p>&nbsp;</p>
<script src='https://code.responsivevoice.org/responsivevoice.js'></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<style>
#word-counter {
    font-size: 40px;
    font-weight: bold;
    border: 4px solid #000;
    float: left;
    padding: 0px;
    border-radius: 50%;
    height: 70px;
    width: 70px;
    text-align: center;
}
.word-left-counter {
    clear: both;
    width: 70px;
    text-align: center;
    font-weight: bold;
    margin-top: 5px;
}

#audio, #audio2 {
    float: right;
    border: 4px solid #000;
    border-radius: 50%;
    height: 70px;
    width: 70px;
    padding: 9px;
    cursor: pointer;
}

#btnNext, #btnBack, #btnEnd {
    font-size: 40px;
}

#response {
    border: 4px solid blue;
    font-size: 60px;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 style="text-align: center;">Vocabulary Test</h1>
            <p>&nbsp;</p>
            <?php if($finished == 1):?>
                <div class="alert alert-primary" style="max-width: 20rem; margin: auto;">You already finish your vocabulary test.</div>
            <?php else:?>
                <?php if($test):?>
                    <div id="test-end" class="card mb-3" style="max-width: 18rem; margin: auto;display: none; text-align: center;">
                      <div class="card-body" style="text-align: center;">
                        <h4 style="text-align: center;">Succcess! You have completed your vocabulary test. Click SUBMIT to submit your test.</h4>
                        <button class="btn btn-success" type="button" id="btnEnd" onclick="EndTest()">SUBMIT</button>
                      </div>
                    </div>
                    
                    <div id="test-begin" onclick="StartTest()" class="card text-white bg-success mb-3" style="max-width: 18rem; margin: auto;cursor: pointer;">
                      <div class="card-body">
                        <h2 class="card-title" style="text-align: center;">START TEST</h2>
                      </div>
                    </div>
                    <div id="test-content" style="display: none;">
                        <div class="row">
                            <div class="col-md-4">
                                <div id="audio" style="display: none;">
                                    <i class="fas fa-volume-up" style="font-size: 44px;"></i>
                                </div>
                                <div id="audio2" style="display: none;">
                                    <i class="fas fa-volume-up" style="font-size: 44px;"></i>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="audio-content" style="float: left; display: none;"></div>
                            </div>
                            <div class="col-md-4">
                                <div id="word-counter"></div>
                                <div class="word-left-counter">Word</div>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="row">
                            <div class="col-md-12">
                                <form style="max-width: 500px; margin: auto;">
                                    <div class="form-group">
                                        <textarea class="form-control" id="response" rows="1"></textarea>
                                        <p class="help-block" style="text-align: center;"><strong>Write the word in the box</strong></p>
                                        <div class="alert alert-danger" style="display: none;" id="error-write">You must enter the word.</div>
                                    </div>
                                    <button style="display: none;" id="btnBack" class="btn btn-info float-left" type="button" onclick="BackWord()">PREV</button>
                                    <button class="btn btn-success float-right" type="button" id="btnNext" onclick="SetWord()">NEXT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                <?php else:?>
                    <div class="alert alert-warning" role="alert" style="max-width: 18rem; margin: auto;">
                        There is no test to start.
                    </div>            
                <?php endif;?>            
            <?php endif;?>
        </div>
    </div>
</div>
<script>
test = <?php echo json_encode($test);?>;
words = <?php echo json_encode($words);?>;
level = <?php echo $level;?>;
test_status = 0;
answers = new Array();
current = 0;
student_test_id = null;

currentWord = null;
user_test_id = null;

function EndTest() {
    $.post( "<?php echo base_url();?>student_dashboard/ajax_end_test", {test_id: test.id}, function( data ) {

    }, "json").done(function( data ) {
        if(data.error != '') {
            $('#test-content').show();
            $('#test-begin').hide();          
            $('#test-content').html('<div class="alert alert-danger">' + data.error + '</div>');  
            return;
        }
        $('#test-end').html('<div class="alert alert-success">Test finished!</div><p>&nbsp;</p><a href="<?php echo base_url();?>student_dashboard/logout"><strong>LOGOUT</strong></a><br><br>');
        closeFullscreen();
        setTimeout(function() {
            location.href = '<?php echo base_url();?>student_dashboard/logout';
        }, 30000);
    });     
}

array_answers = new Array();

function SetWord() {
    $('#btnBack, #btnNext').attr('disabled', 'disabled');
    $('#error-write').hide();
    response = $('#response').val();
    
    if(response == '') {
        $('#error-write').show();
        $('#btnBack, #btnNext').removeAttr('disabled');
        return;
    }
    
    array_answers[current - 1] = response;
     
    $.post( "<?php echo base_url();?>student_dashboard/ajax_save_answer", {fullscreen: (fullscreen ? 1 : 0), student_test_id: student_test_id, level: level, test_id: test.id, answer: response, word_id: currentWord.id}, function( data ) {

    }, "json").done(function( data ) {
        if(data.error != '') {
            $('#test-content').show();
            $('#test-begin').hide();          
            $('#test-content').html('<div class="alert alert-danger">' + data.error + '</div>');  
            return;
        }
        if(current < words.length) {
            current++;
            SetCurrentTest();
        } else {
            $('#test-content').hide();
            $('#test-begin').hide();  
            $('#test-end').show();
        }
    }); 
}

function StartTest() {
    $.post( "<?php echo base_url();?>student_dashboard/ajax_start_test", {test_id: test.id}, function( data ) {

    }, "json").done(function( data ) {
        if(data.error != '') {
            $('#test-content').show();
            $('#test-begin').hide();          
            $('#test-content').html('<div class="alert alert-danger">' + data.error + '</div>');  
            return;
        }
        student_test_id = data.student_test_id;
        test_status = 1;
        $('#test-content').show();
        $('#test-begin').hide();
        current = 1;
        openFullscreen();
        SetCurrentTest();
    }); 
}

function BackWord() {
    if(current > 0) {
        current--;
        prevWord = array_answers[current - 1];
        console.log(prevWord);
        if(typeof(prevWord) != 'undefined') {
            $('#response').val(prevWord);
        }
        SetCurrentTest(false);
    }
}

function SetCurrentTest(clear) {
    if(current > 1) {
        $('#btnBack').show();
    } else {
        $('#btnBack').hide();
    }
    $('#btnBack, #btnNext').removeAttr('disabled');
    if(typeof(clear) != 'undefined' && clear == false) {
        
    } else {
        $('#response').val('');
    }
    
    $('#word-counter').html(current);
    currentWord = words[current - 1];
    $('#audio-content').html('');
    $('#audio, #audio2').hide();
    if(currentWord.audio != "") {
        $('#audio').show();
        $('#audio-content').html('<audio controls id="audio-player"><source src="<?php echo base_url();?>uploads/audios/'+currentWord.audio+'" type="audio/mpeg"></audio>');
        $('#audio').unbind('click');
        $('#audio').click(function() {
            $('#audio-player').trigger("play");
        });
    } else {
        $('#audio2').show();
        <?php if($teacher->recorded == 1):?>
            $('#audio2').unbind('click');
            $('#audio2').click(function() {
                responsiveVoice.speak(currentWord.word); 
            });
        <?php endif;?>
        //responsiveVoice.speak(currentWord.word);
        //alert(currentWord.word);
        //$('.word-left-counter').html(currentWord.word);
    }
}


function OnJQReady() {
    <?php if($current_word > 0 && !$finished):?>
        student_test_id = <?php echo $student_test_id;?>;
        test_status = 1;
        $('#test-content').show();
        $('#test-begin').hide();
        current = <?php echo ($current_word + 1);?>;
        openFullscreen();
        SetCurrentTest();
    <?php endif;?>
}

//not_fullscreen

var elem = document.documentElement;

/* View in fullscreen */
function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

/* Close fullscreen */
function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) { /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE/Edge */
    document.msExitFullscreen();
  }
}

fullscreen = false;

function CheckFull() {
    fullscreen = (!fullscreen); 
}

/* Standard syntax */
document.addEventListener("fullscreenchange", function() {
    CheckFull();
});

/* Firefox */
document.addEventListener("mozfullscreenchange", function() {
    CheckFull();
});

/* Chrome, Safari and Opera */
document.addEventListener("webkitfullscreenchange", function() {
    CheckFull();
});

/* IE / Edge */
document.addEventListener("msfullscreenchange", function() {
    fullscreen = (!fullscreen);
});
window.localStorage.setItem('session_data', 'student_dashboard');
session_data = window.localStorage.getItem('session_data');
console.log(session_data);
</script>