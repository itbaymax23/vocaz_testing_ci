<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'students';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin_dashboard'] = "admin_dashboard/index";
$route['admin_dashboard/(:num)/(:num)'] = "admin_dashboard/suscription_plan/$1/$2";
$route['admin_teachers'] = "admin_dashboard/teachers";
$route['admin_new_teacher'] = "admin_dashboard/new_teacher";
$route['admin_edit_teacher/(:num)'] = "admin_dashboard/edit_teacher/$1";
$route['admin_students'] = "admin_dashboard/students";
$route['admin_new_student'] = "admin_dashboard/new_student";
$route['admin_edit_student/(:num)'] = "admin_dashboard/edit_student/$1";
$route['admin_settings'] = "admin_dashboard/admin_settings";
$route['admin_create_roster'] = "admin_dashboard/create_roster";
$route['admin_import_roster'] = "admin_dashboard/import_roster";
$route['admin_recording'] = "admin_dashboard/recordings";
$route['admin_reports'] = "admin_dashboard/reports";
$route['admin_view_report/(:num)/(:num)'] = "admin_dashboard/view_report/$1/$2";
$route['admin_view_all_reports/(:num)/(:num)'] = "admin_dashboard/view_all_reports/$1/$2";
$route['admin_print_report/(:num)/(:num)'] = "admin_dashboard/print_report/$1/$2";
$route['admin_print_all_reports/(:num)/(:num)'] = "admin_dashboard/print_all_reports/$1/$2";
$route['admin_view_answer/(:num)/(:num)'] = "admin_dashboard/view_answer/$1/$2";
$route['admin_view_all_answers/(:num)/(:num)'] = "admin_dashboard/view_all_answers/$1/$2";
$route['admin_print_answer/(:num)/(:num)'] = "admin_dashboard/print_answer/$1/$2";
$route['admin_print_all_answers/(:num)/(:num)'] = "admin_dashboard/print_all_answers/$1/$2";
$route['admin_archive'] = "admin_dashboard/archive";

$route['teacher_create_roster'] = "teacher_dashboard/create_roster";
$route['teacher_import_roster'] = "teacher_dashboard/import_roster";
$route['teacher_students'] = "teacher_dashboard/students";
$route['teacher_new_student'] = "teacher_dashboard/new_student";
$route['teacher_edit_student/(:num)'] = "teacher_dashboard/edit_student/$1";
$route['teacher_settings'] = "teacher_dashboard/teacher_settings";
$route['teacher_recording'] = "teacher_dashboard/recordings";
$route['teacher_start_test'] = "teacher_dashboard/start_test";
$route['teacher_students_loggedin'] = "teacher_dashboard/students_loggedin";
$route['teacher_reports'] = "teacher_dashboard/reports";
$route['teacher_view_answer/(:num)/(:num)'] = "teacher_dashboard/view_answer/$1/$2";
$route['teacher_view_all_answers/(:num)'] = "teacher_dashboard/view_all_answers/$1";
$route['teacher_print_answer/(:num)/(:num)'] = "teacher_dashboard/print_answer/$1/$2";
$route['teacher_print_all_answers/(:num)'] = "teacher_dashboard/print_all_answers/$1";
$route['teacher_view_report/(:num)/(:num)'] = "teacher_dashboard/view_report/$1/$2";
$route['teacher_view_all_reports/(:num)'] = "teacher_dashboard/view_all_reports/$1";
$route['teacher_print_report/(:num)/(:num)'] = "teacher_dashboard/print_report/$1/$2";
$route['teacher_print_all_reports/(:num)'] = "teacher_dashboard/print_all_reports/$1";
$route['teacher_archive'] = "teacher_dashboard/archive";
$route['teacher_analysis'] = "teacher_dashboard/analysis";
//$route['student_dashboard'] = "student_dashboard/index";

$route['superadmin_dashboard'] = "superadmin_dashboard/index";
$route['superadmin_settings'] = "superadmin_dashboard/settings";
$route['superadmin_schools'] = "superadmin_dashboard/schools";

//$route['admin_dashboard/ajax_new_teacher'] = "admin_dashboard/ajax_new_teacher";