<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_dashboard extends CI_Controller {
    var $usuario = null;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
     
    function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        if(!$this->session->userdata('id') || $this->session->userdata('type') != 'student') {
            redirect('/admin/login');
        } else {
    	    $uid = $this->session->userdata('id');
            $this->load->model('Authentication_model');
            $user = $this->Authentication_model->get_user_by_id($uid);
            $this->usuario = $user;
        }
    }

    public function load_layout($view, $params = null)
    {

        // Paso por parámetro la vista $view al layout y la muestro
        $params['user'] = $this->usuario;
        $params['sidebar'] = $this->load->view('admin_student/sidebar', array(), true);
        $data = array();
        $data['content'] = $this->load->view($view, $params, true);
        
        $this->load->view('admin_student/layout', $data, false);
    }
    
    public function ajax_save_answer() {
        $this->load->model('Test_model');
        $this->load->helper('words_helper');
        $this->load->model('Words_model');
        
        $test_id = $this->input->post('test_id');
        $fullscreen = $this->input->post('fullscreen');
        
        $test = $this->Test_model->get($test_id);
        if($test->status == 2) {
            echo json_encode(array('error' => 'The test is over.'));die;
        }
        
        $word_id = $this->input->post('word_id');
        $answer = $this->input->post('answer');
        $level = $this->input->post('level');
        $student_test_id = $this->input->post('student_test_id');
        
        $studen_test = $this->Test_model->get_student_test($test_id, $this->usuario->id);
        if($studen_test->banned == 1) {
            echo json_encode(array('error' => 'Your test has been freezed. Talk with your teacher.'));die;
        }
        
        if(intval($fullscreen) == 0) {
            $this->Test_model->fullscreen_off($studen_test->student_id, $test_id);
        }
        
        $data = array('students_tests_id' => $student_test_id, 'word_id' => $word_id, 'answer' => $answer);
        
        $word = $this->Words_model->get_word_array($word_id);

        $puntaje = get_puntaje_word($answer, $level, $word);

        $data = array_merge($data, $puntaje);
        
        $workspace_id = $this->session->userdata('workspace_id');
        $data['workspace_id'] = $workspace_id;
        $this->Test_model->student_insert_answer($data);
        echo json_encode(array('error' => ''));   
    }
    
    public function ajax_end_test() {
        $this->load->model('Test_model');
        $test_id = $this->input->post('test_id');    
        $this->Test_model->end_test($this->usuario->id, $test_id);
        echo json_encode(array('error' => ''));  
    }
    
    public function ajax_start_test() {
        $this->load->model('Test_model');
        $test_id = $this->input->post('test_id');
        
        $test = $this->Test_model->get($test_id);
        if($test->status == 2) {
            echo json_encode(array('error' => 'The test is over.'));die;
        }
        
        $workspace_id = $this->session->userdata('workspace_id');
        $sti = $this->Test_model->student_start_test($test_id, $this->usuario->id, $workspace_id);
        echo json_encode(array('error' => '', 'student_test_id' => $sti));
    }
    
	public function index()
	{
	   $this->load->model('Test_model');
       $this->load->model('Teacher_model');
       $this->load->model('Words_model');
       
	   $test = $this->Test_model->get_by_roster($this->usuario->roster_id, 1);
       $teacher = $this->Teacher_model->get_teacher_by_id($test->teacher_id);
       $words = $this->Words_model->get_all_by_level($teacher->level);
       
       $words_array = array();
       
       foreach($words as $key => $item) {
           //$words[$key]->audio = ""; 
           if($teacher->recorded) {
                $audioRow = $this->Words_model->get_audio_teacher($teacher->id, $item->id);
                if(!$audioRow) {
                    $audioRow = $this->Words_model->get_audio_teacher($teacher->workspace_id, $item->id);
                }
                if($audioRow) {
                    $words[$key]->audio = $audioRow->audio;
                }
                //$words[$key]->audio = (!empty($audioRow) ? $audioRow->audio : "");            
           }
           
           $words_array[] = array('id' => $item->id, 'audio' => $words[$key]->audio, 'word' => $item->word);
       }
       
       $res_array = $this->Test_model->get_current_word_test($test->id, $this->usuario->id);
       $current_word = $res_array['count'];
       $student_test_id = $res_array['sti'];
       
       $finished = 0;
       if($this->Test_model->is_test_finished($test->id, $this->usuario->id)) {
            $finished = 1;
       }

	   $this->load_layout('admin_student/index', array('teacher' => $teacher, 'finished' => $finished, 'student_test_id' => $student_test_id, 'current_word' => intval($current_word), 'test' => $test, 'words' => $words_array, 'level' => $teacher->level));
	}
    
    public function logout() {
        $this->load->library('session');
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('hash');
        $this->session->unset_userdata('name');
        $this->session->sess_destroy();
        redirect('/students/login');
    }
    
    public function login() {
        $this->load_layout('admin_student/login', array('saludo' => 'Hola Mundo!'));
    }
}
