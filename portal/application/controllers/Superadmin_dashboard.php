<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Superadmin_dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        if(!$this->session->has_userdata('id') || $this->session->userdata('type') != 'superadmin') {
            redirect('/superadmin/login');
        }
    }
    
    public function load_layout($view, $params = null)
    {
        // Paso por parámetro la vista $view al layout y la muestro
        $this->load->model('Authentication_model');
        $uid = $this->session->userdata('id');
        $user = $this->Authentication_model->get_user_by_id($uid);
        $params['user'] = $user;
        $params['sidebar'] = $this->load->view('superadmin/sidebar', array(), true);
        $data = array();
        $data['content'] = $this->load->view($view, $params, true);
        $this->load->view('superadmin/layout', $data, false);
    }
    
    public function ajax_remove_audio() {
        $this->load->model('Words_model');
        $word_id = $this->input->post('id');
        $this->Words_model->update(array('audio' => ''), $word_id);
        echo json_encode(array('error' => ''));die;
    }
    
    public function ajax_set_audio() {
        $word_id = $this->input->post('word_id');
        $filename = $this->input->post('filename');
        $this->load->model('Words_model');
        $this->Words_model->update(array('audio' => $filename), $word_id);
        //$this->Words_model->add_audio_teacher($filename, $id, $word_id);
        echo json_encode(array('error' => ''));die;
    }
    
    public function ajax_upload_audio() {
       if(!empty($_FILES['file']['name'])){
    
         $word_id = $this->input->post('word_id');   
         // Set preference
         $filename = time() . '_' . $word_id . '_audio.mp3';
         
         $config['upload_path'] = dirname(dirname(dirname(__FILE__))) . "/uploads/audios/";
         $config['allowed_types'] = '*';
         $config['max_size'] = '20480'; // max_size in kb
         $config['file_name'] = $filename;
    
         //Load upload library
         $this->load->library('upload',$config); 
    
         // File upload
         if($this->upload->do_upload('file')){
           // Get data about the file
           $uploadData = $this->upload->data();
           echo $filename;
         } else {
           echo $this->upload->display_errors();
         }
       }        
    }
    
    public function recording() {
        $this->load->model('Words_model');
        
        $id = $this->session->userdata('id');
        $words = $this->Words_model->get_all_superadmin($id);
        
        $this->load_layout('superadmin/recordings', array('words' => $words));
    }
    
    public function ajax_edit_admin() {
        $this->load->model('Authentication_model');
        
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $hash = sha1($email . $password);
        $hashed_password = sha1(base_url() . $password);
        $data = array(
            'email' => $email,
            'name' => $name,
            'hash' => $hash,
            'username' => $username
        ); 
        
        if(!empty($password)) {
            $data['password'] = $hashed_password;
        }
        
        $user = $this->Authentication_model->get_user_by_email($email);
        
        if($user && $user->id != $id) {
            echo json_encode(array('error' => 'The Email is already in use.'));die;
        }
        
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username($username);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }            
        
        $this->Authentication_model->update($data, $id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The settings has been updated.'));die;        
    }
    
    public function settings() {
        $this->load->model('Authentication_model');
        
        $id = $this->session->userdata('id');
        $user = $this->Authentication_model->get_user_by_id($id);
        $this->load_layout('superadmin/settings', array('user' => $user));         
    }
    
    public function logout() {
        $this->load->library('session');
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('hash');
        $this->session->unset_userdata('name');
        $this->session->sess_destroy();
        redirect('/superadmin/login');
    }
    
    public function ajax_new_school() {
        $this->load->model('Authentication_model');
        $this->load->model('Student_model');
        $this->load->model('Plan_model');
        
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $address = $this->input->post('address');
        $hash = sha1($email . $password);
        $hashed_password = sha1(base_url() . $password);
        $data = array(
            'email' => $email,
            'password' => $password,
            'name' => $name,
            'type' => 'admin',
            'hash' => $hash,
            'status' => 1,
            'username' => $username,
            'address' => $address
        );
        
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username($username);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }
        
        $Student_id = $this->Student_model->add_student($data);
        if($Student_id == -1) {
            echo json_encode(array('error' => 'The Email is already in use.'));die;
        }

        /*$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'vincentkonica@gmail.com',
            'smtp_pass' => '1911eagles',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
                
        $this->load->library('email', $config);
        $this->email->from('info@jgalarza.com', 'Vocabulary');
        $this->email->to($email);
         
        $this->email->subject('Invitation to join Vocabulary');
        
        $hash_url = base_url() . 'admin/accept_invitation/' . $hash . '/' . $hashed_password;
        
        $message = '<a href="' . $hash_url . '">Join Vocabulary</a>';
        
        $this->email->message($message);*/

        $school_id = str_pad($Student_id, 4, '0', STR_PAD_LEFT);

        $this->Authentication_model->set_school_id($school_id, $Student_id);
        
        // $dias_trial = $this->Plan_model->get_option('trial_days')->value;
        $dias_trial = $this->Plan_model->get_plan_by_name('TRIAL')->during_days;
        
        $this->Plan_model->add_trial($dias_trial, $Student_id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The school has been created.'));die;        
    }
    
    public function ajax_remove_school() {
        $this->load->model('Student_model');
        $id = $this->input->post('id');
        $this->Student_model->delete_student($id);
        echo json_encode(array('error' => ''));        
    }
    
    public function ajax_edit_school() {
        $this->load->model('Authentication_model');
        
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $hash = sha1($email . $password);
        $hashed_password = sha1(base_url() . $password);
        $data = array(
            'email' => $email,
            'name' => $name,
            'hash' => $hash,
            'username' => $username
        ); 
        
        if(!empty($password)) {
            $data['password'] = $hashed_password;
        }
        
        $user = $this->Authentication_model->get_user_by_email($email);
        
        if($user && $user->id != $id) {
            echo json_encode(array('error' => 'The Email is already in use.'));die;
        }
        
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username($username);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }            
        
        $this->Authentication_model->update($data, $id);
        
        $trial = $this->input->post('trial');
        if($trial > 0) {
            $this->load->model('Plan_model');
            $this->Plan_model->add_trial($trial, $id);
        }
        
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The settings has been updated.'));die;        
    }
    
    public function edit_school($sid) {
        $this->load->model('Student_model');
        $student = $this->Student_model->get_student_by_id($sid);
        $this->load_layout('superadmin/edit_school', array('school' => $student));
    }
    
    public function new_school() {
        
        $this->load_layout('superadmin/new_school', array());
    }
    
    public function schools() {
        $this->load->model('Authentication_model');
        $this->load->model('Plan_model');
        
        $schools = $this->Authentication_model->get_users_by_type('admin');
        
        foreach($schools as $key => $item) {
            $subscription = $this->Plan_model->get_current_suscription($item->id);
            if($subscription && strtotime($subscription->end_date) > time()) {
                $schools[$key]->payment_status = 'Subscribed';
            } else {
                $trial = $this->Plan_model->get_trial($item->id);
                if($trial && strtotime($trial->end_date) > time()) {
                    $schools[$key]->payment_status = 'Trial';
                } else {
                    $schools[$key]->payment_status = 'Not subscribed';
                }
            }
        }
        
        $this->load_layout('superadmin/schools', array('schools' => $schools));
    }
    
	public function index()
	{
	    $this->load->model('Plan_model');
        
        // $trial_days_input = $this->input->post('trial');
        // if($trial_days_input) {
        //     $this->Plan_model->set_option('trial_days', $trial_days_input);
        // }
        
        // $trialoff = $this->input->post('trialoff');
        // if($trialoff) {
        //     $this->Plan_model->delete_trials();
        // }
        
        // $post_price = $this->input->post('price');
        // if($post_price) {
        //     $this->Plan_model->set_option('price', intval($post_price) * 100);
        // }
        
        
        // $dias_trial = $this->Plan_model->get_option('trial_days')->value;
        
        // $price = intval($this->Plan_model->get_option("price")->value) / 100;
        
        // $this->load_layout('superadmin/dashboard', array('trial' => $trial, 'price' => $price, 'trial_days' => $dias_trial, 'plans' => $plans));

        $plans = $this->Plan_model->get_all_plans();

	    $this->load_layout('superadmin/dashboard', array('plans' => $plans));
	}
    
    public function ajax_edit_plan() {
        $this->load->model('Plan_model');
        
        $id = $this->input->post('id');
        $plan_name = $this->input->post('plan_name');
        $during_days = $this->input->post('during_days');
        $plan_price = $this->input->post('plan_price');
        $data = array(
            'plan_name' => $plan_name,
            'during_days' => $during_days,
            'plan_price' => $plan_price,
        );
        
        $this->Plan_model->update_plan($data, $id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The Plan settings has been updated.'));die;
    }
    
    public function edit_plan($id) {
        $this->load->model('Plan_model');
        $plan = $this->Plan_model->get_plan_by_id($id);
        $this->load_layout('superadmin/edit_plan', array('plan' => $plan));
    }
}