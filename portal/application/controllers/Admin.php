<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
     
    public function load_layout($view, $params = null)
    {
        // Paso por parámetro la vista $view al layout y la muestro
        $data = array();
        $data['content'] = $this->load->view($view, $params, true);
        $this->load->view('admin/layout', $data, false);
    }
    
    public function ajax_search_school() {
        $this->load->model('Authentication_model');
        
        $term = trim($this->input->get('term'));
        $uterm = strtolower($term);
        $result = $this->Authentication_model->get_schools_by_term($term);
        
        $res_array = array();
        foreach($result as $key => $item) {
            $name = strtolower($item->name);
            if(strpos($name, $uterm) !== FALSE) {
                $res_array[] = array('value' => $item->school_id, 'label' => $item->name, 'address' => $item->address);    
            }
        }
        
        echo json_encode($res_array);
    }
    
    public function ajax_register() {
        $this->load->model('Authentication_model');
        $this->load->model('Student_model');
        $this->load->model('Plan_model');
        
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $address = $this->input->post('address');

        $hash = sha1($email . $password);
        $hashed_password = sha1(base_url() . $password);
        $data = array(
            'email' => $email,
            'password' => $password,
            'name' => $name,
            'type' => 'admin',
            'hash' => $hash,
            'status' => 1,
            'username' => $username,
            'address' => $address
        );
        
        /*if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username($username);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }*/
        
        $Student_id = $this->Student_model->add_student($data, true);
        if($Student_id == -1) {
            //echo json_encode(array('error' => 'The Email is already in use.'));die;
        }
        
        $school_id = str_pad($Student_id, 4, '0', STR_PAD_LEFT);
        
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'phpmailservices@gmail.com',
            'smtp_pass' => 'phpmail123',
            'mailtype'  => 'html', 
            'charset'   => 'utf-8'
        );
        $this->load->library('email', $config);
        
        $this->config->load('customconfig');
        
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        
        $this->email->from($this->config->item('from_email'), 'Vocabulary');
        $this->email->to($email);
        $this->email->subject('Registration successfully - School ID');
        $message = '<h3>Thanks you for registering!</h3><br>Your School ID is<b>:<br>' . $school_id . '</b>';
        $this->email->message($message);
        $this->email->send();
        
        $this->Authentication_model->set_school_id($school_id, $Student_id);
        
        // $dias_trial = $this->Plan_model->get_option('trial_days')->value;
        $dias_trial = $this->Plan_model->get_plan_by_name('TRIAL')->during_days;
        
        $this->Plan_model->add_trial($dias_trial, $Student_id);
        
        echo json_encode(array('error' => '', 'msg' => 'Thanks for registering. You will receive an email with these data as soon as possible.<br>Your School ID is: <strong>'.$school_id.'</strong>'));die;        
    }
    
    public function ajax_recover() {
        $email = $this->input->post('email');
        $this->load->model('Authentication_model');
        
        $user = $this->Authentication_model->get_user_by_email($email);
        if(!$user) {
            echo json_encode(array('error' => 'The Email do not existe in the system.'));die;
        }

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'phpmailservices@gmail.com',
            'smtp_pass' => 'phpmail123',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
        
        $this->config->load('customconfig');
        
        $this->load->library('email', $config);
        $this->email->from($this->config->item('from_email'), 'Vocabulary');
        $this->email->to($email);
         
        $this->email->subject('Recover Account');
        
        $hash_url = base_url() . 'admin/reset_password/' . $user->hash . '/' . $user->password;
        
        $message = '<a href="' . $hash_url . '">Reset password</a>';
        
        $this->email->message($message);
        $this->email->send();
        
        echo json_encode(array('error' => '', 'msg' => 'Done! Check your inbox :)'. $hash_url));die;
    }
    
    public function ajax_reset_password() {
        $password = $this->input->post('password');
        $hash = $this->input->post('hash');
        $oldps = $this->input->post('oldps');
        
        $this->load->model('Authentication_model');
        
        $user = $this->Authentication_model->get_user_by_hash_and_password($hash, $oldps);
        
        if($user) {
            $this->Authentication_model->reset_password($user->id, $password);
            echo json_encode(array('error' => '', 'msg' => 'Done! You can sign in now :)'. $hash_url));die;
        } else {
            echo json_encode(array('error' => 'Something went wrong.'));die;
        }
    }
    
    public function ajax_login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $school = $this->input->post('school');
        
        $this->load->model('Authentication_model');
        
        if($email != 'superadmin') {
            $user = $this->Authentication_model->login_school($email, $password, $school);
        } else {
            $user = $this->Authentication_model->login($email, $password);
        }
                
        if($user == -1) {
            if(strpos($email, '@') !== FALSE) {
                echo json_encode(array('error' => 'The Email do not existe in the system for the School.'));die;    
            } else {
                echo json_encode(array('error' => 'The Username do not existe in the system for the School.'));die;
            }
        }
        if($user == -2) {
            echo json_encode(array('error' => 'The password is invalid.'));die;
        }
        
        $this->session->set_userdata(
            array(
                'id' => $user->id,
                'email' => $user->email,
                'type' => $user->type,
                'hash' => $user->hash,
                'name' => $user->name . (!empty($user->lname) ? ' ' . $user->lname : ''),
                'workspace_id' => ($user->type == 'admin' ? $user->id : $user->workspace_id),
            )
        );
        
        if($user->type == 'admin') {
            $this->load->model('Plan_model');
            $subscription = $this->Plan_model->get_suscription($user->school_id);
            
            if((!$subscription || strtotime($subscription->end_date) < time())) {
                $trial = $this->Plan_model->get_trial($user->school_id);
                if(!$trial || strtotime($trial->end_date) < time()) {
                    // $owner = $this->Authentication_model->get_user_by_id($user->workspace_id);
                    $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://smtp.googlemail.com',
                        'smtp_port' => 465,
                        'smtp_user' => 'phpmailservices@gmail.com',
                        'smtp_pass' => 'phpmail123',
                        'mailtype'  => 'html', 
                        'charset'   => 'iso-8859-1'
                    );
                            
                    $this->config->load('customconfig');
                            
                    $this->load->library('email', $config);
                    $this->email->from($this->config->item('from_email'), 'Vocabulary');
                    $this->email->to($user->email);
                     
                    $this->email->subject('Your trial has expired.');
                    
                    $message = 'Your trial has expired. You must choose a subscription plan to continue using Vocabulary Test.';
                    
                    $this->email->message($message);
                    $this->email->send();
                    
                    echo json_encode(array('error' => 'Your license has expire please renew it to reactivate your account.'));die;    
                } else {
                    $diff_days = floor(ceil(strtotime($trial->end_date) - time()) / 86400);
                    if($diff_days == 20) {
                        // $owner = $this->Authentication_model->get_user_by_id($user->workspace_id);
                        $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'ssl://smtp.googlemail.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'phpmailservices@gmail.com',
                            'smtp_pass' => 'phpmail123',
                            'mailtype'  => 'html', 
                            'charset'   => 'iso-8859-1'
                        );
                        
                        $this->config->load('customconfig');
                                
                        $this->load->library('email', $config);
                        $this->email->from($this->config->item('from_email'), 'Vocabulary');
                        $this->email->to($user->email);
                         
                        $this->email->subject('Your trial will expire soon.');
                        
                        $message = 'Your trial will expire in '.($diff_days).' days.';
                        
                        $this->email->message($message); 
                        $this->email->send();
                    } elseif ($diff_days == 10) {
                        // $owner = $this->Authentication_model->get_user_by_id($user->workspace_id);
                        $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'ssl://smtp.googlemail.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'phpmailservices@gmail.com',
                            'smtp_pass' => 'phpmail123',
                            'mailtype'  => 'html', 
                            'charset'   => 'iso-8859-1'
                        );
                        
                        $this->config->load('customconfig');
                                
                        $this->load->library('email', $config);
                        $this->email->from($this->config->item('from_email'), 'Vocabulary');
                        $this->email->to($user->email);
                         
                        $this->email->subject('Your trial will expire soon.');
                        
                        $message = 'Your trial will expire in '.($diff_days).' days.';
                        
                        $this->email->message($message); 
                        $this->email->send();
                    } elseif ($diff_days == 1) {
                        // $owner = $this->Authentication_model->get_user_by_id($user->workspace_id);
                        $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'ssl://smtp.googlemail.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'phpmailservices@gmail.com',
                            'smtp_pass' => 'phpmail123',
                            'mailtype'  => 'html', 
                            'charset'   => 'iso-8859-1'
                        );
                        
                        $this->config->load('customconfig');
                                
                        $this->load->library('email', $config);
                        $this->email->from($this->config->item('from_email'), 'Vocabulary');
                        $this->email->to($user->email);
                         
                        $this->email->subject('Your trial will expire soon.');
                        
                        $message = 'Your trial will expire in '.($diff_days).' day.';
                        
                        $this->email->message($message); 
                        $this->email->send();
                    }
                }
            }
        }
        
        switch($user->type) {
            case 'admin':
                $redir = 'admin_dashboard';
            break;
            case 'superadmin':
                $redir = 'superadmin_dashboard';
            break;
            case 'teacher':
                $redir = 'teacher_dashboard';
            break;
            case 'student':
                $redir = 'student_dashboard';
            break;
        }
        
        echo json_encode(array('error' => '', 'redir' => $redir));die;
    }
    
    public function logout() {
        $this->load->library('session');
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('hash');
        $this->session->unset_userdata('name');
        $this->session->sess_destroy();
        redirect('/admin/login');
    }
    
	public function index()
	{
	    
        if(!$this->session->userdata('id')) {
           redirect('/admin/login');
        }
        else {
            $type = $this->session->userdata('type');

            switch($type) {
                case 'admin':
                    redirect('/admin_dashboard');
                break;
                case 'superadmin':
                    redirect('/superadmin_dashboard');
                break;
                case 'teacher':
                    redirect('/teacher_dashboard');
                break;
                case 'student':
                    redirect('/student_dashboard');
                break;
            }
            // $this->load->view('welcome_message');
        }
	}
    
    public function lost_your_password() {
        //echo sha1(base_url() . '123456');
        $this->load_layout('admin/lost_your_password', array('saludo' => 'Hola Mundo!'));
    }
    
    public function login() {
        //echo sha1(base_url() . '123456');
        $this->load_layout('admin/login', array('saludo' => 'Hola Mundo!'));
    }
    
    public function register() {
        //echo sha1(base_url() . '123456');
        $this->load_layout('admin/register', array('saludo' => 'Hola Mundo!'));
    }
    
    public function reset_password($hash = '', $password = '') {
        //$hash = $this->uri->segment(0);
        
        $this->load->model('Authentication_model');
        $user = $this->Authentication_model->get_user_by_hash_and_password($hash, $password);
        
        $this->load_layout('admin/reset_password', array('user' => $user));
    }
    
    public function accept_invitation($hash = '', $password = '') {
        //$hash = $this->uri->segment(0);
        
        $this->load->model('Authentication_model');
        $user = $this->Authentication_model->get_user_by_hash_and_password($hash, $password);
        if($user) {
            if($user->type == 'teacher') {
                $this->load->model('Teacher_model');
                $this->Teacher_model->update_teacher(array('status' => 1), $user->id);
                $this->session->set_userdata(
                    array(
                        'id' => $user->id,
                        'email' => $user->email,
                        'type' => $user->type,
                        'hash' => $user->hash,
                        'name' => $user->name . (!empty($user->lname) ? ' ' . $user->lname : ''),
                        'workspace_id' => $user->workspace_id
                    )
                );
                redirect('teacher_dashboard');                
            } else if($user->type == 'student') {
                $this->load->model('Student_model');
                $this->Student_model->update_student(array('status' => 1), $user->id);
                $this->session->set_userdata(
                    array(
                        'id' => $user->id,
                        'email' => $user->email,
                        'type' => $user->type,
                        'hash' => $user->hash,
                        'name' => $user->name . (!empty($user->lname) ? ' ' . $user->lname : ''),
                        'workspace_id' => $user->workspace_id
                    )
                );
                redirect('teacher_dashboard');
            } else if($user->type == 'admin') {
                $this->load->model('Teacher_model');
                $this->Teacher_model->update_teacher(array('status' => 1), $user->id);
                $this->session->set_userdata(
                    array(
                        'id' => $user->id,
                        'email' => $user->email,
                        'type' => $user->type,
                        'hash' => $user->hash,
                        'name' => $user->name . (!empty($user->lname) ? ' ' . $user->lname : ''),
                        'workspace_id' => $user->workspace_id
                    )
                );
                redirect('admin_dashboard');                 
            }

        }
    }
}