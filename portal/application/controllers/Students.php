<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
     
    public function load_layout($view, $params = null)
    {
        // Paso por parámetro la vista $view al layout y la muestro
        $data = array();
        $data['content'] = $this->load->view($view, $params, true);
        $this->load->view('layout', $data, false);
 
    }
    
	public function index()
	{
	    if(!$this->session->userdata('id')) {
	       $this->login();
	    } else {
	    	$type = $this->session->userdata('type');

        	switch($type) {
	            case 'admin':
	                redirect('/admin_dashboard');
	            break;
	            case 'superadmin':
	                redirect('/superadmin_dashboard');
	            break;
	            case 'teacher':
	                redirect('/teacher_dashboard');
	            break;
	            case 'student':
	                redirect('/student_dashboard');
	            break;
	        }
			// $this->load->view('welcome_message');
	    }
	}
    
    public function login() {
        $this->load_layout('login', array('saludo' => 'Hola Mundo!'));
    }
}
