<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_dashboard extends CI_Controller {

    var $usuario = null;

    function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        if(!$this->session->has_userdata('id') || $this->session->userdata('type') != 'admin') {
            redirect('/admin/login');
        }
    }    

    public function load_layout($view, $params = null)
    {
	    $uid = $this->session->userdata('id');
        $this->load->model('Authentication_model');
        $this->load->model('Plan_model');
        $params['subscription'] = $this->Plan_model->get_suscription($uid);
        $params['trial'] = $this->Plan_model->get_trial($uid);
        $user = $this->Authentication_model->get_user_by_id($uid);
        $this->usuario = $user;
        // Paso por parámetro la vista $view al layout y la muestro
        $params['user'] = $user;
        $params['sidebar'] = $this->load->view('admin_dashboard/sidebar', array(), true);
        $data = array();
        $data['content'] = $this->load->view($view, $params, true);
        
        $this->load->view('admin_dashboard/layout', $data, false);
    }
    
    public function suscription_plan($id = null) {
        $this->load->model('Plan_model');

        // $plan_value = intval($this->Plan_model->get_option("price")->value) / 100;
        if (isset($id)) {
            if ($id != 2 && $id != 3 && $id != 4) {
                $id = 2;
            }
            $plan = $this->Plan_model->get_plan_by_id($id);
        } else {
            $id = 2;
            $plan = $this->Plan_model->get_plan_by_id($id);
        }
        $plan_value = intval($plan->plan_price);
        $plan_name = $plan->plan_name;
        $during_days = $plan->during_days;
        $data['plan_id'] = $id;
        $data['link_plan'] = $this->Plan_model->create_payment_item($id, $plan_value, $plan_name);
        $data['amount'] = $plan_value;
        $uid = $this->session->userdata('id');
        
        if(!empty($_REQUEST['success'])) {
            $payment_paypal = $this->Plan_model->execute_item_payment();

            $jsonResult = $payment_paypal->toJSON();
            
            $res_pay = json_decode($jsonResult);

            if($res_pay->state == "approved") {
                
                $plan_index = $this->input->get('product_id');
                $workspace_id = $this->session->userdata('workspace_id');
                $paid = 1;
                $amount = $plan_value;
                $this->Plan_model->add(array(
                    'owner_id' => $uid,
                    'level' => $id,
                    'amount' => $plan_value,
                    'plan' => $plan_name,
                    'start_date' => date("Y-m-d H:i:s"),
                    'end_date' => date("Y-m-d H:i:s", strtotime("+{$during_days} days")),
                    'workspace_id' => $workspace_id
                ));
                
                $this->session->set_flashdata('success', 'Your subscription plan was updated successfully. Thanks you!');                
                redirect(base_url() . 'admin_dashboard/suscription_plan');
            }
        }
        
        if($this->input->post('stripeToken')) {
            $token = $this->input->post('stripeToken');
            
            $amount = $plan_value;
            
            require_once(APPPATH . 'third_party/stripe/vendor/autoload.php');
            $paid = 0;
            try {
                \Stripe\Stripe::setApiKey('sk_test_kqRIl6hxFgBkJEPjAJBm0YGH');
                $charge = \Stripe\Charge::create(array(
                    'source' => $token,
                    'amount' => $plan_value,
                    'currency' => 'USD',
                    'description' => $plan_name
                ));
                
                $result = json_decode(json_encode($charge));
                
                if($result->paid == 1) {
                    $workspace_id = $this->session->userdata('workspace_id');
                    $paid = 1;
                    $this->Plan_model->add(array(
                        'owner_id' => $uid,
                        'level' => $id,
                        'amount' => $plan_value,
                        'plan' => $plan_name,
                        'start_date' => date("Y-m-d H:i:s"),
                        'end_date' => date("Y-m-d H:i:s", strtotime("+{$during_days} days")),
                        'workspace_id' => $workspace_id                      
                    ));
                    
                    $this->session->set_flashdata('success', 'Your subscription plan was updated successfully. Thanks you!');
                    redirect(base_url() . 'admin_dashboard/suscription_plan');
                }
            
            /*} catch(\Stripe\Error\Card $e) {
                $paid = 0;
                $this->session->set_flashdata('error', $body['error']);*/
            }catch (Exception $e) {
                $paid = 0;
                $this->session->set_flashdata('error', 'There was an error trying to use your card.');
            }
        }
    
        $data['current_plan'] = $this->Plan_model->get_suscription($uid);
        
        $this->load_layout('admin_dashboard/suscription_plan', $data);       
        
    }
    
    public function recordings() {
        $this->load->model('Words_model');
        
        $id = $this->session->userdata('id');
        $words = $this->Words_model->get_all($id);
        
        $this->load_layout('admin_dashboard/recordings', array('words' => $words));
    }
    
    public function ajax_remove_audio() {
        $this->load->model('Words_model');
        $audio_id = $this->input->post('id');
        $this->Words_model->remove_audio($audio_id);
        echo json_encode(array('error' => ''));die;
    }
    
    public function ajax_set_audio() {
        $word_id = $this->input->post('word_id');
        $filename = $this->input->post('filename');
        $this->load->model('Words_model');
        $id = $this->session->userdata('id');
        //$this->Words_model->update(array('audio' => $filename), $word_id);
        $this->Words_model->add_audio_teacher($filename, $id, $word_id);
        echo json_encode(array('error' => ''));die;
    }
    
    public function ajax_upload_audio() {
       if(!empty($_FILES['file']['name'])){
    
         $word_id = $this->input->post('word_id');   
         // Set preference
         $filename = time() . '_' . $word_id . '_audio.mp3';
         
         $config['upload_path'] = dirname(dirname(dirname(__FILE__))) . "/uploads/audios/";
         $config['allowed_types'] = '*';
         $config['max_size'] = '20480'; // max_size in kb
         $config['file_name'] = $filename;
    
         //Load upload library
         $this->load->library('upload',$config); 
    
         // File upload
         if($this->upload->do_upload('file')){
           // Get data about the file
           $uploadData = $this->upload->data();
           echo $filename;
         } else {
           echo $this->upload->display_errors();
         }
       }        
    }
    
    public function ajax_new_roster() {
        $this->load->model('Roster_model');
        $name = $this->input->post('name');
        $grade = $this->input->post('grade');
        $teacher_id = $this->input->post('teacher_id');
        
        /*$roster_used = $this->Roster_model->gt_by_teacher_id($teacher_id);
        if($roster_used) {
            $this->Roster_model->update(array('teacher_id' => 0), $roster_used->id);
        } else {
            
        }*/
        $workspace_id = $this->session->userdata('workspace_id');
        $this->Roster_model->add($name, $teacher_id, $grade, $workspace_id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! Rosted added successfully.'));die; 
    }
    
    public function create_roster() {
        $this->load->model('Teacher_model');
        $workspace_id = $this->session->userdata('workspace_id');
        $teachers = $this->Teacher_model->get_all_teachers($workspace_id);
        $this->load_layout('admin_dashboard/create_roster', array('teachers' => $teachers));
    }
    
    public function ajax_promote() {
        $this->load->model('Authentication_model');
        $this->load->model('Roster_model');
        $students = $this->input->post('students');
        $workspace_id = $this->session->userdata('id');
        foreach($students as $key => $item) {
            $user = $this->Authentication_model->get_user_by_id($item);
            $next_roster = $this->Roster_model->get_next_roster($user->roster_id, $workspace_id);
            if($next_roster == -1) {
                $this->Authentication_model->update(array('archived' => 1, 'roster_id' => 0), $user->id);
            } else {
                $this->Authentication_model->update(array('roster_id' => $next_roster), $user->id);
            }
        }
        
        echo json_encode(array('error' => ''));die;
    }
    
    public function promote_students() {
        $this->load->model('Roster_model');
        $this->load->model('Student_model');
        $this->load->model('Teacher_model');
        
        $rows = array();
        $sel_roster = null;
        
        $uid = $this->session->userdata('id');
        
        $workspace_id = $this->session->userdata('workspace_id');
        $teachers = $this->Teacher_model->get_all_teachers($workspace_id);
        
        $rosters = $this->Roster_model->get_rosters($workspace_id);
        foreach($rosters as $key => $item) {
            $rosters[$key]->students = $this->Student_model->get_students_by_roster($item->id);
        }
        
        $this->load_layout('admin_dashboard/promote_students', array('rosters' => $rosters, 'rows' => $rows, 'sel_roster' => $sel_roster, 'teachers' => $teachers));        
    }
    
    public function import_roster() {
        $this->load->model('Roster_model');
        $this->load->model('Student_model');
        $this->load->model('Teacher_model');
        
        $rows = array();
        $sel_roster = null;
        
        $uid = $this->session->userdata('id');
        
        if($this->input->post('rid')) {
            $mi_imagen = time() . '_roster.xlsx';
            $config['upload_path'] = dirname(dirname(dirname(__FILE__))) . "/uploads/rosters/";
            $config['file_name'] = $mi_imagen;
            $config['allowed_types'] = "xlsx";
            $config['max_size'] = "10240000";
        
            $this->load->library('upload', $config);
        
            if (!$this->upload->do_upload()) {
                //*** ocurrio un error
                $data['uploadError'] = $this->upload->display_errors();
                echo $this->upload->display_errors();
                return;
            } 
            $filename = dirname(dirname(dirname(__FILE__))) . '/uploads/rosters/' . $mi_imagen;
            $rows = $this->Roster_model->read_excel_roaster($filename); 
            $sel_roster_id = $this->input->post('rid'); 
            $sel_roster = $this->Roster_model->get($sel_roster_id);     
        }
        
        if($this->input->post('roster_id')) {

            $roster_id = $this->input->post('roster_id');
            $name = $this->input->post('name');
            $teacher_id = $this->input->post('teacher_id');
            
            $roster_used = $this->Roster_model->get_by_teacher_id($teacher_id);
            if($roster_used && $roster_id != $roster_used->id) {
                $this->Roster_model->update(array('teacher_id' => 0), $roster_used->id);
            }
            
            $this->Roster_model->update(array('name' => $name, 'teacher_id' => $teacher_id), $roster_id);
        }
        
        if($this->input->post('roster_delete_id')) {
            //echo $roster_delete_id;
            $this->Roster_model->reset_roster($ridlist);
            $roster_delete_id = $this->input->post('roster_delete_id');
            $this->Roster_model->delete($roster_delete_id);
        }
        /**/
        
        $workspace_id = $this->session->userdata('workspace_id');
        $teachers = $this->Teacher_model->get_all_teachers($workspace_id);
        
        $rosters = $this->Roster_model->get_rosters($workspace_id);
        foreach($rosters as $key => $item) {
            $rosters[$key]->students = $this->Student_model->get_students_by_roster($item->id);
        }
        
        $this->load_layout('admin_dashboard/rosters', array('rosters' => $rosters, 'rows' => $rows, 'sel_roster' => $sel_roster, 'teachers' => $teachers));
        
    }
    
    public function ajax_save_list() {
        $ridlist = $this->input->post('ridlist');
        $students = $this->input->post('students');
        
        $this->load->model('Roster_model');
        
        $this->Roster_model->reset_roster($ridlist);
        $roster = $this->Roster_model->get($ridlist);
        
        foreach($students as $key => $item) {
            $this->Roster_model->add_student_roster($item, $ridlist, $roster->grade);
        }
        
        echo json_encode(array('error' => ''));die;
    }
    
    public function ajax_edit_teacher() {
        $this->load->model('Teacher_model');
        $this->load->model('Authentication_model');
        
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $lname = $this->input->post('lname');
        $username = $this->input->post('username');
        $recorded = $this->input->post('recorded');
        $level = $this->input->post('level');
        $hash = sha1($email . $password);
        $hashed_password = sha1(base_url() . $password);
        $data = array(
            'email' => $email,
            'name' => $name,
            'hash' => $hash,
            'recorded' => $recorded,
            'lname' => $lname,
            'username' => $username,
            'level' => $level
        ); 
        
        if(!empty($password)) {
            $data['password'] = $hashed_password;
        }
        
        $uid = $this->session->userdata('id');
        $useradm = $this->Authentication_model->get_user_by_id($uid);
        
        $user = $this->Authentication_model->get_user_by_email_and_wid($email, $useradm->id);
        //print_r($user);
        if($user && $user->id != $id) {
            echo json_encode(array('error' => 'The Email is already in use.'));die;
        }
        
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username_and_wid($username, $useradm->id);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }
        
        $this->Teacher_model->update_teacher($data, $id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The teacher has been updated.'));die;        
    }
    
    public function ajax_edit_admin() {
        $this->load->model('Authentication_model');
        
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $hash = sha1($email . $password);
        $hashed_password = sha1(base_url() . $password);
        $data = array(
            'email' => $email,
            'name' => $name,
            'hash' => $hash,
            'username' => $username
        ); 
        
        if(!empty($password)) {
            $data['password'] = $hashed_password;
        }
        
        $uid = $this->session->userdata('id');
        $useradm = $this->Authentication_model->get_user_by_id($uid);
        
        $user = $this->Authentication_model->get_user_by_email_and_wid($email, $useradm->id);
        
        if($user && $user->id != $id) {
            echo json_encode(array('error' => 'The Email is already in use.'));die;
        }
        
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username_and_wid($username, $useradm->id);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }            
        
        $this->Authentication_model->update($data, $id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The settings has been updated.'));die;        
    }
    
    public function admin_settings() {
        $this->load->model('Authentication_model');
        
        $id = $this->session->userdata('id');
        $user = $this->usuario;
        
        $this->load_layout('admin_dashboard/settings', array('user' => $user));        
    }
    
    public function new_student() {
        $this->load->model('Roster_model');
        $workspace_id = $this->session->userdata('workspace_id');
        $rosters = $this->Roster_model->get_rosters($workspace_id);
        $this->load_layout('admin_dashboard/new_student', array('rosters' => $rosters));
    }
    
    public function edit_student($sid) {
        $this->load->model('Student_model');
        $this->load->model('Roster_model');
        $workspace_id = $this->session->userdata('workspace_id');
        $rosters = $this->Roster_model->get_rosters($workspace_id);
        $student = $this->Student_model->get_student_by_id($sid);
        $this->load_layout('admin_dashboard/edit_student', array('student' => $student, 'rosters' => $rosters));
    }
    
    public function edit_teacher($sid) {
        $this->load->model('Teacher_model');
        $teacher = $this->Teacher_model->get_teacher_by_id($sid);
        $this->load_layout('admin_dashboard/edit_teacher', array('teacher' => $teacher));
    }
    
    public function ajax_new_teacher() {
        $this->load->model('Teacher_model');
        $this->load->model('Authentication_model');
        
        $uid = $this->session->userdata('id');
        
        $useradm = $this->Authentication_model->get_user_by_id($uid);
        
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $level = $this->input->post('level');
        $hash = sha1($email . $password);
        $hashed_password = sha1(base_url() . $password);
        $workspace_id = $this->session->userdata('workspace_id');
        $data = array(
            'email' => $email,
            'password' => $password,
            'name' => $name,
            'type' => 'teacher',
            'hash' => $hash,
            'status' => 0,
            'username' => $username,
            'level' => $level,
            'workspace_id' => $workspace_id
        );

        $existe = $this->Authentication_model->get_user_by_email_and_wid($email, $useradm->id);
        if($existe) {
            echo json_encode(array('error' => 'The Email is already in use.'));die;
        } 
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username_and_wid($username, $useradm->id);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }
        
        $teacher_id = $this->Teacher_model->add_teacher($data);

        /*$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'vincentkonica@gmail.com',
            'smtp_pass' => '1911eagles',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
                        
        $this->load->library('email', $config);
        $this->email->from('info@jgalarza.com', 'Vocabulary');
        $this->email->to($email);
         
        $this->email->subject('Invitation to join Vocabulary');
        
        $hash_url = base_url() . 'admin/accept_invitation/' . $hash . '/' . $hashed_password;
        
        $message = '<a href="' . $hash_url . '">Join Vocabulary</a>';
        
        $this->email->message($message);*/
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The teacher has been created.'));die; 
    }
    
    public function new_teacher() {
        $this->load_layout('admin_dashboard/new_teacher');
    }
    
    public function ajax_edit_student() {
        $this->load->model('Student_model');
        $this->load->model('Authentication_model');
        
        $id = $this->input->post('id');
        // $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $lname = $this->input->post('lname');
        $roster_id = $this->input->post('roster_id');
        $username = $this->input->post('username');
        $hash = sha1(base_url() . $password);
        $hashed_password = sha1(base_url() . $password);
        $data = array(
            // 'email' => $email,
            'name' => $name,
            'hash' => $hash,
            'roster_id' => $roster_id,
            'lname' => $lname,
            'username' => $username
        ); 
        
        if(!empty($password)) {
            $data['password'] = $hashed_password;
        }
        
        $uid = $this->session->userdata('id');
        $useradm = $this->Authentication_model->get_user_by_id($uid);
        
        // $user = $this->Authentication_model->get_user_by_email_and_wid($email, $useradm->id);
        // //print_r($user);
        // if($user && $user->id != $id) {
        //     echo json_encode(array('error' => 'The Email is already in use.'));die;
        // }
        
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username_and_wid($username, $useradm->id);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }            
        
        $this->Student_model->update_student($data, $id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The student has been updated.'));die;
    }
    
    public function ajax_new_student() {
        $this->load->model('Student_model');
        $this->load->model('Authentication_model');
        
        $uid = $this->session->userdata('id');
        $useradm = $this->Authentication_model->get_user_by_id($uid);
        
        // $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $lname = $this->input->post('lname');
        $roster_id = $this->input->post('roster_id');
        $username = $this->input->post('username');
        $hash = sha1($email . $password);
        $hashed_password = sha1(base_url() . $password);
        $workspace_id = $this->session->userdata('workspace_id');
        $data = array(
            // 'email' => $email,
            'password' => $password,
            'name' => $name,
            'type' => 'student',
            'hash' => $hash,
            'status' => 0,
            'roster_id' => $roster_id,
            'lname' => $lname,
            'username' => $username,
            'workspace_id' => $workspace_id
        );
        
        // $existe = $this->Authentication_model->get_user_by_email_and_wid($email, $useradm->id);
        // if($existe) {
        //     echo json_encode(array('error' => 'The Email is already in use.'));die;
        // } 

        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username_and_wid($username, $useradm->id);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }
        
        $Student_id = $this->Student_model->add_student($data, true);

        /*$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'vincentkonica@gmail.com',
            'smtp_pass' => '1911eagles',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
                        
        $this->load->library('email', $config);
        $this->email->from('info@jgalarza.com', 'Vocabulary');
        $this->email->to($email);
         
        $this->email->subject('Invitation to join Vocabulary');
        
        $hash_url = base_url() . 'admin/accept_invitation/' . $hash . '/' . $hashed_password;
        
        $message = '<a href="' . $hash_url . '">Join Vocabulary</a>';
        
        $this->email->message($message);*/
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The student has been created.'));die; 
    }
    
    public function ajax_remove_student() {
        $this->load->model('Student_model');
        $id = $this->input->post('id');
        $this->Student_model->delete_student($id);
        echo json_encode(array('error' => ''));
    }
    
    public function ajax_remove_teacher() {
        $this->load->model('Teacher_model');
        $id = $this->input->post('id');
        $this->Teacher_model->delete_teacher($id);
        echo json_encode(array('error' => ''));
    }
    
    public function alumni() {
        $this->load->model('Student_model');
        
        $workspace_id = $this->session->userdata('workspace_id');
        $students = $this->Student_model->get_all_archived_students($workspace_id);
        
        $this->load_layout('admin_dashboard/alumni', array('students' => $students));         
    }
    
    public function students() {
        $this->load->model('Student_model');
        
        $workspace_id = $this->session->userdata('workspace_id');
        $students = $this->Student_model->get_all_students($workspace_id);
        
        $this->load_layout('admin_dashboard/students', array('students' => $students));        
    }
    
    public function teachers() {
        $this->load->model('Teacher_model');
        
        $workspace_id = $this->session->userdata('workspace_id');
        $teachers = $this->Teacher_model->get_all_teachers($workspace_id);
        
        $this->load_layout('admin_dashboard/teachers', array('teachers' => $teachers));
    }
    
	public function reports()
	{
	    $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $this->load->model('Teacher_model');
        $uid = $this->session->userdata('id');
        $workspace_id = $this->session->userdata('workspace_id');
        $teachers = $this->Teacher_model->get_all_teachers($workspace_id);
        
        $teache_id = $this->input->post('id');
        $year = date("Y");
        $tests = $this->Test_model->get_by_year($year, $teache_id);
        if(!$tests) {
            $archives = $this->Test_model->get_archives($teache_id);
            $year = $archives[0]->anio;
            $tests = $this->Test_model->get_by_year($year, $teache_id);
        }
        
        foreach($tests as $key => $item) {
            $tests[$key]->students = $this->Test_model->get_students_by_test($item->id);
            foreach($tests[$key]->students as $kst => $stud) {
                $tests[$key]->students[$kst]->student = $this->Authentication_model->get_user_by_id($stud->student_id);
            }
        }
	    $this->load_layout('admin_dashboard/reports', array('id' => $teache_id, 'tests' => $tests, 'year' => $year, 'teachers' => $teachers));
	}
    
    public function archive() {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $this->load->model('Teacher_model');
        $uid = $this->session->userdata('id');
        $workspace_id = $this->session->userdata('workspace_id');
        $teachers = $this->Teacher_model->get_all_teachers($workspace_id);
                
        $year = $this->input->post('year');
        if(empty($year)) {
            $year = date("Y");
        }
        
        $tid = $this->input->post('id');
        
        $tests = array();
        
        if(!empty($year)) {
            $tests = $this->Test_model->get_by_year($year, $tid);
            foreach($tests as $key => $item) {
                $tests[$key]->students = $this->Test_model->get_students_by_test($item->id);
                foreach($tests[$key]->students as $kst => $stud) {
                    $tests[$key]->students[$kst]->student = $this->Authentication_model->get_user_by_id($stud->student_id);
                }
            }            
        }
        
        $archives = $this->Test_model->get_archives($tid);
        $this->load_layout('admin_dashboard/archive', array('id' => $tid, 'archives' => $archives, 'year' => $year, 'tests' => $tests, 'teachers' => $teachers));
    }
    
    public function print_report($student_test_id, $test_id) {
        $this->view_report($student_test_id, $test_id, 1);
    }
    
    public function print_all_reports($teacherID, $year) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $tid = $this->session->userdata('id');
        
        if(!$year) {
            $year = date("Y");
        }
        
        $tests = $this->Test_model->get_tests_by_year($teacherID, $year);
        $teacher = $this->Authentication_model->get_user_by_id($teacherID);
        //print_r($tests);
        $reports = array();
        
        foreach($tests as $key => $item) {
            $students_tests = $this->Test_model->get_students_by_test($item->id);
            //print_r($students_tests);
            foreach($students_tests as $kst => $stu) {
                //print_r($stu);
                $curHTML = $this->view_report($stu->id, $stu->test_id, 0, 1);
                
                $reports[] = $curHTML;
            }
        }
        
        $html = implode('', $reports);
        
        $layoutHTML = $this->load->view('admin_teacher/view_report_l' . $teacher->level . '_all', array('content' => $html), true);

        include_once APPPATH.'third_party/mpdf-development/vendor/autoload.php';
        //if(file_exists(APPPATH.'third_party/mpdf/mpdf.php')) {echo 'EXISTE';}
        //echo APPPATH.'third_party/mpdf/mpdf.php';die;

        ini_set("pcre.backtrack_limit", "5000000");
        
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($layoutHTML);
        $mpdf->Output();
    }
    
    public function view_all_reports($teacherID, $year) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $tid = $this->session->userdata('id');
        
        if(!$year) {
            $year = date("Y");
        }

        $tests = $this->Test_model->get_tests_by_year($teacherID, $year);
        $teacher = $this->Authentication_model->get_user_by_id($teacherID);

        $reports = array();
        
        foreach($tests as $key => $item) {
            $students_tests = $this->Test_model->get_students_by_test($item->id);
            //print_r($students_tests);
            foreach($students_tests as $kst => $stu) {
                //print_r($stu);
                $curHTML = $this->view_report($stu->id, $stu->test_id, 0, 1);
                
                $reports[] = $curHTML;
            }
        }
        
        $html = implode('', $reports);
        
        $this->load->view('admin_teacher/view_report_l' . $teacher->level . '_all', array('content' => $html), false);
    }
    
    public function view_report($student_test_id, $test_id, $print = 0, $return = 0) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $this->load->model('Words_model');
        
        $tid = $this->session->userdata('id');
        $teacher = $this->Authentication_model->get_user_by_id($tid);
        
        $student_test = $this->Test_model->get_student_test_by_id($student_test_id);
        
        $student = $this->Authentication_model->get_user_by_id($student_test->student_id);

        $year = date("Y");
        $all_student_test = $this->Test_model->get_all_student_test_by_year($year, $student_test->student_id);

        //print_r($student_test);
        $test = $this->Test_model->get($test_id);
        $words = $this->Words_model->get_words_by_level_array($teacher->level);
        
        $total = 0;

        if($teacher->level == 1) {
            $results = array(
                'wo1_beginning_total' => 0,
                'wo1_final_total' => 0,
                'wo1_short_total' => 0,
                'wo1_consonant_total' => 0,
                'wo1_conblends_total' => 0,
                'wo1_longvow_total' => 0,
                'wo1_othervow_total' => 0,
                'wo1_inflected_total' => 0,
                'word_ok' => 0,
                'featured_point0' => array(),
                'correct_spelling0' => array(),
                'featured_point0_total' => 0,
                'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                'correct_spelling1' => array(),
                'featured_point1_total' => 0,
                'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                'correct_spelling2' => array(),
                'featured_point2_total' => 0,
                'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                'correct_spelling3' => array(),
                'featured_point3_total' => 0,
                'correct_spelling3_total' => 0,
            );            
        }
                
        if($teacher->level == 2) {
            $results = array(
                'wo2_beginning_total' => 0,
                'wo2_final_total' => 0,
                'wo2_short_total' => 0,
                'wo2_consonant_total' => 0,
                'wo2_conblends_total' => 0,
                'wo2_longvow_total' => 0,
                'wo2_othervow_total' => 0,
                'wo2_inflected_total' => 0,
                'wo2_syllable_total' => 0,
                'wo2_unacented_total' => 0,
                'wo2_harder_total' => 0,
                'wo2_base_total' => 0,
                'word_ok' => 0,
                'featured_point0' => array(),
                'correct_spelling0' => array(),
                'featured_point0_total' => 0,
                'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                'correct_spelling1' => array(),
                'featured_point1_total' => 0,
                'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                'correct_spelling2' => array(),
                'featured_point2_total' => 0,
                'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                'correct_spelling3' => array(),
                'featured_point3_total' => 0,
                'correct_spelling3_total' => 0,
            );            
        }
        
        if($teacher->level == 3) {
            $results = array(
                'wo3_blends_total' => 0,
                'wo3_vowels_total' => 0,
                'wo3_complex_total' => 0,
                'wo3_inflected_total' => 0,
                'wo3_unnacented_total' => 0,
                'wo3_affixes_total' => 0,
                'wo3_reduced_total' => 0,
                'wo3_greek_total' => 0,
                'wo3_assimilated_total' => 0,
                'word_ok' => 0,
                'featured_point0' => array(),
                'correct_spelling0' => array(),
                'featured_point0_total' => 0,
                'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                'correct_spelling1' => array(),
                'featured_point1_total' => 0,
                'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                'correct_spelling2' => array(),
                'featured_point2_total' => 0,
                'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                'correct_spelling3' => array(),
                'featured_point3_total' => 0,
                'correct_spelling3_total' => 0,
            );            
        }
        
        foreach($words as $key => $item) {
            $answer = $this->Test_model->get_student_test_answer($student_test->id, $item['id']);

            foreach($answer as $kans => $ans) {
                if(strpos($kans, 'wo' . $teacher->level . '_') !== FALSE) {
                    $results[$kans . '_total'] += $ans;
                    $total += $ans;
                }
                if($kans == 'word_ok') {
                    $results['word_ok'] += $ans;
                }
            }
            for($index = 0; $index < 4; $index++) {
                if ($index >= count($all_student_test)) {
                    $results['featured_point'.$index][$key] = 0;
                    $results['correct_spelling'.$index][$key] = 0;
                } else {
                    $answer1 = $this->Test_model->get_student_test_answer($all_student_test[$index]->id, $item['id']);

                    $results['featured_point'.$index][$key] = 0;
                    $results['correct_spelling'.$index][$key] = 0;

                    foreach($answer1 as $kans1 => $ans1) {
                        if(strpos($kans1, 'wo' . $teacher->level . '_') !== FALSE) {
                            $results['featured_point'. $index . '_total'] += $ans1;
                            $results['featured_point'.$index][$key] += $ans1;
                        }
                        if($kans1 == 'word_ok') {
                            $results['correct_spelling'.$index.'_total'] += $ans1;
                            $results['correct_spelling'.$index][$key] += $ans1;
                        }
                    }
                }
            }
        }
    
        $data = array(
            'return' => $return,
            'total' => $total,
            'results' => $results,
            'test' => $test,
            'student' => $student,
            'words' => $words,
            'teacher' => $teacher,
            'student_test' => $student_test
        );
        
        if($return == 1) {
            $html = $this->load->view('admin_teacher/view_report_l' . $teacher->level . '_all_item', $data, true);
            return $html;
        }
        
        if($print == 1) {
            $html = $this->load->view('admin_teacher/view_report_l' . $teacher->level, $data, true);
            //error_reporting(E_ALL);
            include_once APPPATH.'third_party/mpdf-development/vendor/autoload.php';
            //if(file_exists(APPPATH.'third_party/mpdf/mpdf.php')) {echo 'EXISTE';}
            //echo APPPATH.'third_party/mpdf/mpdf.php';die;
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $mpdf->Output();
            //echo $html;
            //$this->m_pdf->pdf->WriteHTML($html);
            //$this->m_pdf->pdf->Output();
            
            
            /*
            $html = $this->load->view('admin_teacher/view_report_l' . $teacher->level, $data, true);
            echo $html;
            $pdfFilePath = "output_pdf_name.pdf";
            $this->m_pdf->pdf->WriteHTML($html);
            $this->m_pdf->pdf->Output();*/ 
        } else {
            $this->load->view('admin_teacher/view_report_l' . $teacher->level, $data, false);
        }
    }

    public function print_answer($student_test_id, $test_id) {
        $this->view_answer($student_test_id, $test_id, 1);
    }

    public function print_all_answers($teacherID, $year) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $tid = $this->session->userdata('id');
        
        if(!$year) {
            $year = date("Y");
        }
        
        $tests = $this->Test_model->get_tests_by_year($teacherID, $year);
        $teacher = $this->Authentication_model->get_user_by_id($teacherID);
        //print_r($tests);
        $answers = array();
        
        foreach($tests as $key => $item) {
            $students_tests = $this->Test_model->get_students_by_test($item->id);
            //print_r($students_tests);
            foreach($students_tests as $kst => $stu) {
                //print_r($stu);
                $curHTML = $this->view_answer($stu->id, $stu->test_id, 0, 1);
                
                $answers[] = $curHTML;
            }
        }
        
        $html = implode('', $answers);
        
        $layoutHTML = $this->load->view('admin_teacher/view_answer_l' . $teacher->level . '_all', array('content' => $html), true);

        include_once APPPATH.'third_party/mpdf-development/vendor/autoload.php';
        //if(file_exists(APPPATH.'third_party/mpdf/mpdf.php')) {echo 'EXISTE';}
        //echo APPPATH.'third_party/mpdf/mpdf.php';die;

        ini_set("pcre.backtrack_limit", "5000000");
        
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($layoutHTML);
        $mpdf->Output();
    }

    public function view_all_answers($teacherID, $year) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $tid = $this->session->userdata('id');
        
        if(!$year) {
            $year = date("Y");
        }

        $tests = $this->Test_model->get_tests_by_year($teacherID, $year);
        $teacher = $this->Authentication_model->get_user_by_id($teacherID);

        $answers = array();
        
        foreach($tests as $key => $item) {
            $students_tests = $this->Test_model->get_students_by_test($item->id);
            //print_r($students_tests);
            foreach($students_tests as $kst => $stu) {
                //print_r($stu);
                $curHTML = $this->view_answer($stu->id, $stu->test_id, 0, 1);
                
                $answers[] = $curHTML;
            }
        }
        
        $html = implode('', $answers);
        
        $this->load->view('admin_teacher/view_answer_l' . $teacher->level . '_all', array('content' => $html), false);
    }

    public function view_answer($student_test_id, $test_id, $print = 0, $return = 0) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $this->load->model('Words_model');
        
        $tid = $this->session->userdata('id');
        $teacher = $this->Authentication_model->get_user_by_id($tid);
        
        $student_test = $this->Test_model->get_student_test_by_id($student_test_id);
        
        $student = $this->Authentication_model->get_user_by_id($student_test->student_id);

        $year = date("Y");
        $all_student_test = $this->Test_model->get_all_student_test_by_year($year, $student_test->student_id);


        //print_r($student_test);
        $test = $this->Test_model->get($test_id);
        $words = $this->Words_model->get_words_by_level_array($teacher->level);
        
        $total = 0;

        if($teacher->level == 1) {
            $results = array(
                // 'wo1_beginning_total' => 0,
                // 'wo1_final_total' => 0,
                // 'wo1_short_total' => 0,
                // 'wo1_consonant_total' => 0,
                // 'wo1_conblends_total' => 0,
                // 'wo1_longvow_total' => 0,
                // 'wo1_othervow_total' => 0,
                // 'wo1_inflected_total' => 0,
                // 'word_ok' => 0,
                'featured_point0' => array(),
                // 'correct_spelling0' => array(),
                // 'featured_point0_total' => 0,
                // 'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                // 'correct_spelling1' => array(),
                // 'featured_point1_total' => 0,
                // 'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                // 'correct_spelling2' => array(),
                // 'featured_point2_total' => 0,
                // 'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                // 'correct_spelling3' => array(),
                // 'featured_point3_total' => 0,
                // 'correct_spelling3_total' => 0,
            );            
        }
                
        if($teacher->level == 2) {
            $results = array(
                // 'wo2_beginning_total' => 0,
                // 'wo2_final_total' => 0,
                // 'wo2_short_total' => 0,
                // 'wo2_consonant_total' => 0,
                // 'wo2_conblends_total' => 0,
                // 'wo2_longvow_total' => 0,
                // 'wo2_othervow_total' => 0,
                // 'wo2_inflected_total' => 0,
                // 'wo2_syllable_total' => 0,
                // 'wo2_unacented_total' => 0,
                // 'wo2_harder_total' => 0,
                // 'wo2_base_total' => 0,
                // 'word_ok' => 0,
                'featured_point0' => array(),
                // 'correct_spelling0' => array(),
                // 'featured_point0_total' => 0,
                // 'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                // 'correct_spelling1' => array(),
                // 'featured_point1_total' => 0,
                // 'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                // 'correct_spelling2' => array(),
                // 'featured_point2_total' => 0,
                // 'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                // 'correct_spelling3' => array(),
                // 'featured_point3_total' => 0,
                // 'correct_spelling3_total' => 0,
            );            
        }
        
        if($teacher->level == 3) {
            $results = array(
                // 'wo3_blends_total' => 0,
                // 'wo3_vowels_total' => 0,
                // 'wo3_complex_total' => 0,
                // 'wo3_inflected_total' => 0,
                // 'wo3_unnacented_total' => 0,
                // 'wo3_affixes_total' => 0,
                // 'wo3_reduced_total' => 0,
                // 'wo3_greek_total' => 0,
                // 'wo3_assimilated_total' => 0,
                // 'word_ok' => 0,
                'featured_point0' => array(),
                // 'correct_spelling0' => array(),
                // 'featured_point0_total' => 0,
                // 'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                // 'correct_spelling1' => array(),
                // 'featured_point1_total' => 0,
                // 'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                // 'correct_spelling2' => array(),
                // 'featured_point2_total' => 0,
                // 'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                // 'correct_spelling3' => array(),
                // 'featured_point3_total' => 0,
                // 'correct_spelling3_total' => 0,
            );            
        }

        foreach($words as $key => $item) {
            $answer = $this->Test_model->get_student_test_answer($student_test->id, $item['id']);
            // foreach($answer as $kans => $ans) {
            //     if(strpos($kans, 'wo' . $teacher->level . '_') !== FALSE) {
            //         $results[$kans . '_total'] += $ans;
            //         $total += $ans;
            //     }
            //     if($kans == 'word_ok') {
            //         $results['word_ok'] += $ans;
            //     }
            // }
            for($index = 0; $index < 4; $index++) {
                if ($index >= count($all_student_test)) {
                    $results['featured_point'.$index][$key] = ' ';
                    // $results['correct_spelling'.$index][$key] = ' ';
                } else {
                    $answer1 = $this->Test_model->get_student_test_answer($all_student_test[$index]->id, $item['id']);
                    $results['featured_point'.$index][$key] = $answer1['answer'];
                    // $results['correct_spelling'.$index][$key] = 0;

                    // foreach($answer1 as $kans1 => $ans1) {
                    //     if(strpos($kans1, 'wo' . $teacher->level . '_') !== FALSE) {
                    //         $results['featured_point'. $index . '_total'] += $ans1;
                    //         $results['featured_point'.$index][$key] += $ans1;
                    //     }
                    //     if($kans1 == 'word_ok') {
                    //         $results['correct_spelling'.$index.'_total'] += $ans1;
                    //         $results['correct_spelling'.$index][$key] += $ans1;
                    //     }
                    // }
                }
            }
        }
    
        $data = array(
            'return' => $return,
            'total' => $total,
            'results' => $results,
            'test' => $test,
            'student' => $student,
            'words' => $words,
            'teacher' => $teacher,
            'student_test' => $student_test
        );
        
        if($return == 1) {
            $html = $this->load->view('admin_teacher/view_answer_l' . $teacher->level . '_all_item', $data, true);
            return $html;
        }
        
        if($print == 1) {
            $html = $this->load->view('admin_teacher/view_answer_l' . $teacher->level, $data, true);
            //error_reporting(E_ALL);
            include_once APPPATH.'third_party/mpdf-development/vendor/autoload.php';
            //if(file_exists(APPPATH.'third_party/mpdf/mpdf.php')) {echo 'EXISTE';}
            //echo APPPATH.'third_party/mpdf/mpdf.php';die;
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $mpdf->Output();
            //echo $html;
            //$this->m_pdf->pdf->WriteHTML($html);
            //$this->m_pdf->pdf->Output();
            
            
            /*
            $html = $this->load->view('admin_teacher/view_report_l' . $teacher->level, $data, true);
            echo $html;
            $pdfFilePath = "output_pdf_name.pdf";
            $this->m_pdf->pdf->WriteHTML($html);
            $this->m_pdf->pdf->Output();*/ 
        } else {
            $this->load->view('admin_teacher/view_answer_l' . $teacher->level, $data, false);
        }
    }
    
	public function index()
	{
        $this->load->model('Authentication_model');
        $this->load->model('Test_model');
        $workspace_id = $this->session->userdata('workspace_id');
        $count_teachers = $this->Authentication_model->get_count_by_type('teacher', $workspace_id);
        $count_students = $this->Authentication_model->get_count_by_type('student', $workspace_id);
        $count_reports = $this->Test_model->get_count_reports_all($workspace_id)[0]->cantidad;
        
        $stats_bar = $this->Test_model->get_promedios_all('ASC', $workspace_id);
        $stats_bar = array_slice($stats_bar, 0, 10);
           
	    $this->load_layout('admin_dashboard/index', array('stats_bar' => $stats_bar, 'cteachers' => $count_teachers, 'cstudents' => $count_students, 'creports' => $count_reports));
	}   
    
}