<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher_dashboard extends CI_Controller {

    var $usuario = null;

    function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        if(!$this->session->has_userdata('id') || $this->session->userdata('type') != 'teacher') {
            redirect('/admin/login');
        }
    }

    public function load_layout($view, $params = null)
    {
	    $uid = $this->session->userdata('id');
        $workspace_id = $this->session->userdata('workspace_id');
        $this->load->model('Authentication_model');
        $this->load->model('Plan_model');
        $params['subscription'] = $this->Plan_model->get_suscription($workspace_id);
        $params['trial'] = $this->Plan_model->get_trial($workspace_id);

        $user = $this->Authentication_model->get_user_by_id($uid);
        $this->usuario = $user;
        // Paso por parámetro la vista $view al layout y la muestro
        $params['user'] = $user;
        $params['sidebar'] = $this->load->view('admin_teacher/sidebar', array(), true);
        $data = array();
        $data['content'] = $this->load->view($view, $params, true);
        
        $this->load->view('admin_teacher/layout', $data, false);
    }
    
    public function ajax_new_roster() {
        $this->load->model('Roster_model');
        $name = $this->input->post('name');
        $grade = $this->input->post('grade');
        $uid = $this->session->userdata('id');
        $workspace_id = $this->session->userdata('workspace_id');
        $this->Roster_model->add($name, $uid, $grade, $workspace_id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! Rosted added successfully.'));die; 
    }
    
    public function create_roster() {
        $this->load_layout('admin_teacher/create_roster');
    }
    
    public function ajax_remove_audio() {
        $this->load->model('Words_model');
        $audio_id = $this->input->post('id');
        $this->Words_model->remove_audio($audio_id);
        echo json_encode(array('error' => ''));die;
    }
    
    public function ajax_set_audio() {
        $word_id = $this->input->post('word_id');
        $filename = $this->input->post('filename');
        $id = $this->session->userdata('id');
        $this->load->model('Words_model');
        $this->Words_model->add_audio_teacher($filename, $id, $word_id);
        echo json_encode(array('error' => ''));die;
    }
    
    public function ajax_upload_audio() {
       if(!empty($_FILES['file']['name'])){
    
         $word_id = $this->input->post('word_id');   
         // Set preference
         $filename = time() . '_' . $word_id . '_audio.mp3';
         
         $config['upload_path'] = dirname(dirname(dirname(__FILE__))) . "/uploads/audios/";
         $config['allowed_types'] = '*';
         $config['max_size'] = '20480'; // max_size in kb
         $config['file_name'] = $filename;
    
         //Load upload library
         $this->load->library('upload',$config); 
    
         // File upload
         if($this->upload->do_upload('file')){
           // Get data about the file
           $uploadData = $this->upload->data();
           echo $filename;
         } else {
            echo $this->upload->display_errors();
         }
       }        
    }
    
    public function import_roster() {
        $this->load->model('Roster_model');
        $this->load->model('Student_model');
        
        $rows = array();
        $sel_roster = null;
        
        if($this->input->post('rid')) {
            $mi_imagen = time() . '_roster.xlsx';
            $config['upload_path'] = dirname(dirname(dirname(__FILE__))) . "/uploads/rosters/";
            $config['file_name'] = $mi_imagen;
            $config['allowed_types'] = "xlsx";
            $config['max_size'] = "10240000";
        
            $this->load->library('upload', $config);
        
            if (!$this->upload->do_upload()) {
                //*** ocurrio un error
                $data['uploadError'] = $this->upload->display_errors();
                echo $this->upload->display_errors();
                return;
            } 
            $filename = dirname(dirname(dirname(__FILE__))) . '/uploads/rosters/' . $mi_imagen;
            $rows = $this->Roster_model->read_excel_roaster($filename); 
            $sel_roster_id = $this->input->post('rid'); 
            $sel_roster = $this->Roster_model->get($sel_roster_id);     
        }
        
        if($this->input->post('roster_id')) {

            $roster_id = $this->input->post('roster_id');
            $name = $this->input->post('name');
            $this->Roster_model->update(array('name' => $name), $roster_id);
        }
        
        if($this->input->post('roster_delete_id')) {
            //echo $roster_delete_id;
            $this->Roster_model->reset_roster($ridlist);
            $roster_delete_id = $this->input->post('roster_delete_id');
            $this->Roster_model->delete($roster_delete_id);
        }
        /**/
        
        $tid = $this->session->userdata('id');
        $rosters = $this->Roster_model->get_rosters_by_teacher($tid);
        foreach($rosters as $key => $item) {
            $rosters[$key]->students = $this->Student_model->get_students_by_roster($item->id);
        }
        
        $this->load_layout('admin_teacher/rosters', array('rosters' => $rosters, 'rows' => $rows, 'sel_roster' => $sel_roster));
        
    }
    
    public function ajax_save_list() {
        $ridlist = $this->input->post('ridlist');
        $students = $this->input->post('students');
        
        $this->load->model('Roster_model');
        
        $this->Roster_model->reset_roster($ridlist);
        
        $roster = $this->Roster_model->get($ridlist);
        
        foreach($students as $key => $item) {
            $this->Roster_model->add_student_roster($item, $ridlist, $roster->grade);
        }
        
        echo json_encode(array('error' => ''));die;
    }
    
    public function ajax_edit_teacher() {
        $this->load->model('Teacher_model');
        $this->load->model('Authentication_model');
        
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $lname = $this->input->post('lname');
        $recorded = $this->input->post('recorded');
        $hash = sha1($email . $password);
        $hashed_password = sha1(base_url() . $password);
        $data = array(
            'email' => $email,
            'name' => $name,
            'hash' => $hash,
            'recorded' => $recorded,
            'lname' => $lname,
            'username' => $username
        ); 
        
        if(!empty($password)) {
            $data['password'] = $hashed_password;
        }

        $uid = $this->session->userdata('id');
        $useradm = $this->Authentication_model->get_user_by_id($uid);
                
        $user = $this->Authentication_model->get_user_by_email_and_wid($email, $useradm->workspace_id);
        
        if($user && $user->id != $id) {
            echo json_encode(array('error' => 'The Email is already in use.'));die;
        }
        
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username_and_wid($username, $useradm->workspace_id);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }
        
        
        $this->Teacher_model->update_teacher($data, $id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The settings has been updated.'));die;        
    }
    
    public function ajax_edit_student() {
        $this->load->model('Student_model');
        $this->load->model('Authentication_model');
        
        $id = $this->input->post('id');
        // $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $lname = $this->input->post('lname');
        $username = $this->input->post('username');
        $roster_id = $this->input->post('roster_id');
        $hash = sha1(base_url() . $password);
        $hashed_password = sha1(base_url() . $password);
        $data = array(
            // 'email' => $email,
            'name' => $name,
            'hash' => $hash,
            'roster_id' => $roster_id,
            'lname' => $lname,
            'username' => $username
        ); 
        
        if(!empty($password)) {
            $data['password'] = $hashed_password;
        }
        
        $uid = $this->session->userdata('id');
        $useradm = $this->Authentication_model->get_user_by_id($uid);
        
        // $user = $this->Authentication_model->get_user_by_email_and_wid($email, $useradm->workspace_id);
        
        // if($user && $user->id != $id) {
        //     echo json_encode(array('error' => 'The Email is already in use.'));die;
        // }  
        
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username_and_wid($username, $useradm->workspace_id);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }           
        
        $this->Student_model->update_student($data, $id);
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The student has been updated.'));die;
    }
    
    public function ajax_new_student() {
        $this->load->model('Student_model');
        $this->load->model('Authentication_model');
        
        // $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $lname = $this->input->post('lname');
        $username = $this->input->post('username');
        $roster_id = $this->input->post('roster_id');
        $hash = sha1(base_url() . $password);
        $hashed_password = sha1(base_url() . $password);
        $workspace_id = $this->session->userdata('workspace_id');
        $data = array(
            // 'email' => $email,
            'password' => $password,
            'name' => $name,
            'type' => 'student',
            'hash' => $hash,
            'status' => 0,
            'roster_id' => $roster_id,
            'lname' => $lname,
            'username' => $username,
            'workspace_id' => $workspace_id
        );
        
        
        $uid = $this->session->userdata('id');
        $useradm = $this->Authentication_model->get_user_by_id($uid);
        
        // $existe = $this->Authentication_model->get_user_by_email_and_wid($email, $useradm->workspace_id);
        // if($existe) {
        //     echo json_encode(array('error' => 'The Email is already in use.'));die;
        // } 
        
        if(!empty($username)) {
            $user = $this->Authentication_model->get_user_by_username_and_wid($username, $useradm->workspace_id);
            if($user && $user->id != $id) {
                echo json_encode(array('error' => 'uname_used'));die;
            }
        }                
        
        $Student_id = $this->Student_model->add_student($data, true);

        /*$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'vincentkonica@gmail.com',
            'smtp_pass' => '1911eagles',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
                
        $this->load->library('email', $config);
        $this->email->from('info@jgalarza.com', 'Vocabulary');
        $this->email->to($email);
         
        $this->email->subject('Invitation to join Vocabulary');
        
        $hash_url = base_url() . 'admin/accept_invitation/' . $hash . '/' . $hashed_password;
        
        $message = '<a href="' . $hash_url . '">Join Vocabulary</a>';
        
        $this->email->message($message);*/
        
        echo json_encode(array('error' => '', 'msg' => 'Done! The student has been created.'));die; 
    }
    
    public function new_student() {
        $this->load->model('Roster_model');
        $workspace_id = $this->session->userdata('workspace_id');
        $rosters = $this->Roster_model->get_rosters($workspace_id);
        $this->load_layout('admin_teacher/new_student', array('rosters' => $rosters));
    }
    
    public function edit_student($sid) {
        $this->load->model('Roster_model');
        $this->load->model('Student_model');
        $student = $this->Student_model->get_student_by_id($sid);
        $workspace_id = $this->session->userdata('workspace_id');
        $rosters = $this->Roster_model->get_rosters($workspace_id);
        $this->load_layout('admin_teacher/edit_student', array('student' => $student, 'rosters' => $rosters));
    }
    
    public function ajax_remove_student() {
        $this->load->model('Student_model');
        $id = $this->input->post('id');
        $this->Student_model->delete_student($id);
        echo json_encode(array('error' => ''));
    }
    
    public function teacher_settings() {
        $this->load->model('Teacher_model');
        
        $id = $this->session->userdata('id');
        $students = $this->Teacher_model->get_teacher_by_id($id);
        
        $this->load_layout('admin_teacher/settings', array('user' => $user));        
    }
    
    public function ajax_start_test() {
        $this->load->model('Test_model');
        $this->load->model('Roster_model');
        $roster_id = $this->input->post('roster_id');
        $practice = $this->input->post('practice');
        $id = $this->session->userdata('id');
        $roster = $this->Roster_model->get($roster_id);
        $workspace_id = $this->session->userdata('workspace_id');
        $this->Test_model->start_test($roster_id, $id, $roster->grade, $workspace_id, $practice);
        
        echo json_encode(array('error' => ''));
    }
    
    public function ajax_bann_student() {
        $this->load->model('Test_model');
        $student_id = $this->input->post('student_id');
        $tid = $this->session->userdata('id'); 
        $test = $this->Test_model->get_active_test($tid);
        $this->Test_model->bann_student($test->id, $student_id);
        echo json_encode(array());die;       
    }
    
    public function ajax_unbann_student() {
        $this->load->model('Test_model');
        $student_id = $this->input->post('student_id');
        $tid = $this->session->userdata('id'); 
        $test = $this->Test_model->get_active_test($tid);
        $this->Test_model->unbann_student($test->id, $student_id);
        echo json_encode(array());die;       
    }
    
    public function ajax_stop_test() {
        
        $this->load->model('Test_model');
        $test_id = $this->input->post('test_id');
        $this->Test_model->update(array('status' => 2), $test_id);
        echo json_encode(array('error' => ''));
    }
    
    public function ajax_get_student_status() {
        $this->load->model('Test_model');
        $student_id = $this->input->post('student_id');
        $tid = $this->session->userdata('id');
        
        $test = $this->Test_model->get_active_test($tid);
        
        $student_test = $this->Test_model->get_student_test($test->id, $student_id);
        
        if(!$student_test) {
            echo json_encode(array('absent' => 1));die;
        }
        
        $result = array('status' => $student_test->status, 'banned' => $student_test->banned, 'fulloff' => $student_test->not_fullscreen);
        
        echo json_encode(array('absent' => 0, 'result' => $result));
    }
    
    public function start_test() {
        $this->load->model('Test_model');
        $this->load->model('Roster_model');
        $this->load->model('Teacher_model');
        
        $tid = $this->session->userdata('id');
        
        $teacher = $this->Teacher_model->get_teacher_by_id($tid);
        $roster = $this->Roster_model->get_by_teacher_id($tid);

        $test = $this->Test_model->get_by_roster($roster->id, 1);
        
        /*$rosters = $this->Roster_model->get_rosters();
        foreach($rosters as $key => $item) {
            $test = $this->Test_model->get_by_roster($item->id, 1);
            $rosters[$key]->testing = (!empty($test));
            $rosters[$key]->test = $test;
        }*/
        $this->load_layout('admin_teacher/start_test', array('tid' => $test->id, 'roster' => $roster, 'teacher' => $teacher, 'test' => $test));        
    }
    
    public function students() {
        $this->load->model('Student_model');
        $this->load->model('Roster_model');
        
        $tid = $this->session->userdata('id');
        $roster = $this->Roster_model->get_by_teacher_id($tid);
        $students = array();
        if($roster) {
            $students = $this->Student_model->get_students_by_roster($roster->id);
        }
        
        $this->load_layout('admin_teacher/students', array('students' => $students));
    }
    
    public function recordings() {
        $this->load->model('Words_model');
        
        $id = $this->session->userdata('id');
        $workspace_id = $this->session->userdata('workspace_id');
        $words = $this->Words_model->get_all($id, $workspace_id);
        
        $this->load_layout('admin_teacher/recordings', array('words' => $words));
    }
    
    public function students_loggedin() {
        $this->load->model('Test_model');
        $this->load->model('Roster_model');
        
        $id = $this->session->userdata('id');
        $test = $this->Test_model->get_active_test($id);
        
        $roster = $this->Roster_model->get_by_teacher_id($id);
        
        $students = $this->Roster_model->get_students_by_roster_id($roster->id);
        
        $this->load_layout('admin_teacher/students_loggedin', array('students' => $students));
    }
    
	public function reports()
	{
	    $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        
        $teache_id = $this->session->userdata('id');
        $year = date("Y");
        $tests = $this->Test_model->get_by_year($year, $teache_id);
        if(!$tests) {
            $archives = $this->Test_model->get_archives($teache_id);
            $year = $archives[0]->anio;
            $tests = $this->Test_model->get_by_year($year, $teache_id);
        }
        
        foreach($tests as $key => $item) {
            $tests[$key]->students = $this->Test_model->get_students_by_test($item->id);
            foreach($tests[$key]->students as $kst => $stud) {
                $tests[$key]->students[$kst]->student = $this->Authentication_model->get_user_by_id($stud->student_id);
            }
        }
	    $this->load_layout('admin_teacher/reports', array('tests' => $tests, 'year' => $year));
	}
    
    public function archive() {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        
        $year = $this->input->post('year');
        if(empty($year)) {
            $year = date("Y");
        }
        
        $tid = $this->session->userdata('id');
        
        $tests = array();
        
        if(!empty($year)) {
            $tests = $this->Test_model->get_by_year($year, $tid);
            foreach($tests as $key => $item) {
                $tests[$key]->students = $this->Test_model->get_students_by_test($item->id);
                foreach($tests[$key]->students as $kst => $stud) {
                    $tests[$key]->students[$kst]->student = $this->Authentication_model->get_user_by_id($stud->student_id);
                }
            }            
        }
        
        $archives = $this->Test_model->get_archives($tid);
        $this->load_layout('admin_teacher/archive', array('archives' => $archives, 'year' => $year, 'tests' => $tests));
    }
    
    public function print_report($student_test_id, $test_id) {
        $this->view_report($student_test_id, $test_id, 1);
    }
    
    public function print_all_reports($year) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $tid = $this->session->userdata('id');
        
        if(!$year) {
            $year = date("Y");
        }
        
        $tests = $this->Test_model->get_tests_by_year($tid, $year);
        $teacher = $this->Authentication_model->get_user_by_id($tid);
        // print_r($tests);
        $reports = array();
        
        foreach($tests as $key => $item) {
            $students_tests = $this->Test_model->get_students_by_test($item->id);
            // print_r($students_tests);
            foreach($students_tests as $kst => $stu) {
                // print_r($stu);
                $curHTML = $this->view_report($stu->id, $stu->test_id, 0, 1);
                // print_r($curHTML);
                $reports[] = $curHTML;
            }
        }
        
        $html = implode('', $reports);
        // print_r($html);
        
        $layoutHTML = $this->load->view('admin_teacher/view_report_l' . $teacher->level . '_all', array('content' => $html), true);

        include_once APPPATH.'third_party/mpdf-development/vendor/autoload.php';
        //if(file_exists(APPPATH.'third_party/mpdf/mpdf.php')) {echo 'EXISTE';}
        //echo APPPATH.'third_party/mpdf/mpdf.php';die;
        
        ini_set("pcre.backtrack_limit", "5000000");
        
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($layoutHTML);
        $mpdf->Output();
    }
    
    public function view_all_reports($year) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $tid = $this->session->userdata('id');
        
        if(!$year) {
            $year = date("Y");
        }

        $tests = $this->Test_model->get_tests_by_year($tid, $year);
        $teacher = $this->Authentication_model->get_user_by_id($tid);

        $reports = array();
        
        foreach($tests as $key => $item) {
            $students_tests = $this->Test_model->get_students_by_test($item->id);
            //print_r($students_tests);
            foreach($students_tests as $kst => $stu) {
                //print_r($stu);
                $curHTML = $this->view_report($stu->id, $stu->test_id, 0, 1);
                
                $reports[] = $curHTML;
            }
        }
        
        $html = implode('', $reports);
        
        $this->load->view('admin_teacher/view_report_l' . $teacher->level . '_all', array('content' => $html), false);
    }
    
    public function view_report($student_test_id, $test_id, $print = 0, $return = 0) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $this->load->model('Words_model');
        
        $tid = $this->session->userdata('id');
        $teacher = $this->Authentication_model->get_user_by_id($tid);
        
        $student_test = $this->Test_model->get_student_test_by_id($student_test_id);
        
        $student = $this->Authentication_model->get_user_by_id($student_test->student_id);

        $year = date("Y");
        $all_student_test = $this->Test_model->get_all_student_test_by_year($year, $student_test->student_id);


        //print_r($student_test);
        $test = $this->Test_model->get($test_id);
        $words = $this->Words_model->get_words_by_level_array($teacher->level);
        
        $total = 0;

        if($teacher->level == 1) {
            $results = array(
                'wo1_beginning_total' => 0,
                'wo1_final_total' => 0,
                'wo1_short_total' => 0,
                'wo1_consonant_total' => 0,
                'wo1_conblends_total' => 0,
                'wo1_longvow_total' => 0,
                'wo1_othervow_total' => 0,
                'wo1_inflected_total' => 0,
                'word_ok' => 0,
                'featured_point0' => array(),
                'correct_spelling0' => array(),
                'featured_point0_total' => 0,
                'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                'correct_spelling1' => array(),
                'featured_point1_total' => 0,
                'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                'correct_spelling2' => array(),
                'featured_point2_total' => 0,
                'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                'correct_spelling3' => array(),
                'featured_point3_total' => 0,
                'correct_spelling3_total' => 0,
            );            
        }
                
        if($teacher->level == 2) {
            $results = array(
                'wo2_beginning_total' => 0,
                'wo2_final_total' => 0,
                'wo2_short_total' => 0,
                'wo2_consonant_total' => 0,
                'wo2_conblends_total' => 0,
                'wo2_longvow_total' => 0,
                'wo2_othervow_total' => 0,
                'wo2_inflected_total' => 0,
                'wo2_syllable_total' => 0,
                'wo2_unacented_total' => 0,
                'wo2_harder_total' => 0,
                'wo2_base_total' => 0,
                'word_ok' => 0,
                'featured_point0' => array(),
                'correct_spelling0' => array(),
                'featured_point0_total' => 0,
                'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                'correct_spelling1' => array(),
                'featured_point1_total' => 0,
                'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                'correct_spelling2' => array(),
                'featured_point2_total' => 0,
                'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                'correct_spelling3' => array(),
                'featured_point3_total' => 0,
                'correct_spelling3_total' => 0,
            );            
        }
        
        if($teacher->level == 3) {
            $results = array(
                'wo3_blends_total' => 0,
                'wo3_vowels_total' => 0,
                'wo3_complex_total' => 0,
                'wo3_inflected_total' => 0,
                'wo3_unnacented_total' => 0,
                'wo3_affixes_total' => 0,
                'wo3_reduced_total' => 0,
                'wo3_greek_total' => 0,
                'wo3_assimilated_total' => 0,
                'word_ok' => 0,
                'featured_point0' => array(),
                'correct_spelling0' => array(),
                'featured_point0_total' => 0,
                'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                'correct_spelling1' => array(),
                'featured_point1_total' => 0,
                'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                'correct_spelling2' => array(),
                'featured_point2_total' => 0,
                'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                'correct_spelling3' => array(),
                'featured_point3_total' => 0,
                'correct_spelling3_total' => 0,
            );            
        }

        foreach($words as $key => $item) {
            $answer = $this->Test_model->get_student_test_answer($student_test->id, $item['id']);
            foreach($answer as $kans => $ans) {
                if(strpos($kans, 'wo' . $teacher->level . '_') !== FALSE) {
                    $results[$kans . '_total'] += $ans;
                    $total += $ans;
                }
                if($kans == 'word_ok') {
                    $results['word_ok'] += $ans;
                }
            }
            for($index = 0; $index < 4; $index++) {
                if ($index >= count($all_student_test)) {
                    $results['featured_point'.$index][$key] = 0;
                    $results['correct_spelling'.$index][$key] = 0;
                } else {
                    $answer1 = $this->Test_model->get_student_test_answer($all_student_test[$index]->id, $item['id']);

                    $results['featured_point'.$index][$key] = 0;
                    $results['correct_spelling'.$index][$key] = 0;

                    foreach($answer1 as $kans1 => $ans1) {
                        if(strpos($kans1, 'wo' . $teacher->level . '_') !== FALSE) {
                            $results['featured_point'. $index . '_total'] += $ans1;
                            $results['featured_point'.$index][$key] += $ans1;
                        }
                        if($kans1 == 'word_ok') {
                            $results['correct_spelling'.$index.'_total'] += $ans1;
                            $results['correct_spelling'.$index][$key] += $ans1;
                        }
                    }
                }
            }
        }
    
        $data = array(
            'return' => $return,
            'total' => $total,
            'results' => $results,
            'test' => $test,
            'student' => $student,
            'words' => $words,
            'teacher' => $teacher,
            'student_test' => $student_test
        );
        
        if($return == 1) {
            $html = $this->load->view('admin_teacher/view_report_l' . $teacher->level . '_all_item', $data, true);
            return $html;
        }
        
        if($print == 1) {
            $html = $this->load->view('admin_teacher/view_report_l' . $teacher->level, $data, true);
            //error_reporting(E_ALL);
            include_once APPPATH.'third_party/mpdf-development/vendor/autoload.php';
            //if(file_exists(APPPATH.'third_party/mpdf/mpdf.php')) {echo 'EXISTE';}
            //echo APPPATH.'third_party/mpdf/mpdf.php';die;
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $mpdf->Output();
            //echo $html;
            //$this->m_pdf->pdf->WriteHTML($html);
            //$this->m_pdf->pdf->Output();
            
            
            /*
            $html = $this->load->view('admin_teacher/view_report_l' . $teacher->level, $data, true);
            echo $html;
            $pdfFilePath = "output_pdf_name.pdf";
            $this->m_pdf->pdf->WriteHTML($html);
            $this->m_pdf->pdf->Output();*/ 
        } else {
            $this->load->view('admin_teacher/view_report_l' . $teacher->level, $data, false);
        }
    }

    public function print_answer($student_test_id, $test_id) {
        $this->view_answer($student_test_id, $test_id, 1);
    }

    public function print_all_answers($year) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $tid = $this->session->userdata('id');
        
        if(!$year) {
            $year = date("Y");
        }
        
        $tests = $this->Test_model->get_tests_by_year($tid, $year);
        $teacher = $this->Authentication_model->get_user_by_id($tid);
        //print_r($tests);
        $answers = array();
        
        foreach($tests as $key => $item) {
            $students_tests = $this->Test_model->get_students_by_test($item->id);
            //print_r($students_tests);
            foreach($students_tests as $kst => $stu) {
                //print_r($stu);
                $curHTML = $this->view_answer($stu->id, $stu->test_id, 0, 1);
                
                $answers[] = $curHTML;
            }
        }
        
        $html = implode('', $answers);
        
        $layoutHTML = $this->load->view('admin_teacher/view_answer_l' . $teacher->level . '_all', array('content' => $html), true);

        include_once APPPATH.'third_party/mpdf-development/vendor/autoload.php';
        //if(file_exists(APPPATH.'third_party/mpdf/mpdf.php')) {echo 'EXISTE';}
        //echo APPPATH.'third_party/mpdf/mpdf.php';die;

        ini_set("pcre.backtrack_limit", "5000000");

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($layoutHTML);
        $mpdf->Output();
    }

    public function view_all_answers($year) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $tid = $this->session->userdata('id');
        
        if(!$year) {
            $year = date("Y");
        }

        $tests = $this->Test_model->get_tests_by_year($tid, $year);
        $teacher = $this->Authentication_model->get_user_by_id($tid);

        $answers = array();
        
        foreach($tests as $key => $item) {
            $students_tests = $this->Test_model->get_students_by_test($item->id);
            //print_r($students_tests);
            foreach($students_tests as $kst => $stu) {
                //print_r($stu);
                $curHTML = $this->view_answer($stu->id, $stu->test_id, 0, 1);
                
                $answers[] = $curHTML;
            }
        }
        
        $html = implode('', $answers);
        
        $this->load->view('admin_teacher/view_answer_l' . $teacher->level . '_all', array('content' => $html), false);
    }

    public function view_answer($student_test_id, $test_id, $print = 0, $return = 0) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $this->load->model('Words_model');
        
        $tid = $this->session->userdata('id');
        $teacher = $this->Authentication_model->get_user_by_id($tid);
        
        $student_test = $this->Test_model->get_student_test_by_id($student_test_id);
        
        $student = $this->Authentication_model->get_user_by_id($student_test->student_id);

        $year = date("Y");
        $all_student_test = $this->Test_model->get_all_student_test_by_year($year, $student_test->student_id);


        //print_r($student_test);
        $test = $this->Test_model->get($test_id);
        $words = $this->Words_model->get_words_by_level_array($teacher->level);
        
        $total = 0;

        if($teacher->level == 1) {
            $results = array(
                // 'wo1_beginning_total' => 0,
                // 'wo1_final_total' => 0,
                // 'wo1_short_total' => 0,
                // 'wo1_consonant_total' => 0,
                // 'wo1_conblends_total' => 0,
                // 'wo1_longvow_total' => 0,
                // 'wo1_othervow_total' => 0,
                // 'wo1_inflected_total' => 0,
                // 'word_ok' => 0,
                'featured_point0' => array(),
                // 'correct_spelling0' => array(),
                // 'featured_point0_total' => 0,
                // 'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                // 'correct_spelling1' => array(),
                // 'featured_point1_total' => 0,
                // 'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                // 'correct_spelling2' => array(),
                // 'featured_point2_total' => 0,
                // 'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                // 'correct_spelling3' => array(),
                // 'featured_point3_total' => 0,
                // 'correct_spelling3_total' => 0,
            );            
        }
                
        if($teacher->level == 2) {
            $results = array(
                // 'wo2_beginning_total' => 0,
                // 'wo2_final_total' => 0,
                // 'wo2_short_total' => 0,
                // 'wo2_consonant_total' => 0,
                // 'wo2_conblends_total' => 0,
                // 'wo2_longvow_total' => 0,
                // 'wo2_othervow_total' => 0,
                // 'wo2_inflected_total' => 0,
                // 'wo2_syllable_total' => 0,
                // 'wo2_unacented_total' => 0,
                // 'wo2_harder_total' => 0,
                // 'wo2_base_total' => 0,
                // 'word_ok' => 0,
                'featured_point0' => array(),
                // 'correct_spelling0' => array(),
                // 'featured_point0_total' => 0,
                // 'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                // 'correct_spelling1' => array(),
                // 'featured_point1_total' => 0,
                // 'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                // 'correct_spelling2' => array(),
                // 'featured_point2_total' => 0,
                // 'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                // 'correct_spelling3' => array(),
                // 'featured_point3_total' => 0,
                // 'correct_spelling3_total' => 0,
            );            
        }
        
        if($teacher->level == 3) {
            $results = array(
                // 'wo3_blends_total' => 0,
                // 'wo3_vowels_total' => 0,
                // 'wo3_complex_total' => 0,
                // 'wo3_inflected_total' => 0,
                // 'wo3_unnacented_total' => 0,
                // 'wo3_affixes_total' => 0,
                // 'wo3_reduced_total' => 0,
                // 'wo3_greek_total' => 0,
                // 'wo3_assimilated_total' => 0,
                // 'word_ok' => 0,
                'featured_point0' => array(),
                // 'correct_spelling0' => array(),
                // 'featured_point0_total' => 0,
                // 'correct_spelling0_total' => 0,
                'featured_point1' => array(),
                // 'correct_spelling1' => array(),
                // 'featured_point1_total' => 0,
                // 'correct_spelling1_total' => 0,
                'featured_point2' => array(),
                // 'correct_spelling2' => array(),
                // 'featured_point2_total' => 0,
                // 'correct_spelling2_total' => 0,
                'featured_point3' => array(),
                // 'correct_spelling3' => array(),
                // 'featured_point3_total' => 0,
                // 'correct_spelling3_total' => 0,
            );            
        }

        foreach($words as $key => $item) {
            $answer = $this->Test_model->get_student_test_answer($student_test->id, $item['id']);
            // foreach($answer as $kans => $ans) {
            //     if(strpos($kans, 'wo' . $teacher->level . '_') !== FALSE) {
            //         $results[$kans . '_total'] += $ans;
            //         $total += $ans;
            //     }
            //     if($kans == 'word_ok') {
            //         $results['word_ok'] += $ans;
            //     }
            // }
            for($index = 0; $index < 4; $index++) {
                if ($index >= count($all_student_test)) {
                    $results['featured_point'.$index][$key] = ' ';
                    // $results['correct_spelling'.$index][$key] = ' ';
                } else {
                    $answer1 = $this->Test_model->get_student_test_answer($all_student_test[$index]->id, $item['id']);
                    $results['featured_point'.$index][$key] = $answer1['answer'];
                    // $results['correct_spelling'.$index][$key] = 0;

                    // foreach($answer1 as $kans1 => $ans1) {
                    //     if(strpos($kans1, 'wo' . $teacher->level . '_') !== FALSE) {
                    //         $results['featured_point'. $index . '_total'] += $ans1;
                    //         $results['featured_point'.$index][$key] += $ans1;
                    //     }
                    //     if($kans1 == 'word_ok') {
                    //         $results['correct_spelling'.$index.'_total'] += $ans1;
                    //         $results['correct_spelling'.$index][$key] += $ans1;
                    //     }
                    // }
                }
            }
        }
    
        $data = array(
            'return' => $return,
            'total' => $total,
            'results' => $results,
            'test' => $test,
            'student' => $student,
            'words' => $words,
            'teacher' => $teacher,
            'student_test' => $student_test
        );
        
        if($return == 1) {
            $html = $this->load->view('admin_teacher/view_answer_l' . $teacher->level . '_all_item', $data, true);
            return $html;
        }
        
        if($print == 1) {
            $html = $this->load->view('admin_teacher/view_answer_l' . $teacher->level, $data, true);
            //error_reporting(E_ALL);
            include_once APPPATH.'third_party/mpdf-development/vendor/autoload.php';
            //if(file_exists(APPPATH.'third_party/mpdf/mpdf.php')) {echo 'EXISTE';}
            //echo APPPATH.'third_party/mpdf/mpdf.php';die;
            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $mpdf->Output();
            //echo $html;
            //$this->m_pdf->pdf->WriteHTML($html);
            //$this->m_pdf->pdf->Output();
            
            
            /*
            $html = $this->load->view('admin_teacher/view_report_l' . $teacher->level, $data, true);
            echo $html;
            $pdfFilePath = "output_pdf_name.pdf";
            $this->m_pdf->pdf->WriteHTML($html);
            $this->m_pdf->pdf->Output();*/ 
        } else {
            $this->load->view('admin_teacher/view_answer_l' . $teacher->level, $data, false);
        }
    }
    
    private function get_points_test($roster_id, $student_test_id, $level) {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $this->load->model('Words_model');

        $test = $this->Test_model->get_by_roster($roster_id, 2);
        
        $student_test = $this->Test_model->get_student_test_by_id($student_test_id);
        
        $words = $this->Words_model->get_words_by_level_array($level);
        
        $total = 0;

        if($level == 1) {
            $results = array(
                'wo1_beginning_total' => 0,
                'wo1_final_total' => 0,
                'wo1_short_total' => 0,
                'wo1_consonant_total' => 0,
                'wo1_conblends_total' => 0,
                'wo1_longvow_total' => 0,
                'wo1_othervow_total' => 0,
                'wo1_inflected_total' => 0,
                'word_ok' => 0,
            );            
        }
                
        if($level == 2) {
            $results = array(
                'wo2_beginning_total' => 0,
                'wo2_final_total' => 0,
                'wo2_short_total' => 0,
                'wo2_consonant_total' => 0,
                'wo2_conblends_total' => 0,
                'wo2_longvow_total' => 0,
                'wo2_othervow_total' => 0,
                'wo2_inflected_total' => 0,
                'wo2_syllable_total' => 0,
                'wo2_unacented_total' => 0,
                'wo2_harder_total' => 0,
                'wo2_base_total' => 0,
                'word_ok' => 0,
            );            
        }
        
        if($level == 3) {
            $results = array(
                'wo3_blends_total' => 0,
                'wo3_vowels_total' => 0,
                'wo3_complex_total' => 0,
                'wo3_inflected_total' => 0,
                'wo3_unnacented_total' => 0,
                'wo3_affixes_total' => 0,
                'wo3_reduced_total' => 0,
                'wo3_greek_total' => 0,
                'wo3_assimilated_total' => 0,
                'word_ok' => 0,
            );            
        }
        
        foreach($words as $key => $item) {
            $answer = $this->Test_model->get_student_test_answer($student_test->id, $item['id']);
            foreach($answer as $kans => $ans) {
                if(strpos($kans, 'wo' . $teacher->level . '_') !== FALSE) {
                    $results[$kans . '_total'] += $ans;
                    $total += $ans;
                }
                if($kans == 'word_ok') {
                    $results['word_ok'] += $ans;
                    $total += $ans;
                }
            }
        }
        
        return $total;
    }
    
    public function analysis() {
        $this->load->model('Test_model');
        $this->load->model('Authentication_model');
        $this->load->model('Words_model');
        $this->load->model('Roster_model');
        $this->load->helper('words_helper');
        
        $teacher_id = $this->session->userdata('id');
        $teacher = $this->Authentication_model->get_user_by_id($teacher_id);
        
        $words = $this->Words_model->get_words_by_level_array($teacher->level);
        
        $data = array();
        $stats_bar_DESC = $this->Test_model->get_promedios_by_teacher($teacher_id, 'DESC');
        $data['correctas'] = $stats_bar_DESC;//array_slice($stats_bar_DESC, 0, 15);  

        $stats_bar_ASC = $this->Test_model->get_promedios_by_teacher($teacher_id, 'ASC');
        $data['incorrectas'] = $stats_bar_ASC;//array_slice($stats_bar_ASC, 0, 15);     
        
        if($teacher->level == 1) {
            $maxCountPoints = 56;
            $maxPoints = 26;
            $results = array(
                'wo1_beginning' => 0,
                'wo1_final' => 0,
                'wo1_short' => 0,
                'wo1_consonant' => 0,
                'wo1_conblends' => 0,
                'wo1_longvow' => 0,
                'wo1_othervow' => 0,
                'wo1_inflected' => 0
            ); 
            
            $grades = array(
                '1' => 'TK',
                '2' => 'Kinder',
            );                       
        }
                
        if($teacher->level == 2) {
            $maxCountPoints = 62;
            $maxPoints = 25;
            $results = array(
                'wo2_beginning' => 0,
                'wo2_final' => 0,
                'wo2_short' => 0,
                'wo2_consonant' => 0,
                'wo2_conblends' => 0,
                'wo2_longvow' => 0,
                'wo2_othervow' => 0,
                'wo2_inflected' => 0,
                'wo2_syllable' => 0,
                'wo2_unacented' => 0,
                'wo2_harder' => 0,
                'wo2_base' => 0,
            ); 
            
            $grades = array(
                '3' => '1st Grade',
                '4' => '2nd Grade',
                '5' => '3rd Grade', 
            );                       
        }
        
        if($teacher->level == 3) {
            $maxCountPoints = 68;
            $maxPoints = 31;
            $results = array(
                'wo3_blends' => 0,
                'wo3_vowels' => 0,
                'wo3_complex' => 0,
                'wo3_inflected' => 0,
                'wo3_unnacented' => 0,
                'wo3_affixes' => 0,
                'wo3_reduced' => 0,
                'wo3_greek' => 0,
                'wo3_assimilated' => 0,
            );
            
            $grades = array(
                '6' => '4th Grade',
                '7' => '5th Grade',
                '8' => '6th Grade',
                '9' => '7th Grade',
                '10' => '8th Grade', 
            );                        
        }
        
        $total_tests = 0;//$this->Test_model->get_count_reports($teacher_id)[0]->cantidad;
        
        $array_cols_ok = $array_cols_no = array();
        
        foreach($results as $key => $item) {
            $newkey = get_name_field($key);
            if(!is_array($array_cols_ok[$key])) {
                $array_cols_ok[$key] = array('label' => $newkey, 'value' => 0);
                $array_cols_no[$key] = array('label' => $newkey, 'value' => 0);
            }
            foreach($words as $kw => $wo) {
                if(!empty($wo[$key])) {
                    $total_tests = ($this->Test_model->get_stats_by_field_no_value($key, $teacher_id, $wo['id'])[0]->valor);
                    $value_ok = ($this->Test_model->get_stats_by_field($key, $teacher_id, 1, $wo['id'])[0]->valor);
                    $value_no = ($this->Test_model->get_stats_by_field($key, $teacher_id, 0, $wo['id'])[0]->valor);
                    $array_cols_ok[$key]['value'] += $value_ok;   
                    $array_cols_no[$key]['value'] += $value_no;                 
                }                
            }
        }
        
        usort($array_cols_ok, 'sortByOrder');
        usort($array_cols_no, 'sortByOrder');
        
        $data['cols_ok'] = $array_cols_ok;
        $data['cols_no'] = $array_cols_no;
        
        $roster = $this->Roster_model->get_by_teacher_id($teacher_id);
        $archives = $this->Test_model->get_archives_by_roster($teacher_id, $roster->id);

        $year = $archives[0]->anio;
        $tests = $this->Test_model->get_tests_by_year_and_roster($teacher_id, $year, $roster->id);
        $array_tests_comp = array();
        
        $max_value = 0;
        
        foreach($tests as $key => $item) {
            $value = 0;
            $students_tests = $this->Test_model->get_students_by_test($item->id);
            //print_r($students_tests);
            if(count($students_tests) == 0) continue;
            //print_r($item);
            foreach($students_tests as $ks => $st) {

                $value += $this->get_points_test($roster->id, $st->id, $teacher->level);
            }

            $value = ceil($value / count($students_tests));
            if($max_value == 0 || $value > $max_value) {
                $max_value = $value;
            }
            $array_tests_comp[] = array('label' => date("m-d-Y", strtotime($item->created)), 'value' => $value);
        }
        
        foreach($array_tests_comp as $key => $item) {
            $array_tests_comp[$key]['value'] = ceil($item['value'] * 100 / $max_value);
        }
        
        $data['tests_comp'] = $array_tests_comp;
        usort($data['tests_comp'], 'sortByLabel');
        
        $lastTest = $this->Test_model->get_by_roster_last($roster->id, 2);

        $students_tests = $this->Test_model->get_students_by_test($lastTest->id);
        $array_average_students = array();
        $max_value = 0;
        foreach($students_tests as $key => $item) {
            $value = $this->get_points_test($roster->id, $st->id, $teacher->level);
            $student = $this->Authentication_model->get_user_by_id($item->student_id);
            $array_average_students[] = array('label' => $student->name . ' ' . $student->lname, 'value' => $value);
            if($max_value == 0 || $value > $max_value) {
                $max_value = $value;
            }
        }
        
        $array_average_students_corr = $array_average_students;
        $array_average_students_incorr = $array_average_students;
        
        foreach($array_average_students_corr as $key => $item) {
            $array_average_students_corr[$key]['value'] = number_format((($maxCountPoints + $maxPoints) - $item['value']) * 100 / ($maxCountPoints + $maxPoints), 1);
        }
        
        foreach($array_average_students_incorr as $key => $item) {
            $array_average_students_incorr[$key]['value'] = number_format($item['value'] * 100 / ($maxCountPoints + $maxPoints), 1);
        }
        
        $data['average_students'] = $array_average_students;
        $data['average_students_corr'] = $array_average_students_corr;
        $data['average_students_incorr'] = $array_average_students_incorr;
        //$this->get_points_test();
        //print_r($data);
        
        $workspace_id = $this->session->userdata('workspace_id');

        $results_by_grade_ok = $results_by_grade_no = array();
        foreach($grades as $key => $item) {
            $currentGrade = intval($key);
            switch($currentGrade) {
                case 1:
                case 2:
                    $maxWords = 26;
                break;
                case 3:
                case 4:
                case 5:
                    $maxWords = 25;
                break;
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    $maxWords = 31;
                break;
            }
            
            $lastTest = $this->Test_model->get_last_test_by_grade($currentGrade, $workspace_id);
            if($lastTest) {
                $percent = $this->Test_model->get_percent_test($lastTest->id, $maxWords);
                $results_by_grade_ok[] = array('label' => $item, 'value' => $percent['ok']);
                $results_by_grade_no[] = array('label' => $item, 'value' => $percent['no']);                
            }
        }
        
        $correctas_malas_clase = array();
        
        
        $lastTest = $this->Test_model->get_last_test_by_grade($roster->grade, $workspace_id);

        $students_tests = $this->Test_model->get_students_by_test($lastTest->id);

        foreach($students_tests as $key => $item) {
            $student = $this->Authentication_model->get_user_by_id($item->student_id);
            $correctas_alumno = $this->Test_model->get_sum_correctas($item->id);
            $porcentaje = ceil($correctas_alumno * 100 / $maxPoints);
            $correctas_malas_clase[] = array('label' => $student->name . ' ' . $student->lname, 'value' => $porcentaje);            
        }
        
        $data['correctas_malas_clase'] = $correctas_malas_clase;
        $data['results_by_grade_ok'] = $results_by_grade_ok;
        $data['results_by_grade_no'] = $results_by_grade_no;
        
        $this->load_layout('admin_teacher/analysis', $data);
    }
    
    public function ajax_remove_test() {
        $this->load->model('Test_model');
        $id = $this->input->post('id');
        $test = $this->Test_model->remove_test($id);
        echo json_encode(array());die;          
    }
    
	public function index()
	{
	    $this->load->model('Authentication_model');
        $this->load->model('Test_model');

        $this->load->model('Student_model');
        $this->load->model('Roster_model');
        
        $tid = $this->session->userdata('id');
        $roster = $this->Roster_model->get_by_teacher_id($tid);
        $students = array();
        if($roster) {
            $students = $this->Student_model->get_students_by_roster($roster->id);
            $count_students = count($students);
        } else {
            $count_students = 0;
        }

        $workspace_id = $this->session->userdata('workspace_id');
        // $count_students = $this->Authentication_model->get_count_by_type('student', $workspace_id);
        $teacher_id = $this->session->userdata('id');
        $stats_bar = $this->Test_model->get_promedios_by_teacher($teacher_id);
        $stats_bar = array_slice($stats_bar, 0, 10);
        
        $count_reports = $this->Test_model->get_count_reports($teacher_id)[0]->cantidad;
        
	    $this->load_layout('admin_teacher/index', array('stats_bar' => $stats_bar, 'cstudents' => $count_students, 'creports' => $count_reports));
	}   
    
}